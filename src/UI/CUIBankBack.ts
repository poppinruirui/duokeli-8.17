class CUIBankBack extends egret.DisplayObjectContainer {

    protected m_uiContainer:eui.Component = new eui.Component();
    protected m_btnCollect:CCosmosImage;
    protected m_btnCollect2X:CCosmosImage;
    protected m_txtBankSaving:eui.Label;


     public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/CBankBack.exml";
        this.addChild( this.m_uiContainer );

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img0" ) as eui.Image;
        this.m_btnCollect2X = new CCosmosImage();
        this.m_btnCollect2X.SetExml( "resource/assets/MyExml/CCosmosSimpleButton.exml" );
        this.m_uiContainer.addChild( this.m_btnCollect2X );
        this.m_btnCollect2X.x = imgTemp.x;
        this.m_btnCollect2X.y = imgTemp.y;
        this.m_btnCollect2X.scaleX = imgTemp.scaleX;
        this.m_btnCollect2X.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnCollect2X.SetImageColor_New( 0, 105, 105, 105 );
        this.m_btnCollect2X.SetTextColor( 0, 0xFFFFFF );     
        this.m_btnCollect2X.SetText( 0, "二倍收益(看广告)" );
        this.m_btnCollect2X.SetTextSize( 0, 22 );

        imgTemp = this.m_uiContainer.getChildByName( "img1" ) as eui.Image;
        this.m_btnCollect = new CCosmosImage();
        this.m_btnCollect.SetExml( "resource/assets/MyExml/CCosmosSimpleButton.exml" );
        this.m_uiContainer.addChild( this.m_btnCollect );
        this.m_btnCollect.x = imgTemp.x;
        this.m_btnCollect.y = imgTemp.y;
        this.m_btnCollect.scaleX = imgTemp.scaleX;
        this.m_btnCollect.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnCollect.SetImageColor_New( 0, 28, 28, 28 );
        this.m_btnCollect.SetTextColor( 0, 0xFFFFFF );     
        this.m_btnCollect.SetText( 0, "收钱" );
        this.m_btnCollect.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Collect, this);
      //  this.m_btnCollect.SetTextSize( 0, 22 );

       this.m_txtBankSaving = this.m_uiContainer.getChildByName( "txt0" ) as eui.Label;


     } // end constructor

     protected onButtonClick_Collect( evt:egret.TouchEvent ):void
     {
         Main.s_CurTown.CollectBankSaving();
         
         this.visible = false;
     }

     public SetBankSaving( nBackSaving:number ):void
     {
         this.m_txtBankSaving.text = nBackSaving.toString();
     }


} // end class