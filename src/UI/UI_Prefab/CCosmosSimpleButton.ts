class CCosmosSimpleButton extends eui.Component {

     public constructor() {
        super();

        this.skinName = "resource/assets/MyExml/CCosmosSimpleButton.exml";

     } // constructor

     public SetImgColor( nIndex:number, r:number, g:number, b:number  ):void
     {
         var img:eui.Image = this.getChildByName( "img" + nIndex ) as eui.Image;
         img.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( r, g, b ) ];
     }

     public SetTextColor( nIndex:number, color:number):void
     {
         var txt:eui.Label = this.getChildByName( "txt" + nIndex ) as eui.Label;
         txt.textColor = color;
     }

     public SetTextContent( nIndex:number, szContent:string ):void
     {
        var txt:eui.Label = this.getChildByName( "txt" + nIndex ) as eui.Label;
        txt.text = szContent;
     }
} // end class