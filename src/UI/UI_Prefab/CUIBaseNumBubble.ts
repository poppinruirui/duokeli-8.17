class CUIBaseNumBubble extends eui.Component {

     protected m_shapeBubble:egret.Shape = new egret.Shape();
     protected m_txtNum:eui.Label = new eui.Label();

     protected m_nRadius:number = 32;

      constructor() {
         super();

 this.addChild( this.m_shapeBubble );
             this.SetExml( "resource/assets/MyExml/UIBase/CUIBaseNumBubble.exml" );

            
            

             this.m_shapeBubble.graphics.beginFill( 0xFF0000 );
             this.m_shapeBubble.graphics.drawCircle(this.m_nRadius, this.m_nRadius, this.m_nRadius  );
             this.m_shapeBubble.graphics.endFill();

             this.m_txtNum = this.getChildByName( "txtNum" ) as eui.Label;
             this.m_txtNum.text = "0";
             this.addChild( this.m_txtNum );
             
        } // end class

     public SetExml( source:string ):void
     {
         this.skinName = source;
     } 

     public SetNum( num:number ):void
     {
          this.m_txtNum.text = num.toString();
     }

} // end class