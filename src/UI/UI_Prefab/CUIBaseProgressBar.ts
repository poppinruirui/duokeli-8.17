class CUIBaseProgressBar extends eui.Component {

     protected m_imgFilledBar:eui.Image;
     protected m_imgFilledBar_Bg:eui.Image;
     protected m_imgBG:eui.Image;
     protected m_labelTitle:eui.Label;

     protected m_BarTotalWidth:number = 0;
     protected m_nTotalTime:number = 0;
     protected m_fTimeElapse:number = 0;
     protected m_bProcessing:boolean = false;

     protected m_pbMain:CUICosmosProgressBar;


     public constructor( szExmlSource:string ) {
        
        super(  );

        this.SetExml( szExmlSource/*"resource/assets/MyExml/CProgressBar.exml"*/ );
         this.m_labelTitle = this.getChildByName( "labelTitle" ) as eui.Label;
         var imgTemp:eui.Image = this.getChildByName( "pbMain" ) as eui.Image;
         this.m_pbMain = new CUICosmosProgressBar();
         this.addChild( this.m_pbMain );
         this.m_pbMain.x = imgTemp.x;
         this.m_pbMain.y = imgTemp.y;

        this.m_pbMain.SetParams( imgTemp.width, imgTemp.height, 2, 0x00EE00, 0x000000, 0x006400 );
    
         this.removeChild( imgTemp );

        /*
        this.m_imgBG = this.getChildByName( "imgBg" ) as eui.Image;
        if ( this.m_imgBG )
        {
            this.m_imgBG.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 211,211,211 ) ];
        }


        this.m_imgFilledBar = this.getChildByName( "imgFilledBar" ) as eui.Image;
        this.m_imgFilledBar.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(0,255,0) ];
        this.m_BarTotalWidth = this.m_imgFilledBar.width;
        this.m_imgFilledBar.width = 0;

        this.m_imgFilledBar_Bg = this.getChildByName( "imgFilledBarBg" ) as eui.Image;
        this.m_imgFilledBar_Bg.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 0,100,0 ) ];

       
        */
     } // end constructor

     public SetTitle( szTitle:string ):void
     {
         this.m_labelTitle.text = szTitle;
     }

     public SetExml( source:string ):void
     {
         this.skinName = source;
     } 

     public Begin( nTotalTime:number ):void
     {
         this.m_nTotalTime = nTotalTime;
         this.m_fTimeElapse = 0;
         this.m_bProcessing = true;
     }

     public Loop( fPercent:number ):void
     {
      if ( fPercent > 1 )
         {
             return;
         }
         //this.m_imgFilledBar.width = this.m_BarTotalWidth * fPercent;
         this.m_pbMain.SetPercent( fPercent );
     }

     public SetFilledBarColor( r:number, g:number, b:number ):void
     {
        //this.m_imgFilledBar.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b) ];
     }

     public SetFilledBarBgColor(r:number, g:number, b:number):void
     {
        //this.m_imgFilledBar_Bg.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b) ];
     }

     public SetFilledBarWidth( nWidth:number ):void
     {
         //this.m_imgFilledBar.width = nWidth;
     }

     public SetFilledBarTotalWidth(nWidth:number):void
     {
         /*
         this.m_BarTotalWidth = nWidth;

         this.m_imgFilledBar_Bg.width = nWidth;
         */
     }

     public SetFilledBarTotalHeight(nHeight:number):void
     {
         /*
         this.m_imgFilledBar_Bg.height = nHeight;
         this.m_imgFilledBar.height = nHeight;
         */
     }

     public SetPercent( nCurValue:number, nTotalValue:number ):void
     {
         /*
         var fPercent:number = nCurValue / nTotalValue;
         this.m_imgFilledBar.width = this.m_BarTotalWidth * fPercent;
         */
     }

     public SetPercentByPercent( fPercent:number ):void
     {
         this.m_pbMain.SetPercent( fPercent );
       // this.m_imgFilledBar.width = this.m_BarTotalWidth * fPercent;
     }

} // end class