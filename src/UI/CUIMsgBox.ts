class CUIMsgBox extends egret.DisplayObjectContainer {

    protected m_imgBg:eui.Image;
    protected m_btnOK:eui.Button;
    protected m_txtMsg:eui.Label;

    public constructor() {
        super();
        this.m_imgBg = new eui.Image();
        this.m_imgBg.texture = RES.getRes("border_png"); 
        this.addChild( this.m_imgBg );
        this.m_imgBg.scaleX = 3;
        this.m_imgBg.scaleY = 6;
        
        this.m_btnOK = new eui.Button();
        this.addChild( this.m_btnOK );
        this.m_btnOK.label = "确定";
        this.m_btnOK.x = 225;
        this.m_btnOK.y = 200;
        this.m_btnOK.addEventListener(egret.TouchEvent.TOUCH_TAP,this.btnTouchHandler,this);

        this.m_txtMsg = new eui.Label;
        this.addChild( this.m_txtMsg );
        this.m_txtMsg.text = "我日你个龟";
        this.m_txtMsg.size = 35;
        this.m_txtMsg.textAlign = egret.HorizontalAlign.CENTER;//设置水平对齐方式
        this.m_txtMsg.textColor = 0xFF0000;//设置颜色
        this.m_txtMsg.x = 20;
        this.m_txtMsg.y = 100;
 }
    
    private btnTouchHandler( evt:egret.TouchEvent ):void
    {
        this.visible = false;
    }

    public ShowMsg( msg:string ):void
    {
        this.visible = true;
        this.m_txtMsg.text = msg;
    }
} // end class