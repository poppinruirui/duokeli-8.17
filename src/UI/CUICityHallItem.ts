class CUICityHallItem extends egret.DisplayObjectContainer {

        protected m_uiContainer:eui.Component = new eui.Component();
        protected m_btnUpgrade:CCosmosImage;

        protected m_progressBar:CUIBaseProgressBar;
        protected m_imgIcon:eui.Image;
        protected m_imgMoneyIcon:eui.Image;

        protected m_txtName:eui.Label;
        protected m_txtDesc:eui.Label;
        protected m_txtGain:eui.Label;

        protected m_imgMask:eui.Image;

        protected m_nCityNo:number = 0;
        protected m_nItemIndex:number = 0;
        protected m_ePageType:Global.eCityHallPageType;
        protected m_nConfig:CConfigCityHallItem = null;
        

        public constructor() {
         super();

         this.m_uiContainer.skinName = "resource/assets/MyExml/CCityHallItem.exml";
         this.addChild( this.m_uiContainer );

         var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img1" ) as eui.Image;
         this.m_btnUpgrade = new CCosmosImage();
         this.m_btnUpgrade.SetExml( "resource/assets/MyExml/CCosmosButton4.exml" );
         this.m_uiContainer.addChild( this.m_btnUpgrade );
         this.m_btnUpgrade.x =  imgTemp.x;
         this.m_btnUpgrade.y =  imgTemp.y;
         this.m_uiContainer.removeChild( imgTemp );
         this.m_btnUpgrade.SetImageColor_255( null, 0, 0, 205, 0 );
         this.m_btnUpgrade.SetImageColor_255( null, 1, 69, 139, 0 );
         this.m_btnUpgrade.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Upgrde, this);


         imgTemp =  this.m_uiContainer.getChildByName( "img3" ) as eui.Image;
         this.m_progressBar = new CUIBaseProgressBar( "resource/assets/MyExml/CProgressBar2.exml" );
         this.m_uiContainer.addChild(this.m_progressBar  );
         this.m_progressBar.x = imgTemp.x;
         this.m_progressBar.y = imgTemp.y;
         this.m_progressBar.width = imgTemp.width;
         this.m_progressBar.height = imgTemp.height;

         this.m_uiContainer.removeChild( imgTemp );
         this.m_progressBar.SetFilledBarWidth( 0 );
         this.m_progressBar.SetFilledBarTotalWidth( imgTemp.width );
         this.m_progressBar.SetFilledBarColor(86, 188, 54);
         this.m_progressBar.SetFilledBarBgColor(172, 172, 172);

         this.m_imgIcon = this.m_uiContainer.getChildByName( "img2" ) as eui.Image;
         this.m_imgMoneyIcon = this.m_uiContainer.getChildByName( "img4" ) as eui.Image;

         this.m_imgMask = this.m_uiContainer.getChildByName( "img5" ) as eui.Image;
         this.m_uiContainer.addChild( this.m_imgMask );

         this.m_txtName = this.m_uiContainer.getChildByName( "label0" ) as eui.Label;
         this.m_txtDesc = this.m_uiContainer.getChildByName( "label1" ) as eui.Label;
         this.m_txtGain = this.m_uiContainer.getChildByName( "label2" ) as eui.Label;

         
        } // end constructor

        public SetBgColor( r:number, g:number, b:number ):void
        {
           var img0:eui.Image = this.m_uiContainer.getChildByName( "img0" ) as eui.Image;
           img0.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( r, g, b )];
        }

        public SetIcon( tex:egret.Texture ):void
        {
            this.m_imgIcon.texture = tex;
        }

        public SetCityNo( nCityNo:number ):void
        {
            this.m_nCityNo = nCityNo;
        }

        public SetUpgradeBtnTotalGain( nGain:number, szContent:string ):void
        {
            if ( nGain >= 0 )
            {
                this.m_btnUpgrade.SetText( 0, "增加" );
            }
            else
            {
                this.m_btnUpgrade.SetText( 0, "减少" );
            }
            this.m_btnUpgrade.SetText( 1, szContent );
        }

        public SetUpgradeBtnCost( nCost:number ):void
        {
            this.m_btnUpgrade.SetText( 2, nCost.toString() );
        }

        public SetUPgradeMonetIcon( icon:egret.Texture ):void
        {
            this.m_btnUpgrade.SetImageTexture( 2, icon );
        }

        public SetItemIndex(  nPageIndex:number, nItemIndex:number ):void
        {
            this.m_nItemIndex = nItemIndex;
            this.m_ePageType = nPageIndex;

            this.m_nConfig = CConfigManager.GetCityHallItemConfig( nPageIndex, nItemIndex );
            if ( this.m_nConfig != null )
            {
                this.m_txtName.text = this.m_nConfig.szName;
                this.m_txtDesc.text = this.m_nConfig.szDesc;
                this.m_txtGain.text = CConfigManager.GenShowStyle ( this.m_nConfig.eShowStyle, 0 );
                
                var nGain:number = this.m_nConfig.nGain;
               
                this.SetUpgradeBtnTotalGain( nGain, CConfigManager.GenShowStyle ( this.m_nConfig.eShowStyle, this.m_nConfig.nGain ) );

                this.SetUpgradeBtnCost( Math.floor( this.m_nConfig.aryValuesCost[0] ) );
                this.SetUPgradeMonetIcon( nPageIndex == 0? RES.getRes( CDef.JINBI_RES ) : RES.getRes( CDef.ZUANSHI_RES ) );
                
            }
            else
            {
                this.m_txtName.text = "（功能尚未开发）";
                this.m_txtDesc.text = "";
                this.m_txtGain.text = "";
                this.m_btnUpgrade.SetEnabled( false );
                this.SetUPgradeMonetIcon( nPageIndex == 0? RES.getRes( CDef.JINBI_RES ) : RES.getRes( CDef.ZUANSHI_RES ) );
   
            }
        }

        public GetItemIndex( ):number
        {
            return this.m_nItemIndex;
        }

        public GetCityNo( ):number
        {
            return this.m_nCityNo;
        }

        public SetLock( bLock:boolean ):void
        {
            this.m_imgMask.visible = bLock;
        }

        protected onButtonClick_Upgrde( evt:egret.TouchEvent ):void
        {
            if ( this.m_nConfig == null )
            {
                console.log( "有Bug，还没开发的功能怎么点得中" );
                return;
            }

            var eMoneyType:Global.eMoneyType = this.m_ePageType == Global.eCityHallPageType.city? Global.eMoneyType.coin : Global.eMoneyType.diamond;

            var data:CCityHallData = Main.s_CurTown.GetCityHallData( this.m_ePageType, this.m_nItemIndex );
            var nCost:number = this.m_nConfig.aryValuesCost[data.m_nLevel];

            var nMyMoney:number = 0;
            if ( this.m_ePageType == Global.eCityHallPageType.city )
            {
                nMyMoney = Main.s_CurTown.GetCoins();
            }
            else if ( this.m_ePageType == Global.eCityHallPageType.game )
            {
                nMyMoney = CPlayer.GetDiamond();
            }

            if ( nMyMoney < nCost )
            {
                console.log( "有bug，钱不够按钮该灰掉，怎么点中的？？" );
                return;
            }

            // 消费金币或钻石
            if ( this.m_ePageType == Global.eCityHallPageType.city )
            {
               Main.s_CurTown.SetCoins( Main.s_CurTown.GetCoins() - nCost, true );
            }
            else if ( this.m_ePageType == Global.eCityHallPageType.game )
            {
                CPlayer.SetDiamond( CPlayer.GetDiamond() - nCost );
            }

            // 获得加成
            // to do


            // 数据刷新 & CityHall界面刷新
            data.m_nLevel++;
            data.m_nCurTotalGain = data.m_nLevel * this.m_nConfig.nGain;

            this.m_txtGain.text = CConfigManager.GenShowStyle( Global.eValueShowStyle.percent, data.m_nCurTotalGain );
        
            if ( data.m_nLevel >= this.m_nConfig.GetMaxLevel() )
            {
                this.m_btnUpgrade.visible = false;
            }
            else
            {
                var nMoneyForNextLevel:number = this.m_nConfig.aryValuesCost[ data.m_nLevel ]; 
                nMyMoney = CPlayer.GetMoney( eMoneyType );
                this.SetUpgradeBtnCost(Math.floor( nMoneyForNextLevel ) );
                if ( nMyMoney < nMoneyForNextLevel )
                {
                    this.m_btnUpgrade.SetEnabled( false );
                }
            }

            this.m_progressBar.SetPercent( data.m_nLevel, this.m_nConfig.GetMaxLevel() );

            Main.s_CurTown.SetCityHallItemAffect( 0, data.m_nCurTotalGain );
            Main.s_CurTown.UpdateCurLotStatus();
            Main.s_CurTown.OnBuildingCPSChanged();
        }

        public UpdateStatus():void
        {
            if (  this.m_nConfig == null )
            {
                return;
            }

            var data:CCityHallData = Main.s_CurTown.GetCityHallData( this.m_ePageType, this.m_nItemIndex );

            if ( data.m_nLevel >= this.m_nConfig.GetMaxLevel() )
            {
                return;
            }

            var nCost:number = this.m_nConfig.aryValuesCost[data.m_nLevel];

            var nMyMoney:number = 0;
            if ( this.m_ePageType == Global.eCityHallPageType.city )
            {
                nMyMoney = CPlayer.GetMoney( Global.eMoneyType.coin );
            }
            else if ( this.m_ePageType == Global.eCityHallPageType.game )
            {
                nMyMoney = CPlayer.GetMoney( Global.eMoneyType.diamond );
            }
            if ( nMyMoney > nCost )
            {
                this.m_btnUpgrade.SetEnabled( true );
            }
            else
            {
                this.m_btnUpgrade.SetEnabled( false );
            }

        }


} // end class