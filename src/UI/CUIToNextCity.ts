class CUIToNextCity extends egret.DisplayObjectContainer {

     protected m_uiContainer:eui.Component = new eui.Component();

     protected m_txtTitle:eui.Label;
     protected m_txtPrompt:eui.Label;

     protected m_btnCancel:CCosmosImage;
     protected m_btnNext:CCosmosImage;
      protected m_btncontinue:CCosmosImage;

      public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/CToNextCityPanel.exml";
        this.addChild( this.m_uiContainer );

        this.m_txtTitle = this.m_uiContainer.getChildByName( "txt0" ) as eui.Label;
        this.m_txtPrompt = this.m_uiContainer.getChildByName( "txt1" ) as eui.Label;

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img2" ) as eui.Image;
        this.m_btnCancel = new CCosmosImage();
        this.m_btnCancel.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        this.m_uiContainer.addChild( this.m_btnCancel );
        this.m_btnCancel.x = imgTemp.x;
        this.m_btnCancel.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnCancel.SetImageColor_New( 0, 105, 105, 105 );
        this.m_btnCancel.SetTextColor(0, 0xFFFFFF);
        this.m_btnCancel.SetText( 0, "取消" );
        this.m_btnCancel.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Cancel, this);

        imgTemp = this.m_uiContainer.getChildByName( "img3" ) as eui.Image;
        this.m_btnNext = new CCosmosImage();
        this.m_btnNext.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        this.m_uiContainer.addChild( this.m_btnNext );
        this.m_btnNext.x = imgTemp.x;
        this.m_btnNext.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnNext.SetImageColor_New( 0, 0, 0, 0 );
        this.m_btnNext.SetTextColor(0, 0xFFFFFF);
        this.m_btnNext.SetText( 0, "下一级城市" );
         this.m_btnNext.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Next, this);


        imgTemp = this.m_uiContainer.getChildByName( "img4" ) as eui.Image;
        this.m_btncontinue = new CCosmosImage();
        this.m_btncontinue.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        this.m_uiContainer.addChild( this.m_btncontinue );
        this.m_btncontinue.x = imgTemp.x;
        this.m_btncontinue.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btncontinue.SetImageColor_New( 0, 0, 0, 0 );
        this.m_btncontinue.SetTextColor(0, 0xFFFFFF);
        this.m_btncontinue.SetText(0, "继续");
           this.m_btncontinue.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Continue, this);

        imgTemp  = this.m_uiContainer.getChildByName( "img0" ) as eui.Image;
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( 0, 0, 0 )];



      } // constructor

      public UpdateInfo():void
      {
          var objectRet:Object = Main.s_CurTown.CheckIfReachThePopulationToNextCity(  );
          if ( objectRet["Result"] )
          {
              this.m_btnNext.visible = true;
              this.m_btnCancel.visible = true;
              this.m_btncontinue.visible = false;
              this.m_txtTitle.text = "恭喜";
              this.m_txtPrompt.text = "人口已达到升级标准，可升级到下一座城市。";

              CSoundManager.PlaySE( Global.eSE.congratulations );
          }
          else
          {
              this.m_btnNext.visible = false;
              this.m_btnCancel.visible = false;
              this.m_btncontinue.visible = true;
              this.m_txtTitle.text = "继续努力";
              this.m_txtPrompt.text = "人口达到" + objectRet["NeedPopulation"] + "即可升级到下一座城市。";
          }
      }

      protected onButtonClick_Cancel( evt:egret.TouchEvent ):void
      {
          CUIManager.SetUiVisible( Global.eUiId.to_next_city, false );
      }

      protected onButtonClick_Next( evt:egret.TouchEvent ):void
      {
          CUIManager.SetUiVisible( Global.eUiId.to_next_city, false );
          Main.s_CurTown.MoveToNextCity();
      }

      protected onButtonClick_Continue( evt:egret.TouchEvent ):void
      {
          CUIManager.SetUiVisible( Global.eUiId.to_next_city, false );  
      }

} // end class