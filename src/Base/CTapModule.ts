/*
     “点击”控制模块。多点触控功能就在这里面

*/

class CTapModule extends egret.DisplayObjectContainer {

    protected static s_aryTouchPoint:Array<CVector> = new Array<CVector>();
    protected static s_nCurDisBetweenTwoPoints:number = 0;

    protected static s_bZooming:boolean = false;

    public static AddTapPoint( touch_id:number, x:number, y:number ):void
    {
        var bExist:boolean = false;
        for ( var  i:number = 0; i < CTapModule.s_aryTouchPoint.length; i++ )
        {
            if ( touch_id == CTapModule.s_aryTouchPoint[i].id )
            {
                CTapModule.s_aryTouchPoint[i].x = x;
                CTapModule.s_aryTouchPoint[i].y = y;
                bExist = true;
                break;
            }
        }

        if ( !bExist )
        {
            if ( CTapModule.s_aryTouchPoint.length >= 2 )
            {
                CResourceManager.DeleteVector( CTapModule.s_aryTouchPoint[0] );
                CTapModule.s_aryTouchPoint.shift();
            }
                    var point:CVector = CResourceManager.NewVector();
        point.id = touch_id;
        point.x = x;
        point.y = y;
            CTapModule.s_aryTouchPoint.push( point );
        }

        if ( CTapModule.s_aryTouchPoint.length == 2 )
        {
       
           //   CUIManager.s_uiMainTitle.ShowDebugInfo("cur evt =" +  CTapModule.s_nCurDisBetweenTwoPoints );
              if ( !CTapModule.s_bZooming )
              {
                  CTapModule.BeginZoom();
              }
        }
    }
        protected static m_fTimeElapse:number = 0;
    protected static BeginZoom():void
    {
        CTapModule.s_bZooming  = true;
        CTapModule.m_fTimeElapse = 0;
        CTapModule.s_nCurDisBetweenTwoPoints = CTapModule.CalculateDisBetweenTwoPoints();
    }

    protected static EndZoom():void
    {
        CTapModule.s_bZooming  = false;
    }

    public static FixedUpdate():void
    {


        if ( !CTapModule.s_bZooming )
        {
            return;
        }

        
        CTapModule.m_fTimeElapse += CDef.s_fFixedDeltaTime;
        if ( CTapModule.m_fTimeElapse < 0.5 )
        {
            return;
        }
        CTapModule.m_fTimeElapse  = 0;
        

        if ( CTapModule.s_aryTouchPoint.length != 2 )
        {
            CTapModule.EndZoom();
            return;
        }

         var nCurDis:number =  CTapModule.CalculateDisBetweenTwoPoints();
        var fBiLi:number = ( nCurDis - CTapModule.s_nCurDisBetweenTwoPoints );
        // CUIManager.s_uiMainTitle.ShowDebugInfo("cur evt =" + CTapModule.s_aryTouchPoint[0].y + "_" + CTapModule.s_aryTouchPoint[1].y  );
         CTapModule.s_nCurDisBetweenTwoPoints = nCurDis;
         if ( fBiLi < 0 )
         {
             Main.s_CurTown.BeginZoom( -1 );
           // Main.s_CurTown.Zoom( -0.1 );
         }else if ( fBiLi > 0 )
         {
              Main.s_CurTown.BeginZoom( 1 );
           //    Main.s_CurTown.Zoom( 0.1 );
         }
         
    }

    protected static CalculateDisBetweenTwoPoints():number
    {
            var x0:number = CTapModule.s_aryTouchPoint[0].x;
            var y0:number = CTapModule.s_aryTouchPoint[0].y;
            var x1:number = CTapModule.s_aryTouchPoint[1].x;
            var y1:number = CTapModule.s_aryTouchPoint[1].y;
            var dis:number = ( x0 - x1 ) * ( x0 - x1 ) + ( y0 - y1 ) * ( y0 - y1 );
            return dis;//Math.sqrt( dis );
    }

    public static RemovePoint(touch_id:number):void
    {
        for ( var i:number = 0; i < CTapModule.s_aryTouchPoint.length; i++ )
        {
            if ( CTapModule.s_aryTouchPoint[i].id == touch_id )
            {
                CResourceManager.DeleteVector( CTapModule.s_aryTouchPoint[i] );
                CTapModule.s_aryTouchPoint.splice( i, 1 );
                return;
            }
        }
      //  console.log( "有bug，没找到对应的touch_id" );
    }

    //  直行双点触控以缩放
    public static TwoPointsTapToZoom():boolean
    {

     if ( CTapModule.s_aryTouchPoint.length < 2 )
     {
         return false;
     }   
     /*
     var nCurDis:number = CTapModule.CalculateDisBetweenTwoPoints();
     
     if ( CTapModule.s_nCurDisBetweenTwoPoints <= 0 )
     {
         CTapModule.s_nCurDisBetweenTwoPoints = 1;
     }
     var fBiLi:number = ( nCurDis - CTapModule.s_nCurDisBetweenTwoPoints ) / CTapModule.s_nCurDisBetweenTwoPoints;
     CUIManager.s_uiMainTitle.ShowDebugInfo("cur evt =" + nCurDis+ "_" + CTapModule.s_nCurDisBetweenTwoPoints );
     CTapModule.s_nCurDisBetweenTwoPoints = nCurDis;

     Main.s_CurTown.Zoom( fBiLi );
     */
     return true;

    }

} // end class