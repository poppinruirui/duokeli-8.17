
class CCloudManager extends egret.DisplayObjectContainer {

    protected m_containerClouds:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    protected m_containerMoutains:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    protected m_containerLights:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    protected m_containerBubble:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
   
    protected m_containerShapes:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();

    protected m_nStageWidth:number = 0;
    protected m_nStageHeight:number = 0;

    protected m_nCloudSpeed:number = 1;

    protected m_nTownId:number = 0;

    protected m_nMaxHeight:number = 0;

protected m_stageBg:egret.Shape = null;
protected m_matrix:egret.Matrix = null;

    public constructor() {
            super();
  
             this.addChild( this.m_containerShapes );
            this.addChild( this.m_containerMoutains );
            this.addChild( this.m_containerClouds );
            this.addChild( this.m_containerLights );
         
           /// this.addChild( this.m_containerBubble );

    } // end constructor

    public GetBubbleContainer():egret.DisplayObjectContainer
    {
        return this.m_containerBubble;
    }

    public Clear():void
    {
        this.m_nTownId = 0;

        // 清除浮云
        for ( var i:number = this.m_containerClouds.numChildren - 1; i >= 0; i-- )    
        {
            var cloud:CObj = this.m_containerClouds.getChildAt(i) as CObj;
            CResourceManager.DeleteObj( cloud );
            
        }

        // 清除山体
        for ( var i:number = this.m_containerMoutains.numChildren - 1; i >= 0; i-- )    
        {
            var moutain:CObj = this.m_containerMoutains.getChildAt(i) as CObj;
            CResourceManager.DeleteObj( moutain );
            
        }

        // 清除气泡
        for ( var i:number = this.m_containerBubble.numChildren - 1; i >= 0; i-- )    
        {
            var bubble:CObj = this.m_containerBubble.getChildAt(i) as CObj;
            CResourceManager.DeleteObj( bubble );
            
        }
        
        // 清除shapes
        for ( var i:number = this.m_containerShapes.numChildren - 1; i >= 0; i-- )
        {
            var shape:egret.Shape = this.m_containerShapes.getChildAt(i) as egret.Shape;
            CResourceManager.DeleteShape( shape );
        }

        // 清除光线
        for ( var i:number = this.m_containerLights.numChildren - 1; i >= 0; i-- )
        {
            var light:CObj = this.m_containerLights.getChildAt(i) as CObj;
            CResourceManager.DeleteObj( light );
        }

    }

     public SetStageBg( stageBg:egret.Shape, matrix:egret.Matrix  ):void
     {
        this.m_stageBg = stageBg;
        this.m_matrix = matrix;
     }

     protected DrawStageBg( color1:number, color2:number ):void
     {
         this.m_stageBg.touchEnabled = false;

     this.m_stageBg.graphics.beginGradientFill( egret.GradientType.LINEAR, [color1, color2], [1,1], [0,255], this.m_matrix );

        this.m_stageBg.graphics.drawRect( 0, 0, this.m_nStageHeight, this.m_nStageWidth  );
       this.m_stageBg.graphics.endFill();
     }

     public InitLights(nTownId:number):void
     {
         this.m_nTownId = nTownId;  

         switch(this.m_nTownId)
         {
             case 8:
             {
               this.InitLights_8();  
             }
             break;


         } // end switch
     }

     public CloudLoop_1Sec():void
     {
         this.GenerateBubbles();
     }

     public GenerateBubbles_5():void
     {
        this.m_fGenereateBubbleTimeElapse += 1;
        if ( this.m_fGenereateBubbleTimeElapse < 5 )
        {
            return;
        }
        this.m_fGenereateBubbleTimeElapse = 0;
        var bubble:CObj = CResourceManager.NewObj();
        this.m_containerBubble.addChild( bubble );
        bubble.SetTexture( CResourceManager.GetBubbleTextureByTownId( this.m_nTownId ) );
        bubble.x = 640 * Math.random();
        bubble.y = 1150;
        var fScale:number =  0.3 + 0.3 * Math.random();
        bubble.scaleX = fScale
        bubble.scaleY = fScale;
        var fSpeed:number = 25 + 10 * Math.random();  
        bubble.SetSpeed( fSpeed );
        bubble.SetTime(0);
        var params:Object =  bubble.GetParams();
        params["x"] = bubble.x;

        this.m_nBubbleDir = 1;
     }

      public GenerateBubbles_8():void
     {
        this.m_fGenereateBubbleTimeElapse += 1;
        if ( this.m_fGenereateBubbleTimeElapse < 5 )
        {
            return;
        }
        this.m_fGenereateBubbleTimeElapse = 0;
        var bubble:CObj = CResourceManager.NewObj();
        this.m_containerBubble.addChild( bubble );
        bubble.SetTexture( CResourceManager.GetBubbleTextureByTownId( this.m_nTownId ) );
        bubble.x = 640 * Math.random();
        bubble.y = 1150;
        var fScale:number =  0.3 + 0.3 * Math.random();
        bubble.scaleX = fScale
        bubble.scaleY = fScale;
        var fSpeed:number = 25 + 10 * Math.random();  
        bubble.SetSpeed( fSpeed );
        bubble.SetTime(0);
        var params:Object =  bubble.GetParams();
        params["x"] = bubble.x;

        this.m_nBubbleDir = 1;
     }

     public GenerateBubbles_6():void
     {
        this.m_fGenereateBubbleTimeElapse += 1;
        if ( this.m_fGenereateBubbleTimeElapse < 4 )
        {
            return;
        }
        this.m_fGenereateBubbleTimeElapse = 0;
        var bubble:CObj = CResourceManager.NewObj();
        this.m_containerBubble.addChild( bubble );
        bubble.SetTexture( CResourceManager.GetBubbleTextureByTownId( this.m_nTownId ) );
        bubble.x = 640 * Math.random();
        bubble.y = 0;
        var fScale:number =  0.3 + 0.3 * Math.random();
        bubble.scaleX = fScale
        bubble.scaleY = fScale;
        var fSpeed:number = 25 + 10 * Math.random();  
        bubble.SetSpeed( fSpeed );
        bubble.SetTime(0);
        var params:Object =  bubble.GetParams();
        params["x"] = bubble.x;

         this.m_nBubbleDir = -1;
     }

     protected m_fGenereateBubbleTimeElapse:number = 0;
     public GenerateBubbles_4():void
     {
        this.m_fGenereateBubbleTimeElapse += 1;
        if ( this.m_fGenereateBubbleTimeElapse < 5 )
        {
            return;
        }
        this.m_fGenereateBubbleTimeElapse = 0;
        var bubble:CObj = CResourceManager.NewObj();
        this.m_containerBubble.addChild( bubble );
        bubble.SetTexture( CResourceManager.GetBubbleTextureByTownId( this.m_nTownId ) );
        bubble.x = 640 * Math.random();
        bubble.y = 1150;
        var fScale:number =  0.3 + 0.3 * Math.random();
        bubble.scaleX = fScale
        bubble.scaleY = fScale;
        var fSpeed:number = 25 + 10 * Math.random();  
        bubble.SetSpeed( fSpeed );
        bubble.SetTime(0);
        var params:Object =  bubble.GetParams();
        params["x"] = bubble.x;
     }

     protected BubbleMoveLoop( ):void
     {
       
         var fBubbleSpeed:number = 30;
         for ( var i:number = this.m_containerBubble.numChildren - 1; i >= 0; i-- )
         {
             var bubble:CObj = this.m_containerBubble.getChildAt(i) as CObj;
             if (  this.m_nBubbleDir == 1 )
             {
                bubble.y -= bubble.GetSpeed() * CDef.s_fFixedDeltaTime;
             }
             else
             {
                bubble.y += bubble.GetSpeed() * CDef.s_fFixedDeltaTime;
             }

              bubble.SetTime(bubble.GetTime() + 0.05);
              var params:Object = bubble.GetParams();
              bubble.x = params["x"] +  Math.sin( bubble.GetTime() ) * 10;

            if (  this.m_nBubbleDir == 1 )
             {
                if ( bubble.y < 400 )
                {
                    var fAlpha:number =( bubble.y - 200 ) / 200;
                    bubble.alpha = fAlpha;
                }
                if ( bubble.y < 200  )
                {
                    CResourceManager.DeleteObj( bubble );
                }
             }
             else
             {
                if ( bubble.y > 400 )
                {
                    var fAlpha:number = 1 - (  bubble.y - 400 ) / 400;
                    bubble.alpha = fAlpha;
                }
                if ( bubble.y > 800  )
                {
                    CResourceManager.DeleteObj( bubble );
                }
             }
         }
     }

     // 气泡
     protected m_nBubbleDir:number = 1;
     public GenerateBubbles():void
     {
         switch( this.m_nTownId )
         {
             /*
             case 8:
             {
                 this.GenerateBubbles_8();
             }
             break;
             */

            case 5:
            {
                this.GenerateBubbles_5();
            }
            break;

            case 6:
            {
                this.GenerateBubbles_6();
            }
            break;




         }
     }

     // 光效
     public InitLights_8():void
     {
        for ( var i:number = 0; i < 3; i++ )
        {
            var nLightWidth:number = 1200;
            var nLightHeight:number = 200;
             
            var light:egret.Shape = new egret.Shape();
              
            switch( i )
            {
                case 0:
                {
                    nLightHeight = 200;
                }
                break;

                case 1:
                {
                    nLightHeight = 100;
                    light.x = 30;
                }
                break;

                case 2:
                {
                    nLightHeight = 400;
                    light.x = 100;
                }
                break;


            } // end switch

            light.y = -50;

            var matrix:egret.Matrix = new egret.Matrix();
            matrix.createGradientBox( nLightWidth, nLightHeight);

            var fAlpha:number = 0.3;
            this.m_containerLights.addChild( light );
            light.graphics.beginGradientFill( egret.GradientType.LINEAR, [0x01b6ff, 0x01b6ff], [fAlpha,0], [0,255], this.m_matrix );
            light.graphics.drawRect( 0, 0, nLightWidth, nLightHeight  );
            light.graphics.endFill();
            light.rotation = 60;
        }
     }

    public InitClouds( nTownId:number, stageWidth:number, stageHeight:number ):void
    {
        this.m_nTownId = nTownId;  
        this.m_nStageWidth = stageWidth;
        this.m_nStageHeight = stageHeight;

        switch( this.m_nTownId )
        {
            case 1:
            {
                this.InitClouds_1( );
                   this.InitMoutains_1();
                this.DrawStageBg( 0x2fd1e8, 0x01cbbb );
             
            }
            break;

            case 2:
            {
                this.InitClouds_2( );
                this.InitMoutains_2();
                this.DrawStageBg( 0xffc283, 0xff9992 );
            }
            break;

            case 3:
            {
                this.InitClouds_3( );
                this.InitMoutains_3();
                this.DrawStageBg( 0x71e09c, 0x0dd1bd );
            }
            break;

            case 4:
            {
                this.InitClouds_4( );
                this.DrawStageBg( 0x04c3df, 0x0092d7 ); 
            }
            break;

            case 5:
            {
                this.DrawStageBg( 0xfbc75a, 0xf7604b ); 
                 this.InitClouds_5( );
            }
            break;

            case 6:
            {
                this.DrawStageBg( 0x4e8fd1, 0x3862aa ); 
                   this.InitMoutains_6();
              //   this.InitClouds_5( );
            }
            break;

            case 7:
            {
                this.DrawStageBg( 0x41283d, 0x24304a ); 
                this.InitMoutains_7();
                this.InitMoon_7();
                this.InitStars();
            }
            break;

            case 8:
            {
               this.DrawStageBg( 0x75db0, 0x092c52 );
                this.InitClounds_8();
                this.InitLights_8();
            }
            break;

        } // end switch
    }
    
    public InitMoutains_2():void
    {
        for ( var i:number = 0 ; i < 3; i++ )
        {
            var moutain:CObj = CResourceManager.NewObj();
            this.m_containerMoutains.addChild( moutain );
            moutain.SetTexture( CResourceManager.GetMoutainTextureByTownId( this.m_nTownId )  );
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = moutain.width * i;
            moutain.y = this.m_nStageHeight;
        }   
    }

    public InitMoutains_3():void
    {
        for ( var i:number = 0 ; i < 3; i++ )
        {
            var moutain:CObj = CResourceManager.NewObj();
            this.m_containerMoutains.addChild( moutain );
            moutain.SetTexture( CResourceManager.GetMoutainTextureByTownId( this.m_nTownId )  );
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = moutain.width * i;
            moutain.y = this.m_nStageHeight;
        }   
    }

    public InitMoutains_6():void
    {
 var moutain:CObj = CResourceManager.NewObj();
            this.m_containerMoutains.addChild( moutain );
            moutain.SetTexture( CResourceManager.GetMoutainTextureByTownId( this.m_nTownId )  );
            var fScale:number = 0.4;
            moutain.scaleX = fScale;
            moutain.scaleY = fScale;
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = 0;
            moutain.y = this.m_nStageHeight;

            moutain = CResourceManager.NewObj();
            this.m_containerMoutains.addChild( moutain );
            moutain.SetTexture( CResourceManager.GetMoutainTextureByTownId( this.m_nTownId )  );
            var fScale:number = 0.6;
            moutain.scaleX = fScale;
            moutain.scaleY = fScale;
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = moutain.width * 0.4 - 150;
            moutain.alpha = 0.5;
            moutain.y = this.m_nStageHeight;
    }

    public InitMoon_7():void
    {
        var moon:CObj = new CObj();
        this.m_containerMoutains.addChild( moon );
        moon.SetTexture( RES.getRes( "moon7_png" ) );
        moon.x = 100;
        moon.y = 100;
        moon.scaleX = 0.3;
        moon.scaleY = 0.3;
    }

    public InitStars():void
    {
        for ( var i:number = 0; i < 16; i++ )
        {
            var star:egret.Shape = new egret.Shape();
            this.m_containerShapes.addChild( star );
            star.graphics.beginFill( 0x6a739e, 1 );

            star.graphics.drawCircle( this.m_nStageWidth * Math.random(), this.m_nStageHeight * Math.random(), 10 *  ( 0.5 + 0.5 * Math.random() )  );
            star.graphics.endFill();

        }
    }

  

    public InitMoutains_7():void
    {
                    var moutain:CObj = CResourceManager.NewObj();
            this.m_containerMoutains.addChild( moutain );
            moutain.SetTexture( CResourceManager.GetMoutainTextureByTownId( this.m_nTownId )  );
            var fScale:number = 0.4;
            moutain.scaleX = fScale;
            moutain.scaleY = fScale;
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = 0;
            moutain.y = this.m_nStageHeight;

            moutain = CResourceManager.NewObj();
            this.m_containerMoutains.addChild( moutain );
            moutain.SetTexture( CResourceManager.GetMoutainTextureByTownId( this.m_nTownId )  );
            var fScale:number = 0.6;
            moutain.scaleX = fScale;
            moutain.scaleY = fScale;
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = moutain.width * 0.4 - 150;
            moutain.alpha = 0.5;
            moutain.y = this.m_nStageHeight;
    }

    public InitMoutains_1():void
    {
      
            var moutain:CObj = CResourceManager.NewObj();
            this.m_containerMoutains.addChild( moutain );
            moutain.SetTexture( CResourceManager.GetMoutainTextureByTownId( this.m_nTownId )  );
            var fScale:number = 0.4;
            moutain.scaleX = fScale;
            moutain.scaleY = fScale;
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = 0;
            moutain.y = this.m_nStageHeight;

            moutain = CResourceManager.NewObj();
            this.m_containerMoutains.addChild( moutain );
            moutain.SetTexture( CResourceManager.GetMoutainTextureByTownId( this.m_nTownId )  );
            var fScale:number = 0.6;
            moutain.scaleX = fScale;
            moutain.scaleY = fScale;
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = moutain.width * 0.4 - 150;
            moutain.alpha = 0.5;
            moutain.y = this.m_nStageHeight;
        
    }

    public InitClouds_2( ):void
    {
        for ( var i:number = 0; i < 6; i++ )
        {
            var cloud:CObj = CResourceManager.NewObj();
            this.m_containerClouds.addChild( cloud );
            var nCloudSubId:number = 1;
            if ( i % 2 == 0 )
            {
                nCloudSubId = 2;
            }
            cloud.SetTexture( CResourceManager.GetCloudTextureByTownId( this.m_nTownId, nCloudSubId )  );
            var scale:number = 0.2 + 0.2 * Math.random(); 
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random(); 
            cloud.y =( this.m_nStageHeight - 200 ) * Math.random(); 
            cloud.SetSpeed( this.m_nCloudSpeed * scale );
            var fTemp:number = Math.random();
            if ( fTemp > 0.5 )
            {
                cloud.alpha = 0.6;
            }  
            
        }

    }

    public GetCurAlphaByPos_4( pos:number ) :number
    {
            var max_alpha:number = 1;
            var min_alpha:number = 0.15;
            var max_pos_y:number = this.m_nMaxHeight;
            var min_pos_y:number = 0;
            var cur_pos:number = pos - min_pos_y;
            var total_dis:number = max_pos_y - min_pos_y;
            var total_alpha:number = max_alpha - min_alpha;
            var cur_alpha:number = min_alpha + ( cur_pos / total_dis ) * total_alpha ;
            return cur_alpha;
    }

    public InitClounds_4():void
    {
        this.m_nMaxHeight = this.m_nStageHeight - 150;
        for ( var i:number = 0; i < 15; i++ )
        {
            var cloud:CObj = CResourceManager.NewObj();
            this.m_containerClouds.addChild( cloud );
            cloud.SetTexture( CResourceManager.GetCloudTextureByTownId( this.m_nTownId )  );
            var scale:number = 0.1 + 0.3 * Math.random(); 
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random(); 
            cloud.y = this.m_nMaxHeight * Math.random(); 
            cloud.SetSpeed( 2 * scale );
            
            cloud.alpha = this.GetCurAlphaByPos_4( cloud.y );
       
        } //  end for
            
    }

    public InitClouds_5():void
    {
         for ( var i:number = 0; i < 6; i++ )
        {
            var cloud:CObj = CResourceManager.NewObj();
            this.m_containerClouds.addChild( cloud );
            var nCloudSubId:number = 1;
            if ( i % 2 == 0 )
            {
                nCloudSubId = 2;
               
                 cloud.SetColor( 134,87,67 );
            }
            else
            {
                 cloud.SetColor( 255,222,143 );
               
            }
            cloud.SetTexture( CResourceManager.GetCloudTextureByTownId( this.m_nTownId - 1, nCloudSubId )  );
            var scale:number = 0.2 + 0.2 * Math.random(); 
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random(); 
            cloud.y =( this.m_nStageHeight - 100 ) * Math.random(); 
            cloud.SetSpeed( this.m_nCloudSpeed * scale );
            var fTemp:number = Math.random();
            if ( fTemp > 0.5 )
            {
                cloud.alpha = 0.6;
            }  
        } // end for
                
    }

    public InitClouds_4():void
    {

        for ( var i:number = 0; i < 6; i++ )
        {
            var cloud:CObj = CResourceManager.NewObj();
            this.m_containerClouds.addChild( cloud );
            var nCloudSubId:number = 1;
            if ( i % 2 == 0 )
            {
                nCloudSubId = 2;
            }
            cloud.SetTexture( CResourceManager.GetCloudTextureByTownId( this.m_nTownId, nCloudSubId )  );
            var scale:number = 0.2 + 0.2 * Math.random(); 
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random(); 
            cloud.y =( this.m_nStageHeight - 100 ) * Math.random(); 
            cloud.SetSpeed( this.m_nCloudSpeed * scale );
            var fTemp:number = Math.random();
            if ( fTemp > 0.5 )
            {
                cloud.alpha = 0.6;
            }  
            
        }

    }

    public InitClouds_3( ):void
    {
        for ( var i:number = 0; i < 6; i++ )
        {
            var cloud:CObj = CResourceManager.NewObj();
            this.m_containerClouds.addChild( cloud );
            var nCloudSubId:number = 1;
            if ( i % 2 == 0 )
            {
                nCloudSubId = 2;
            }
            cloud.SetTexture( CResourceManager.GetCloudTextureByTownId( this.m_nTownId - 1, nCloudSubId )  );
            var scale:number = 0.2 + 0.2 * Math.random(); 
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random(); 
            cloud.y =( this.m_nStageHeight - 300 ) * Math.random(); 
            cloud.SetSpeed( this.m_nCloudSpeed * scale );
            var fTemp:number = Math.random();
            if ( fTemp > 0.5 )
            {
                cloud.alpha = 0.6;
            }  
            
        }

    }


     public InitClouds_1( ):void
    {
       


        for ( var i:number = 0; i < 8; i++ )
        {
           this.GenerateCloud_1();
            
        }
    }

    protected onLoadCloudComplete(event:any):void {
      
        var tex: egret.Texture = <egret.Texture>event;
        for ( var i:number = 0; i < this.m_containerClouds.numChildren; i++ )
        {
            var cloud:CObj = this.m_containerClouds.getChildAt(i) as CObj;
            cloud.SetTexture( tex );
        }
    }

    public InitClounds_8():void
    {
        
        for ( var i:number = 0; i< 16; i++ )
        {
            var cloud:CObj = CResourceManager.NewObj();
            this.m_containerClouds.addChild( cloud );
          
         
            var szFileName:string = CResourceManager.GetCloudResNameByTownId_Local(this.m_nTownId);
            cloud.SetTexture( RES.getRes( szFileName ) );
           
            var scale:number = 0.1 + 0.3 * Math.random(); 
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random(); 
            cloud.y = ( this.m_nStageHeight - 200 ) * Math.random(); 
            cloud.SetSpeed( this.m_nCloudSpeed * scale );
            var fTemp:number = Math.random();
            if ( fTemp > 0.5 )
            {
                cloud.alpha = 0.6;
            }
        }
    }


    public GenerateCloud_1():void
    {
            var cloud:CObj = CResourceManager.NewObj();
            this.m_containerClouds.addChild( cloud );
          
           // var szFileName:string = CResourceManager.GetCloudResNameByTownId(this.m_nTownId);
           // RES.getResByUrl( szFileName ,this.onLoadCloudComplete,this,RES.ResourceItem.TYPE_IMAGE);
            var szFileName:string = CResourceManager.GetCloudResNameByTownId_Local(this.m_nTownId);
           cloud.SetTexture( RES.getRes( szFileName ) );
           
            var scale:number = 0.1 + 0.3 * Math.random(); 
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random(); 
            cloud.y = ( this.m_nStageHeight - 400 ) * Math.random(); 
            cloud.SetSpeed( this.m_nCloudSpeed * scale );
            var fTemp:number = Math.random();
            if ( fTemp > 0.5 )
            {
                cloud.alpha = 0.6;
            }
    }

    public CloudLoop():void
    {
     if ( this.m_nTownId == 0 )
     {
         return;
     }   

     this.BubbleMoveLoop();

     switch( this.m_nTownId )
     {
         case 1:
         {
             this.CloudLoop_1();
         }
         break;

         case 2:
         {
            this.CloudLoop_2();
         }
         break;

         case 3:
         {
            this.CloudLoop_3();
         }
         break;

         case 4:
         {
            this.CloudLoop_4();
         }
         break;

         case 5:
         {
            this.CloudLoop_5();
         }
         break;

         case 8:
         {
             this.CloudLoop_8();
         }
         break;

     } // end switch

    }
    public CloudLoop_3()
    {
        for ( var i:number = this.m_containerClouds.numChildren - 1; i >= 0; i-- )
        {
            var cloud:CObj = this.m_containerClouds.getChildAt(i) as CObj;
            cloud.x -= cloud.GetSpeed();

            if ( cloud.x < -cloud.width * cloud.scaleX  )
            {
                 cloud.x = this.m_nStageWidth; 
                 cloud.y = ( this.m_nStageHeight - 200 ) * Math.random(); 
            }
        } // end for  
    }

    public CloudLoop_5():void
    {
          for ( var i:number = this.m_containerClouds.numChildren - 1; i >= 0; i-- )
        {
            var cloud:CObj = this.m_containerClouds.getChildAt(i) as CObj;
            cloud.x -= cloud.GetSpeed();

            if ( cloud.x < -cloud.width * cloud.scaleX  )
            {
                 cloud.x = this.m_nStageWidth; 
                 cloud.y = this.m_nMaxHeight * Math.random(); 
                 cloud.alpha = this.GetCurAlphaByPos_4( cloud.y );
            }
        } // end for  
    }

    public CloudLoop_4():void
    {
         for ( var i:number = this.m_containerClouds.numChildren - 1; i >= 0; i-- )
        {
            var cloud:CObj = this.m_containerClouds.getChildAt(i) as CObj;
            cloud.x -= cloud.GetSpeed();

            if ( cloud.x < -cloud.width * cloud.scaleX  )
            {
                 cloud.x = this.m_nStageWidth; 
                 cloud.y = this.m_nMaxHeight * Math.random(); 
                 cloud.alpha = this.GetCurAlphaByPos_4( cloud.y );
            }
        } // end for  
    }


    public CloudLoop_2()
    {
        for ( var i:number = this.m_containerClouds.numChildren - 1; i >= 0; i-- )
        {
            var cloud:CObj = this.m_containerClouds.getChildAt(i) as CObj;
            cloud.x -= cloud.GetSpeed();

            if ( cloud.x < -cloud.width * cloud.scaleX  )
            {
                 cloud.x = this.m_nStageWidth; 
                 cloud.y = ( this.m_nStageHeight - 200 ) * Math.random(); 
            }
        } // end for  
    }

    public CloudLoop_8():void
    {
        for ( var i:number = this.m_containerClouds.numChildren - 1; i >= 0; i-- )
        {
            var cloud:CObj = this.m_containerClouds.getChildAt(i) as CObj;
            cloud.x -= cloud.GetSpeed();

            if ( cloud.x < -cloud.width * cloud.scaleX  )
            {
                 cloud.x = this.m_nStageWidth; 
                 cloud.y =( this.m_nStageHeight - 200 ) * Math.random(); 
            }
        } // end for
    }

    public CloudLoop_1()
    {
        for ( var i:number = this.m_containerClouds.numChildren - 1; i >= 0; i-- )
        {
            var cloud:CObj = this.m_containerClouds.getChildAt(i) as CObj;
            cloud.x -= cloud.GetSpeed();

            if ( cloud.x < -cloud.width * cloud.scaleX  )
            {
                 cloud.x = this.m_nStageWidth; 
                 cloud.y =( this.m_nStageHeight - 400 ) * Math.random(); 
            }
        } // end for
    }

} // end class