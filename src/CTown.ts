
class CTown extends egret.DisplayObjectContainer {

static s_aryTempObjs:Array<CObj> = new Array<CObj>();

static s_objectTemp:Object = new Object();

protected m_nStageWidth:number = 0;
protected m_nStageHeight:number = 0;

protected m_dicDistricts:Object = new Object();

private m_containerGrids:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
private m_containerObjs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
private m_containerRoads:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
private m_containerTrees:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
private m_containerArrows:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
private m_containerTurbine:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
private m_containerProgressbar:egret.DisplayObjectContainer = null;// new egret.DisplayObjectContainer();
private m_containerDiBiao:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();



public m_CurDistrict:CDistrict = null;
protected m_containerDistrict:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
protected m_aryAllDistricts:Array<CDistrict> = new Array<CDistrict>();

protected m_CloudManager:CCloudManager = null;

protected m_dicFunctionalBuildings:Object = new Object();

// 临时容器。正式版汽车和建筑必须放在同一个显示容器中，不然无法做遮挡排序
protected m_containerAutomobiles:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
protected m_aryAutomobiles:Array<CAutomobile> = new Array<CAutomobile>();

protected m_dicObjContainer:Object = new Object();

private m_dicGrids:Object = new Object();
protected m_dicRectsForTiles:Object = new Object();
protected m_aryAllGrids:Array<CGrid> = new Array<CGrid>();

private m_eCurOperate:Global.eOperate = Global.eOperate.edit_obj;

protected m_szName:string = "";

protected m_objCurProcessingLot:CObj = null;

protected m_dicEmptyLotNum:Object = new Object();
protected m_dicBulildLotNum:Object = new Object(); // 不包括 市政厅、车站 等非自建建筑
protected m_dicPropertyPercent:Object = new Object(); // 各种性质（住宅、商业、服务）的地块各自占的比例

protected  m_nShowCoinChangeSpeed:number = 0;
protected  s_nCPS:number = 0; // “Coin per Second”每秒产出的金币总数
protected  s_nCPS_WithOutDoubleTime:number = 0; // “Coin per Second”每秒产出的金币总数, 不含DoubleTime加成
protected  s_nCoins:number = 0; // 当前金币数量
protected  s_nPopulation:number = 0; // 当前人口
protected  m_nCurCPSableBuildingNum:number = 0; // 当前能产金币的建筑物个数

//// 当前各种因素的金币产出率
protected m_nBuildingCPS:number = 0;   // 建筑的金币产出率
protected m_nPopulationCPS:number = 0; // 人口的金币产出率

protected m_dicGarageInfo:Object = new Object();

protected m_aryUpgradableBuildings:Array<CObj> = new Array<CObj>();
protected m_nAutoUpgradeIndex:number = 0;
protected m_objUpgradingLot:CObj = null;

protected m_aryCanUpgradeLots:Array<CObj> = new Array<CObj>();
protected m_aryUpgradingLots:Array<CObj> = new Array<CObj>();
protected m_nSimultaneousUpgradeNum:number = 1;

protected m_dicCityHallData_City:Object = new Object();
protected m_dicCityHallData_Game:Object = new Object();

protected m_dicCityHallItemAffect:Object = new Object();


protected m_BankData:CBankData = new CBankData();

protected m_nID:number = 1;

protected m_objTapTownPos:Object = new Object();

protected m_objectNeighbourDistricts:Object = new Object();

public XingNengCeShi():void
{
        /*
                4个“2x2”地块为一组建筑块
                1个“4x4”地块为一组建筑块
                20个道路块包围一组建筑块。
        */
       
       /*
        for ( var i:number = 0; i < 200; i++ )
        {
             var obj:CObj = CResourceManager.NewObj();
             obj.SetTexture( RES.getRes( "dikuai_png" ) );   
             this.addChild( obj );
             obj.x = 300;
             obj.y = 300;
             
        }
        */
    
}

public constructor( nTownId:number, nStageWidth:number, nStageHeight:number ) {
        super();

        this.m_nID =  nTownId;
        this.m_nStageWidth = nStageWidth;
        this.m_nStageHeight = nStageHeight;

        // obj的容器
        this.m_dicObjContainer = new Object();
        for ( var i:number = 0; i < Global.eOperate.num; i++ )
        {
         this.m_dicObjContainer[i] = new Array<string>();
        } // end i
        /*
        this.m_dicObjContainer[Global.eOperate.add_road_corner_shang] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_corner_xia] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_corner_you] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_corner_zuo] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_youshangzuoxia] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_zuoshangyouxia] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_tile_2x2] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_tile_4x4] = new Array<string>();
        */
      

this.scaleX = 0.5;
this.scaleY = 0.5;
this.x  = 321;
this.y  = 460;

        this.addChild( this.m_containerDistrict );
/*
        this.addChild( this.m_containerGrids);

        this.addChild( this.m_containerDiBiao );
        this.addChild( this.m_containerRoads );
        this.addChild( this.m_containerTrees );
        this.addChild( this.m_containerObjs );
        this.addChild( this.m_containerTurbine );
        this.addChild( this.m_containerArrows );
        */
        //this.addChild( this.m_containerAutomobiles );
      

     

    //  this.CreateGrids();


       

     //   this.addChild( CJinBiManager.s_containerJinBi );
      //  this.addChild( CTiaoZiManager.s_lstcontainerTiaoZi );
     //   this.addChild( this.m_containerProgressbar );
       
} // end constructor

public AddProgressBar( district:CDistrict, bar:CUIBaseProgressBar ):void
{
        if ( district != this.m_CurDistrict )
        {
                return;
        }

       this.m_containerProgressbar.addChild(  bar);
}

        public Zoom( fScaleChange:number ):void
        {
                var fCurScale:number = this.scaleX;
                fCurScale *= ( 1 + fScaleChange );
                this.scaleX = fCurScale;  
                this.scaleY = fCurScale;           
        }

        protected m_bZooming:boolean  = false;
        protected m_nZoomChange:number = 0;
        protected m_fZoomTimeElapse:number = 0;
       public BeginZoom( nChange:number ):void
       {
               if ( this.m_bZooming )
               {
                       return;
               }

               if ( this.m_nZoomChange == nChange 

               )
               {
                       return;
               }
this.m_nZoomChange = nChange ;
this.m_bZooming = true;
this.m_fZoomTimeElapse = 0;

       } 


       public  ZoomLoop():void
       {
               if ( !this.m_bZooming )
               {
                       return;
               }

               if ( this.m_nZoomChange > 0 )
               {
                   this.Zoom( 0.06 );     
               }
               else
               {
                        this.Zoom( -0.06 );     
               }

               this.m_fZoomTimeElapse += CDef.s_fFixedDeltaTime;
               if ( this.m_fZoomTimeElapse >= 0.3 )
               {
                       this.EndZoom();
               }
       }

        public EndZoom():void
        {
this.m_bZooming = false;
this.m_nZoomChange = 0;
        }


public Reset():void
{
        this.m_aryUpgradableBuildings.length = 0;
        this.m_nAutoUpgradeIndex = 0;
        this.m_objUpgradingLot = null;
        
        
}

public GetId():number
{
        return this.m_nID;
}

public  Init( nTownId:number  ):void
{

        this.m_nID = nTownId;

        var config:CCofigCityLevel = CConfigManager.GetCityLevelConfig( Main.s_CurTown.GetLevel() );
        Main.s_CurTown.SetCoins( config.nStartCoins, true );

        CUIManager.s_uiMainTitle.SetCityName( config.szName );

       this.m_CloudManager.InitClouds( this.m_nID, this.m_nStageWidth, this.m_nStageHeight);
       
        

     // 初始化汽车的信息
      for ( var i:number = 0; i < CConfigManager.s_nTotalCarNum; i++ )
        {
                this.m_dicGarageInfo[i] = Global.eGarageItemStatus.locked_and_can_unlock;
                
        }   


     // 初始化银行信息    
     this.m_BankData.SetCurLevel( 1 );
     this.m_BankData.nCurProfitRate = CConfigManager.GetBankProfitRate();

     // poppin to do
     // CityHall中Game页第一条可以对银行的时间进行加成

   }

public GetBankData():CBankData
{
        return this.m_BankData;
}

protected m_szTownData:string = "";



public LoadTownData( nTownId:number ):void
{
     this.m_CurDistrict = null;  

     this.m_nID =    nTownId;
     this.m_nSimultaneousUpgradeNum = this.m_nID;
     console.log( "当前可同时升级的地块数最大值：" + this.m_nSimultaneousUpgradeNum  );

     CUIManager.s_uiMainTitle.SetCityId( this.m_nID );
      
 // var szData:string = egret.localStorage.getItem("Town" + nTownId);
   this.m_szTownData = CDataManager.GetMapData(nTownId - 1);
   // console.log( "读档：" + szData );

    this.ChangeDistrict(1);

    if ( this.m_szTownData == null)
    {
        return;
    }
    
/*
    var aryData:string[] = szData.split( '|' );
   
    for ( var i:number = 0; i < aryData.length; i++ )
    {
       
        if ( aryData[i] == "" )
        {
                continue;
        }
        var op:number = i;
        var szGridIds:string = aryData[i];

        var aryGridIs:string[] = szGridIds.split( ',' );
        for ( var j:number = 0; j < aryGridIs.length; j++ )
    {
                switch( op)
                {
                     

                        case Global.eOperate.add_tree:
                        {
                               var aryParams:string[] = aryGridIs[j].split( '_' );
                               var nTreeResId:number = (Number)(aryParams[0]);
                               var fPosInTownX:number = (Number)(aryParams[1]);
                               var fPosInTownY:number = (Number)(aryParams[2]);
                               this.DoAddTree( nTreeResId, fPosInTownX, fPosInTownY );
                        }
                        break;

                        case Global.eOperate.add_windturbine:
                        {
                                var aryParams:string[] = aryGridIs[j].split( '_' );
                               var fPosInTownX:number = (Number)(aryParams[0]);
                               var fPosInTownY:number = (Number)(aryParams[1]);
                               this.DoAddWindTurbine(  fPosInTownX, fPosInTownY ); 
                        }
                        break;

                        default:
                        {
                                var aryGridIndex:string[] = aryGridIs[j].split( '_' );
                                var szIndexX:string = aryGridIndex[0]; 
                                var szIndexY:string = aryGridIndex[1];
                                var grid:CGrid = this.GetGridByIndex( szIndexX, szIndexY );
                                this.DoAddObj( grid, op );
                                if ( op == 5 )
                                {
                                        console.log("我日你个龟");
                                }
                        }
                        break;
                }





        } // end j
    } // end i
*/
 

    this.Init( nTownId );
}

 protected LoadData_EmptyGridType( op:Global.eOperate, szGridIds:string ):void
 {
      
        var aryGridIs:string[] = szGridIds.split( ',' );
        for ( var j:number = 0; j < aryGridIs.length; j++ )
        {
                var aryGridIndex:string[] = aryGridIs[j].split( '_' );
                var szIndexX:string = aryGridIndex[0];
                
                var szIndexY:string = aryGridIndex[1];
                var grid:CGrid = this.GetGridByIndex( szIndexX, szIndexY );
             
             /*
                if ( op == Global.eOperate.set_inside_empty_grid )
                {

                        grid.SetEmptyGridType( Global.eEmptyGridType.inside_city );
                }else if ( op == Global.eOperate.set_edge_empty_grid)
                {
                        
                        grid.SetEmptyGridType( Global.eEmptyGridType.city_edge );
                }
                */

        } // end j 
 }       


 protected m_aryNeighbourGrids:Array<number> = new Array<number>();
public SeekNeighbourGrids(  nIndexX:number, nIndexY:number, nHierarchy:number ):boolean
{
    this.m_aryNeighbourGrids.length = 0;

    var bExceedTownBorder:boolean = false;

            for ( var k:number = 0; k >= -nHierarchy;  k-- )
                                        {
                                                for ( var l:number = 0; l <= nHierarchy; l++  )
                                                {
                                                     var nNeighbourIndexX:number = nIndexX + k + l;
                                                     var nNeighbourIndexY:number = nIndexY + k - l;   
                                                    // console.log( "seek neighbour:" +  nNeighbourIndexX + "_" + nNeighbourIndexY);
                                                      var grid_neighbor:CGrid = this.GetGridByIndex( nNeighbourIndexX.toString(), nNeighbourIndexY.toString() );
                                                    
                                                      if ( grid_neighbor == undefined ) // 没找到grid
                                                        {
                                                                  console.log( nNeighbourIndexX + "," + nNeighbourIndexY + "在边界外" ); 
                                                    
                                                                return false;
                                                         
                                                       
                                                        }      
                                                        else
                                                        {
                                                             this.m_aryNeighbourGrids.push(nNeighbourIndexX);
                                                             this.m_aryNeighbourGrids.push(nNeighbourIndexY);
                                                        }
                                                } // end l
                                              
                                        }        // end k

                                       
                       
                       return true;
}

public EditObj(stageX:number, stageY:number)
{
       
}

public DelObj( obj:CObj ):void
{
        if ( obj.GetFuncType() == Global.eObjFunc.road )
        {
            this.m_containerRoads.removeChild( obj );
        }
        else
        {
            this.m_containerObjs.removeChild( obj );
        }
      CObjManager.DeleteObj( obj );
}

public DoAddWindTurbine( fPosInTownX:number,  fPosInTownY:number):void
{
         var turbine:CWindTurbine = new CWindTurbine();
             this.m_containerTurbine.addChild( turbine );
             turbine.anchorOffsetX = turbine.width / 2;
             turbine.anchorOffsetY = turbine.height;
             turbine.x = fPosInTownX;
             turbine.y = fPosInTownY;
             turbine.touchEnabled = true;
             turbine.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapTurbine, this );
}

public DoAddArrow( operate:Global.eOperate, fPosInTownX:number, fPosInTownY:number ):void
{
        var arrow:CObj =  CResourceManager.NewObj();
        this.m_containerArrows.addChild( arrow );
        arrow.x = fPosInTownX;
        arrow.y = fPosInTownY;
        arrow.SetTexture( RES.getRes( "DistrictArrow_jpeg" ) );
         arrow.anchorOffsetX = arrow.width / 2;
        arrow.anchorOffsetY = arrow.height / 2;
        arrow.scaleX = 0.3;
         arrow.scaleY= 0.3;

         arrow.SetParams( "DistrictDir", Global.eOperate.add_arrow_zuoshang );

         switch( operate )
         {
        case Global.eOperate.add_arrow_zuoshang:
        {
                arrow.rotation = -40;
        }
        break;

                case Global.eOperate.add_arrow_zuoxia:
        {
                arrow.rotation = -135;
        }
        break;

         case Global.eOperate.add_arrow_youshang:
        {
                arrow.rotation = 45;
        }
        break;

               case Global.eOperate.add_arrow_youxia:
        {
                arrow.rotation = 135;
        }
        break;
         } // end switch

         arrow.addEventListener( egret.TouchEvent.TOUCH_TAP, this.handleTapDistrictArrow, this );   

}

public handleTapDistrictArrow( evt:egret.TouchEvent ):void
{
        var arrow:CObj = evt.currentTarget as CObj;
        if ( this.m_eCurOperate == Global.eOperate.del_obj )
        {
                CResourceManager.DeleteObj( arrow );
        }
        else
        {
              this.GotoDistrcit( arrow.GetParamsByKey( "DistrictDir" ) );
        }
}

public GotoDistrcit( dir:Global.eOperate ):void
{

}

public DoAddTree( nTreeId:number, fPosInTownX:number, fPosInTownY:number ):void
{
        var tree:CObj = CResourceManager.NewObj();
        this.m_containerTrees.addChild( tree );
        tree.x = fPosInTownX;
        tree.y = fPosInTownY;
        tree.SetTexture( CResourceManager.GetTreeTextureByTownId( this.m_nID ) /*RES.getRes( "shu_png" )*/ );
        tree.anchorOffsetX = tree.width / 2;
        tree.anchorOffsetY = tree.height;
        tree.scaleX = 0.5;
        tree.scaleY = 0.5;
}

public DoAddObj( grid:CGrid, op:Global.eOperate  ):void
{  
       
             


var szResName = "";

                          var nIndexX:number = grid.GetId()["nIndexX"];
                        var nIndexY:number = grid.GetId()["nIndexY"];
                       // console.log( "点中了：" + nIndexX + "_" + nIndexY );
                        var bCanAddObj:boolean = true;
                        
                      


                        switch(op)
                        {
                                        case Global.eOperate.add_cityhall: // 放置“市政厅”
                                        case Global.eOperate.add_bank: // 放置“银行”
                                        case Global.eOperate.add_garage: // 放置“车站”
                                        {
                                 
                                 /*
                                                var objFunctionalBuilding:CObj =  this.m_dicFunctionalBuildings[op];
                                                if ( objFunctionalBuilding == null || objFunctionalBuilding == undefined )
                                                {
                                                        objFunctionalBuilding = CResourceManager.NewObj();
                                                }
                                                */
                                                var objFunctionalBuilding:CObj = CResourceManager.NewObj();
                                           objFunctionalBuilding.SetLocationGrid( grid );
                                               objFunctionalBuilding.SetBoundGrid( grid );
                                                objFunctionalBuilding.SetFuncType( Global.eObjFunc.building );
                                                objFunctionalBuilding.SetOperateType( op );
                                                objFunctionalBuilding.SetSizeType( Global.eObjSize.size_2x2 );
                                               objFunctionalBuilding.SetSortPosY();
                                         //      console.log( op+ "__" + objFunctionalBuilding.GetSortPosY() + "__" + grid.GetPos()["y"] );
                                                this.InsertObj( objFunctionalBuilding );
                                                var bRet:boolean =  objFunctionalBuilding.ProcessAddSpecialBuilding(op);
                                                 this.m_dicFunctionalBuildings[op] = objFunctionalBuilding;
                                                 var gridX:number = grid.GetPos()["x"];
                                                 var gridY:number = grid.GetPos()["y"];
                                        

                                                 objFunctionalBuilding.SetPos( gridX, gridY + CDef.s_nTileHeight * 0.5 );
                                       
                                               grid.SetBoundObj( objFunctionalBuilding );
                                                grid.SetZhiNengJianZhu( true );
                                        }
                                        
                                        break;

                                case Global.eOperate.add_road_youshangzuoxia:
                                case Global.eOperate.add_road_zuoshangyouxia:
                                case Global.eOperate.add_road_corner_shang:
                                case Global.eOperate.add_road_corner_xia:
                                case Global.eOperate.add_road_corner_you:
                                case Global.eOperate.add_road_corner_zuo:
                                /*
                                case Global.eOperate.add_road_cross:
                                case Global.eOperate.add_road_t_youshang:
                                case Global.eOperate.add_road_t_youxia:
                                case Global.eOperate.add_road_t_zuoshang:
                                case Global.eOperate.add_road_t_zuoxia:
                                */
                                {
                                      
                                      if ( op == Global.eOperate.add_road_zuoshangyouxia ||
                                           op == Global.eOperate.add_road_youshangzuoxia
                                         )
                                         {
                                             //   szResName = "malu_zuoshangyouxia_png";  
                                             if ( this.GetId() == 2 )
                                             {
                                                szResName = "road2_youshangzuoxia_png";
                                             }
                                             else
                                             {
                                                szResName = "road1_youshangzuoxia_png";
                                             }
                                         }

                                        if ( op == Global.eOperate.add_road_corner_shang || 
                                      op == Global.eOperate.add_road_corner_xia
                                         )                
                                         {
                                           //    szResName = "malu_chuizhizhuanzhe_png";  
                                          if ( this.GetId() == 2 )
                                             {
                                                szResName = "road2_corner_shang_png";  
                                             }
                                             else
                                             {
                                                      szResName = "road1_corner_shang_png"; 
                                             }
                                              
                                         }
                                         /*
                                         if (  op == Global.eOperate.add_road_corner_xia)
                                         {
                                                 szResName = "malu_chuizhizhuanzhe_png";  
                                         }
                                         */
                                                
                                        if (  op == Global.eOperate.add_road_corner_zuo ||
                                                op == Global.eOperate.add_road_corner_you
                                                )
                                         {

                                                 //szResName = "malu_shuipingzhuanzhe_png";  
                                                  if ( this.GetId() == 2 )
                                                {
                                                szResName = "road2_corner_you_png";  
                                                }
                                                else
                                                {
                                                         szResName = "road1_corner_you_png";  
                                                }
                                         }


                                         
                                       var dic:Object = grid.GetPos();
                                       
                                        var obj:CObj = CObjManager.NewObjByResName( szResName);
                                        if ( this.GetId() == 2 )
                                        {
                                                obj.SetColor( 144, 102, 85 );
                                        }
                                        else
                                        {
                                                obj.SetColor( 174, 170, 184 );
                                        }
                                        obj.anchorOffsetX = obj.width / 2;
                                        obj.anchorOffsetY = obj.height / 2;
                                         obj.SetLocationGrid( grid );
                                       // this.InsertObj( obj, op );
                                       this.m_containerRoads.addChild( obj );
                                         obj.SetOperateType( op );
                                        obj.SetPos( dic["x"], dic["y"]/* - (obj.height * 0.25)*/ );
                                        obj.SetTown( this );
                                        obj.SetFuncType( Global.eObjFunc.road );
                                        obj.scaleX = 0.5;
                                        obj.scaleY = 0.5;
                                        if ( op == Global.eOperate.add_road_zuoshangyouxia )
                                        {
                                             
                                             obj.scaleX = -0.5;   
                                        }
                                        else if( op == Global.eOperate.add_road_corner_zuo )
                                        {
                                            
                                              obj.scaleX = -0.5;   
                                        }
                                        else if ( op == Global.eOperate.add_road_corner_xia )
                                        {
                                                obj.scaleY = -0.5;    
                                        }
                                       
                 // 物件和格子互相绑定
                                        
 // poppin test 8.25
                                      grid.ClearBoundObj(); // 如果格子上当前已经摆放了物件，则自动清除当前物件
                                         grid.SetBoundObj( obj );
                                         obj.SetBoundGrid( grid );
                                        

                                         obj.SetSizeType( Global.eObjSize.size_1x1 );
                                         obj.SetFuncType( Global.eObjFunc.road );
                                         obj.SetSortPosY();
                                }
                                break;

                                case Global.eOperate.add_dibiao: // 地表
                                {
                                
                                    var dibiao:CObj = CResourceManager.NewDiBiao();
                                    dibiao.SetTexture(  CResourceManager.GetDiBiaoTextureByTownId( this.GetId() ) );   
                                    dibiao.anchorOffsetX = dibiao.width / 2;
                                    dibiao.anchorOffsetY = 0;
                                    dibiao.SetPos( grid.GetPos()["x"], grid.GetPos()["y"] );
                                    this.InsertDiBiao( dibiao );
                                    dibiao.SetLocationGrid( grid );
                                  
                                    if ( CDef.EDITOR_MODE )
                                    {
                                   
                                            dibiao.touchEnabled = true;
                                        dibiao.addEventListener( egret.TouchEvent.TOUCH_TAP, this.handleTapDibiao, this );   
                                }
                                
                                }
                                break;
                


                                case Global.eOperate.add_tile_2x2: // 2x2型宗地
                                case Global.eOperate.add_tile_4x4:// 4x4型宗地
                                {
                                    var nHierarchy:number = 1;
                                    if (op ==Global.eOperate.add_tile_2x2 )
                                    {
                                        szResName= "dikuai_png";
                                        nHierarchy = 1;
                                    }
                                    else if ( op ==Global.eOperate.add_tile_4x4 )
                                    {
                                            szResName= "dikuai_png";
                                        nHierarchy = 3;
                                    }
                                    bCanAddObj = this.SeekNeighbourGrids(nIndexX, nIndexY, nHierarchy);
                             
                                  
                                    if ( !bCanAddObj )
                                    {
                                            Main.s_panelMsgBox.ShowMsg( "占地超过边界，不能放置" );
                                            return;
                                    }    
  
                                  

                                        var dic:Object = grid.GetPos();
                                       
                                        var obj:CObj = CObjManager.NewObjByResName( szResName);
                                         if (op ==Global.eOperate.add_tile_2x2 )
                                         {
                                                 obj.scaleX = 0.35;
                                                 obj.scaleY = 0.35;
                                                      obj.SetPos( dic["x"], dic["y"] + CDef.s_nTileHeight * 0.2); 
                                         }
                                         else{
                                                 obj.scaleX = 0.75;
                                                 obj.scaleY = 0.75;
                                                      obj.SetPos( dic["x"], dic["y"]);
                                         }
                                        obj.anchorOffsetX = obj.width  / 2;
                                        obj.anchorOffsetY = obj.height;
                                         obj.SetLocationGrid( grid );
                                       
                                        obj.SetOperateType( op );
                            
                                   
                                        obj.SetTown( this );
                                        obj.SetFuncType( Global.eObjFunc.building );
                                       
                                        obj.SetBuildingLandStatus( Global.eBuildingLandStatus.empty );
                                      //  obj.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapBuildingLand, this );


                                        if (op ==Global.eOperate.add_tile_2x2 )
                                        {
                                                 obj.SetSizeType( Global.eObjSize.size_2x2 );
                                        }
                                        else if ( op ==Global.eOperate.add_tile_4x4 )
                                        {
                                               obj.SetSizeType( Global.eObjSize.size_4x4 );
                                        }
                                        obj.SetFuncType( Global.eObjFunc.building );
                                        obj.SetSortPosY();
                                            this.InsertObj( obj );                            
                                     for ( var k:number = 0; k < this.m_aryNeighbourGrids.length - 1; k+=2 )
                                    {
                                         var related_grid:CGrid = this.GetGridByIndex( this.m_aryNeighbourGrids[k].toString() , this.m_aryNeighbourGrids[k+1].toString() );
                                         if ( related_grid == null || related_grid == undefined )
                                         {
                                               //  console.log( "有Bug" );
                                                 continue;
                                         }

                                         // 物件和格子互相绑定
                                         if ( grid.GetZhiNengJianZhu() )
                                         {
                                                 console.log( "不得哦111111111" );
                                         }

                                    // poppin test 8.25
                                        related_grid.ClearBoundObj(); // 如果格子上当前已经摆放了物件，则自动清除当前物件
                                         related_grid.SetBoundObj( obj );
                                         obj.SetBoundGrid( related_grid );
                                          
                                          // right here 9.15


                                    }

                                  //  console.log( "当前回收的obj:" + CObjManager.m_lstRecycledObjs.numChildren );
                                }
                                break;

                                
                             
                        } // end switch

                       
                        


                        return;

}

protected m_lstNeighbourGrids:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
public AddObj(grid:CGrid ,  op:Global.eOperate  ):void
{ 
        
       this.m_lstNeighbourGrids.removeChildren();
     
                        if ( grid == null )
                        {
                                return;
                        }
                       
                         this.DoAddObj( grid, op );
                    
                        
}

public GetOperate():Global.eOperate
{
        return this.m_eCurOperate;
}

public SetOperate( eOperate:Global.eOperate ):void
{
        this.m_eCurOperate = eOperate;

        if ( this.m_eCurOperate == Global.eOperate.edit_obj )
        {
              //  Main.s_panelObjEditPanel.Show();
             
        }
        else if ( this.m_eCurOperate == Global.eOperate.del_obj )
        {
              //  Main.s_panelObjEditPanel.Show();
        }
        /*
        else if ( this.m_eCurOperate == Global.eOperate.add_automobile )
        {
               // this.AddAutomobile();
               CUIManager.SetUiVisible( Global.eUiId.garage, true );
               this.UpdateGarageStatus();

        }
        */
        else if (this.m_eCurOperate == Global.eOperate.add_diamond)
        {
                CPlayer.SetDiamond( CPlayer.GetDiamond() + 1 );
        }
        else if (this.m_eCurOperate == Global.eOperate.add_coin)
        {
                CPlayer.SetMoney(Global.eMoneyType.coin,  CPlayer.GetMoney( Global.eMoneyType.coin ) + 10000 );
        }
        else if (this.m_eCurOperate == Global.eOperate.add_dibiao)
        {
               
        }
        else if ( this.m_eCurOperate == Global.eOperate.toggle_grid )
        {
                this.ToggleGrids();
        }
        else 
        {
               // this.AddObj( this.stage.stagx, this.stage.y,  eOperate);
        }
}

public Processtap( grid:CGrid, fPosInTownX:number, fPosInTownY:number ):void
{

        if (this.m_eCurOperate == Global.eOperate.edit_obj )
        { 
          
               var obj:CObj = grid.GetBoundObj();
               if ( obj != null )
               {
 obj.ProcessEdit();
               }
               
        }
        else if (this.m_eCurOperate == Global.eOperate.add_tree)
        {
             
                this.DoAddTree( 0, fPosInTownX, fPosInTownY );
        }
        else if (   this.m_eCurOperate ==   Global.eOperate.add_arrow_zuoshang || 
                    this.m_eCurOperate ==  Global.eOperate.add_arrow_zuoxia ||
                    this.m_eCurOperate ==  Global.eOperate.add_arrow_youshang ||
                    this.m_eCurOperate ==  Global.eOperate.add_arrow_youxia 
        )
                        {
                                this.DoAddArrow(  this.m_eCurOperate , fPosInTownX, fPosInTownY );
                        }
                     
        else if (this.m_eCurOperate == Global.eOperate.del_obj )
        {
            
             var obj:CObj = grid.GetBoundObj();
             if ( obj != null  )
             {
                                      // 物件和格子互相绑定
                                         if ( grid.GetZhiNengJianZhu() )
                                         {
                                                 console.log( "不得哦22222222" );
                                         }
                grid.ClearBoundObj();
             }
        
        }
        else if ( this.m_eCurOperate == Global.eOperate.add_windturbine ) // 添加风车
        {


                this.DoAddWindTurbine(fPosInTownX, fPosInTownY);
                /*
             var turbine:CWindTurbine = new CWindTurbine();
             this.m_containerTurbine.addChild( turbine );
             turbine.anchorOffsetX = turbine.width / 2;
             turbine.anchorOffsetY = turbine.height;
             var objPosInTown:Object = this.StagePos2TownPos( stageX, stageY );
             turbine.x = objPosInTown["x"];
             turbine.y = objPosInTown["y"];
             turbine.touchEnabled = true;
             turbine.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapTurbine, this );
             */
        }
        else
        {
                this.AddObj( grid, this.m_eCurOperate );
        }
        
            

       
}

protected m_nGridGuid:number = 0;
public CreateGrids():void
{



        var nWidth:number = CDef.s_nTileWidth;
        var nHeight:number = CDef.s_nTileHeight;
        var nHalfWidth:number = CDef.s_nTileWidth / 2;
        var nHalfHeight:number = CDef.s_nTileHeight / 2;

// 规定：偶数行的列号都是偶数；奇数行的列号都是奇数
for ( var i:number = -CDef.s_nGridNumY; i <= CDef.s_nGridNumY; i++ ) // y轴方向（行）
{
        var nShit = Math.abs(i);
        for ( var j:number = -CDef.s_nGridNumX + nShit; j <= CDef.s_nGridNumX - nShit; j++ )// x轴方向（列）
        {
                var posX:number = 0;
                var posY:number = 0;
                
                 if ( i % 2 == 0 ) // 偶数行
                  {
                       if ( j % 2 != 0  ) // 不是偶数列就跳过。因为地图的格子机制规定偶数行只有偶数编号的列
                       {
                               continue;
                       }

                       posX = ( j / 2 ) * nWidth;
                       posY = i * nHalfHeight; 
                  }
                  else// 奇数行（在奇数行列处生成逻辑矩形）
                  {
                        if ( j % 2 == 0  ) // 是偶数列就跳过。因为地图的格子机制规定奇数行只能有奇数编号的列
                       {
                               continue;
                       }
                        posX = ( j / 2 ) * nWidth;
                        posY = i * nHalfHeight; 
                  }

                  var grid:CGrid = new CGrid();
                  grid.SetGuid(this.m_nGridGuid++);

                  if ( CDef.EDITOR_MODE )
                  {
                 
                        this.m_containerGrids.addChild( grid );
                  }
             
                  grid.anchorOffsetX = nHalfWidth;
                  grid.anchorOffsetY = nHalfHeight; //;CDef.s_nTileHeight;
                  //grid.DrawRhombus( CDef.s_nTileWidth, CDef.s_nTileHeight, nHalfWidth, nHalfHeight);
                  //grid.x = posX;
                  //grid.y = posY;
                  grid.SetPos( posX, posY );

                  // j是x轴坐标，i是y轴坐标
                  var szKey:string = j + "," + i;
                  grid.SetText( szKey);
                  grid.SetId( j,i );
                  this.m_dicGrids[szKey] = grid;
                  this.m_aryAllGrids.push( grid );

               
        } // end for j
} // end for i    
   
   



} // end CreateGrids

protected m_bTapDown:boolean = false;
public handleTapGridDown():void{
        this.m_bTapDown = true;
}

public handleTapGridMove():void{
        this.m_bTapDown = false;
}

public handleTapGridUp( evt:egret.TouchEvent ):void
{
        if ( !this.m_bTapDown )
        {
                return;
        }

      var real_tap_grid = null;
      var grid:CGrid = evt.currentTarget as CGrid;
      var nIndexX:number = grid.GetId()["nIndexX"];
      var nIndexY:number = grid.GetId()["nIndexY"];

        var nIndexX_Neighbour:number = 0;
        var nIndexY_Neighbour:number = 0;
        var grid_neighbour:CGrid = null;

      // 先判断是不是真的点中自己了。因为这种点击事件是按矩形包围盒来判断的，很容易点到旁边的物体
      var bHit:boolean = grid.hitTestPoint( evt.stageX ,evt.stageY, true );
      if ( !bHit ) // 触发事件的格子并没（像素级）点中
      {
        // 判断是不是点中了其左上角的格子
        nIndexX_Neighbour = nIndexX - 1;
        nIndexY_Neighbour = nIndexY - 1;
        grid_neighbour = this.GetGridByIndex( nIndexX_Neighbour.toString(), nIndexY_Neighbour.toString() );
        if ( grid_neighbour == null || grid_neighbour == undefined )
        {
                bHit = false;
        }else
        {
                bHit = grid_neighbour.hitTestPoint( evt.stageX ,evt.stageY, true );
                if ( !bHit )
                {
                        grid_neighbour = null;

                        // 判断是不是点中了其右上角的格子
                        nIndexX_Neighbour = nIndexX + 1;
                        nIndexY_Neighbour = nIndexY - 1;
                        grid_neighbour = this.GetGridByIndex( nIndexX_Neighbour.toString(), nIndexY_Neighbour.toString() );
                        if ( grid_neighbour == null || grid_neighbour == undefined )
                        {
                                bHit = false;
                        }else
                        {
                                bHit = grid_neighbour.hitTestPoint( evt.stageX ,evt.stageY, true );
                                if ( !bHit )
                                {

                                }
                                else
                                {
                                         real_tap_grid = grid_neighbour;
                                }
                        }
                }
                else
                {
                   real_tap_grid = grid_neighbour;
                }
        }
      }
      else
      {
              real_tap_grid = grid;
      }

      if ( real_tap_grid == null || real_tap_grid == undefined )
      {
              console.log( "有bug，怎么会没点中格子" );
              return;
      }

      real_tap_grid.Processtap();
}


public GetGridByIndex( nIndexX:string, nIndexY:string ):CGrid
{
        var szKey:string = nIndexX + "," + nIndexY;
        var grid:CGrid = this.m_dicGrids[szKey];   
        if ( grid == undefined )
        {
               grid = null; 
        }   
        return grid;
}


private m_fTapBeginMouseX:number = 0;
private m_fTapBeginMouseY:number = 0;
private handleTapGrid_Begin( evt:egret.TouchEvent ):void
{
        this.m_fTapBeginMouseX = evt.stageX;
        this.m_fTapBeginMouseY = evt.stageY;       
}

public ReInsertSortObj(obj:CObj):void
{
        this.m_containerObjs.removeChild( obj );
        this.InsertObj( obj );
}

protected InsertDiBiao( obj:CObj  ):void
{
      var bInserted:boolean = false;
      for ( var i:number = this.m_containerDiBiao.numChildren - 1; i >= 0; i--  )
      {
           var node_obj:CObj = this.m_containerDiBiao.getChildAt(i) as CObj;
           
           if ( obj.y >= node_obj.y )
           {
                   this.m_containerDiBiao.addChildAt( obj, i + 1);
                
                   bInserted = true;
                   return;
           }
      }
      if ( !bInserted )
      {
        this.m_containerDiBiao.addChildAt(  obj, 0 );
         
      }
}

// “插入排序法”和“二分查找法”叠加使用，使性能达到最高
// to youhua 记得用二分查找法. 目前这样全遍历性能太低
private InsertObj( obj:CObj ):void
{
      //  this.m_containerObjs.addChild(obj); 
      var bInserted:boolean = false;
      for ( var i:number = this.m_containerObjs.numChildren - 1; i >= 0; i--  )
      {
           var node_obj:CObj = this.m_containerObjs.getChildAt(i) as CObj;
           
           if ( obj.GetSortPosY() >= node_obj.GetSortPosY() )
           {
                   this.m_containerObjs.addChildAt( obj, i + 1);
                   obj.SetDebugInfo( (i + 1).toString() );
                   bInserted = true;
                   return;
           }
      }
      if ( !bInserted )
      {
        this.m_containerObjs.addChildAt(  obj, 0 );
         obj.SetDebugInfo( "0" );
      }
       
}

public  DragScene( fDeltaX:number, fDeltaY:number ):void
{
        if ( isNaN( fDeltaX ) || isNaN(  fDeltaY ) )
        {
                return;
        }

        if ( Math.abs( fDeltaX ) > 20 || Math.abs( fDeltaY ) > 20 )
        {
                return;
        }
      
        this.x += fDeltaX;
        this.y += fDeltaY;


}

        public GenerateData():string
        {
                console.log( "存档又有" );

                 var nLastIndex:number = Global.eOperate.num - 1;

              var szData:string = "";
              for ( var i:number = 0; i <= nLastIndex; i++ )
              {
                      var ary:Array<string> = this.m_dicObjContainer[i];
                      ary.length = 0;
                      this.m_dicObjContainer[i] = ary;
              }  

           //   var ary_edge:Array<string> = this.m_dicObjContainer[Global.eOperate.set_edge_empty_grid];
           //   var ary_inside:Array<string> = this.m_dicObjContainer[Global.eOperate.set_inside_empty_grid];
              
              for ( var i:number = 0; i < this.m_containerGrids.numChildren; i++ )
              {
                    var grid:CGrid =   this.m_containerGrids.getChildAt(i) as CGrid;
                    if ( grid.IsTaken() )
                    {
                            continue;
                    }

/*
                    if ( grid.GetEmptyGridType() == Global.eEmptyGridType.inside_city )
                    {
                   
                        ary_inside.push(grid.GetIdByString());
                    }
                    else if ( grid.GetEmptyGridType() == Global.eEmptyGridType.city_edge )
                    {
       
                        ary_edge.push(grid.GetIdByString()); 
                    }
                    */
              }

              /// 风车
             var ary:Array<string> = this.m_dicObjContainer[Global.eOperate.add_windturbine];
                for ( var i:number = 0; i < this.m_containerTurbine.numChildren; i++  )
                {
                     var windturbine:CObj = this.m_containerTurbine.getChildAt(i) as CObj;
                     ary.push( windturbine.x + "_" + windturbine.y );
                }

              ///  树
                var ary:Array<string> = this.m_dicObjContainer[Global.eOperate.add_tree];
                for ( var i:number = 0; i < this.m_containerTrees.numChildren; i++  )
                {
                     var tree:CObj = this.m_containerTrees.getChildAt(i) as CObj;
                     ary.push( tree.GetTreeResId() + "_" + tree.x + "_" + tree.y );
                }
/*
              /// cityhall 市政厅
                 var ary_cityhall:Array<string> = this.m_dicObjContainer[Global.eOperate.add_cityhall];
                         var obj:CObj = this.m_dicFunctionalBuildings[Global.eOperate.add_cityhall];

                  ary_cityhall.push( obj.GetLocationGrid().GetIdByString() );    
                  console.log( "市政厅到底有几个：" + ary_cityhall.length );

              /// garage
           
                        var ary:Array<string> = this.m_dicObjContainer[Global.eOperate.add_garage];
                         var obj:CObj = this.m_dicFunctionalBuildings[Global.eOperate.add_garage];
            
                  ary.push( obj.GetLocationGrid().GetIdByString() );    
              

                /// bank
                 var ary:Array<string> = this.m_dicObjContainer[Global.eOperate.add_bank];
                         var obj:CObj = this.m_dicFunctionalBuildings[Global.eOperate.add_bank];

                  ary.push( obj.GetLocationGrid().GetIdByString() );    
              */


              // 地标
              for ( var i:number = 0; i < this.m_containerDiBiao.numChildren; i++ )
              {
                      var obj:CObj = this.m_containerDiBiao.getChildAt(i) as CObj;
                      var ary:Array<string> = this.m_dicObjContainer[Global.eOperate.add_dibiao];
                      ary.push( obj.GetLocationGrid().GetIdByString() );
                       
              }

              // 注意，这里面包含了市政厅、车站、银行 等特殊职能建筑
              for ( var i:number = 0; i < this.m_containerObjs.numChildren; i++ )
              {
                      var obj:CObj = this.m_containerObjs.getChildAt(i) as CObj;
                      var ary:Array<string> = this.m_dicObjContainer[obj.GetOperateType()];
                      ary.push( obj.GetLocationGrid().GetIdByString() );
                  
                    //  console.log( "op type=" + obj.GetOperateType() );
              }


               for ( var i:number = 0; i < this.m_containerRoads.numChildren; i++ )
              {
                      var obj:CObj = this.m_containerRoads.getChildAt(i) as CObj;
                      var ary:Array<string> = this.m_dicObjContainer[obj.GetOperateType()];
                      ary.push( obj.GetLocationGrid().GetIdByString() );
          
              }
              
            
              for ( var i:number = 0; i <= nLastIndex; i++ )
              {
                   

                    var ary:Array<string> = this.m_dicObjContainer[i];
       
                    for ( var j:number = 0; j < ary.length; j++ )
                    {
                        szData += ary[j];
                        if ( j != ary.length - 1 )
                        {
                                szData += ",";
                        }
                    }  // end j
                    if ( i != nLastIndex )
                    {
                            szData += "|";
                    }
              } // end i


              console.log( "存档：" + szData );
              return szData;  
        }

        public FixedUpdate_1():void
        {
           this.UpdateTotalCoins();

           this.CheckIfCanUpgrde();
        }

        public FixedUpdate():void
        {
                this.AutomobileLoop();
                this.BuildingLandLoop();
               // this.AutoUpgradeLoop();
                this.WindTurbineLoop();

                this.ZoomLoop();
        }

        // 把当前正在运行的汽车，从一个区迁移到另一个区 
        
        public MoveCarsFromOneDistrictToAnother( district_src:CDistrict, district_dest:CDistrict ):void
        {
                for ( var i:number = 0; i < this.m_aryAutomobiles.length; i++ )
                {
                        var car:CAutomobile = this.m_aryAutomobiles[i];
                        this.InsertObj( car );
                }
        }

        // 添加一个汽车
        protected m_aryTempRaods:Array<CObj> = new Array<CObj>();
        public AddAutomobile( nIndex:number ):void
        {
                this.m_aryTempRaods.length = 0;

                // 随机搜索一个路面，作为汽车的诞生点 to youhua  这里在加载地图时做一次就好了，重复做是白费性能
                var road:CObj = null;
                for ( var i:number = 0; i < this.m_containerRoads.numChildren; i++ )
                {
                        var obj:CObj = this.m_containerRoads.getChildAt(i) as CObj;
                        if ( obj.GetOperateType() == Global.eOperate.add_road_youshangzuoxia||
                                obj.GetOperateType() == Global.eOperate.add_road_zuoshangyouxia
                         )
                        {
                               this.m_aryTempRaods.push( obj );
                              // road = obj;
                        }
                }
                if ( /*road == null*/this.m_aryTempRaods.length == 0 )
                {
                        Main.s_panelMsgBox.ShowMsg( "必须建设有马路才能放置汽车" );
                        return;
                }

                var nShit:number = Math.floor( Math.random() * this.m_aryTempRaods.length ) ;
                if ( nShit < 0 || nShit >= this.m_aryTempRaods.length)
                {
                      nShit = 0;  
                }
                road = this.m_aryTempRaods[nShit];

                var car:CAutomobile = CAutomobileManager.NewAutomobile();
                car.SetCarIndex( nIndex );
                car.scaleX = 0.3;
                 car.scaleY = 0.3;
                this.m_aryAutomobiles.push( car );

                car.SetFuncType( Global.eObjFunc.car );

              
        
                if ( road.GetOperateType() == Global.eOperate.add_road_youshangzuoxia )
                {
                        car.UpdateDir( Global.eAutomobileDir.zuoxia );
                       
                }
                else if ( road.GetOperateType() == Global.eOperate.add_road_zuoshangyouxia )
                {
                        car.UpdateDir( Global.eAutomobileDir.youxia );
                    
                }

                car.SetStartPosByDir( car.GetDir(), road.x, road.y );
                

                // 汽车的铆点设置在图片正中心
                car.anchorOffsetX = car.m_bmpMainPic.width / 2;
                car.anchorOffsetY = car.m_bmpMainPic.height / 2;
  
                car.SetRaod( road );    
                car.SetMoveStatus( Global.eAutomobileStatus.normal ); // 注意，先SetRaod，再SetMoveStatus


                CSoundManager.PlaySE( Global.eSE.car );

             
                  car.SetSortPosY();
                   this.InsertObj( car );
        }

        protected m_fWindTimeElapse:number = 0;
        protected WindTurbineLoop():void
        {
                this.m_fWindTimeElapse += CDef.s_fFixedDeltaTime
                if ( this.m_fWindTimeElapse < 0.2 )
                {
                        return;
                }
                this.m_fWindTimeElapse = 0;

                for ( var i:number = this.m_containerTurbine.numChildren - 1; i >= 0; i-- )
                {
                        var turbine:CWindTurbine = this.m_containerTurbine.getChildAt(i) as CWindTurbine;
                        turbine.Loop();



                } // end for  
        }
        
        protected AutomobileLoop():void
        {
                 for ( var i:number = this.m_aryAutomobiles.length - 1; i >= 0; i-- )
                {
                        var car:CAutomobile = this.m_aryAutomobiles[i] as CAutomobile;
                        car.Move();
                        //car.U_Turn();
                        car.Turn_Left();
                        car.Turn_Right();
              

                 car.ReInsertSort();
                       car.GenerateCoinLoop();
                        car.JinBiLoop();
                } // end i
        }

        protected onTapBuildingLand( evt:egret.TouchEvent  ):void
        {
                // 判断下是否真正点中了。这个事件是以矩形包围盒来判断点选的
                var bHit:boolean =  evt.currentTarget.hitTestPoint( evt.stageX ,evt.stageY, true );
                if ( bHit )
                {
                     var objBuildingLand:CObj = evt.currentTarget as CObj;
                     switch( objBuildingLand.GetBuildingLandStatus() )
                     {
                        case Global.eBuildingLandStatus.empty: // 空地
                        {
                              this.ProcessTapEmptyLand( objBuildingLand );          
                        }       
                        break;

                        case Global.eBuildingLandStatus.contructing: // 建设中
                        {

                        }       
                        break; 

                        case Global.eBuildingLandStatus.building_exist: // 已建成
                        {

                        }       
                        break;
                     } // end switch
                }
                
        }



        public ProcessTapEmptyLand( obj:CObj ):void
        {
               // obj.BeginConstrucingStatus();   
        }

        protected m_fBuildingLandLoopTimeElapse:number = 0;

        public BuildingLandLoop():void
        {
                var bBuildingLandLoop:boolean = false;
                this.m_fBuildingLandLoopTimeElapse += CDef.s_fFixedDeltaTime;
                if ( this.m_fBuildingLandLoopTimeElapse >= CDef.s_fBuildingLandLoopInterval )
                {
                        bBuildingLandLoop = true;
                        this.m_fBuildingLandLoopTimeElapse = 0;
                }
              
                for ( var i:number = 0; i < this.m_aryAllDistricts.length; i++ )
                {
                        var district:CDistrict = this.m_aryAllDistricts[i];
                        district.BuildingAndUpgradingLoop(bBuildingLandLoop);
                }
                /*
                for ( var i:number = 0; i < this.m_containerObjs.numChildren; i++ )
                {
                    var obj:CObj = this.m_containerObjs.getChildAt(i) as CObj;
                    if ( obj.GetFuncType() != Global.eObjFunc.building ) // 稍后路面可以放在另一个显示列表中，反正不参与遮挡排序，其职能与建筑物也不同
                    {
                            continue;
                    }
                   
                    obj.ProgressBarLoop();

                    if ( bBuildingLandLoop )
                    {
                        obj.BuildingLandLoop();
                    }
                }
                */
        }

        public SetBlur( bBlur:boolean ):void
        {
                return;

                if ( bBlur )
                {
                        this.filters = [CColorFucker.GetBlurFilter()];
                }
                else
                {
                        this.filters = null;
                }
        }

        protected  m_aryEmptyGrid:Array<CGrid> = new Array<CGrid>();
        public GenerateGrass():void
        {
                var nTreeCount:number = 0;

                var fTotalDis:number = 0;
                var nTotalNum:number = 0;
                this.m_aryEmptyGrid.length = 0;
                var fChaZhiEndDis:number = -1;

                // 遍历每一个格子
                for ( var i:number = 0; i < this.m_containerGrids.numChildren; i++ )
                {       
                        var grid:CGrid = this.m_containerGrids.getChildAt(i) as CGrid;
                        if ( grid.IsTaken() )
                        {

                                continue;
                        }
                        else{
                                 var nX:number = grid.GetId()["nIndexX"];
                                  var nY:number = grid.GetId()["nIndexY"];
                                var fMyDis:number = ( nX * nX + nY * nY );
                                if ( grid.GetEmptyGridType() == Global.eEmptyGridType.city_edge )
                                {
                                        fTotalDis+= fMyDis;
                                        nTotalNum++;
                                }

                                if ( fMyDis > fChaZhiEndDis )
                                {
                                        fChaZhiEndDis = fMyDis;
                                }
                     
                        
                                this.m_aryEmptyGrid.push( grid );
                                
                        }
                } // end for

                var AverStartDis:number = fTotalDis / nTotalNum;
           
                var fChaZhiStartDis:number = AverStartDis;
                var startR:number = 82;
                var startG:number = 173;
                var startB:number = 70;
                var EndR:number = 255;
                var EndG:number = 215;
                var EndB:number = 0;
                for ( var i:number = 0; i < this.m_aryEmptyGrid.length; i++ )
                {
                        var grid:CGrid = this.m_aryEmptyGrid[i];
                        if  (grid.IsTaken() || grid.GetEmptyGridType() == Global.eEmptyGridType.inside_city )
                        {
                                continue;
                        }
                        var nX:number = grid.GetId()["nIndexX"];
                        var nY:number = grid.GetId()["nIndexY"];
                        var fMyPos:number = ( nX * nX + nY * nY ) - fChaZhiStartDis;
                        if ( fMyPos < 0 )
                        {
                                fMyPos = 0;
                        }
                        var t:number = fMyPos / ( fChaZhiEndDis - fChaZhiStartDis );
                        
        
                        var r:number = ( 1 - t ) * startR + t * EndR;
                        var g:number = ( 1 - t ) * startG + t * EndG;
                        var b:number = ( 1 - t ) * startB + t * EndB;
                         
                  
                        grid.SetColor( r ,g, b );

                        // 生成一棵树
                        if ( grid.GetEmptyGridType() == Global.eEmptyGridType.outside_city )
                        {
                                nTreeCount++;
                                if ( nTreeCount > 3 )
                                {
                                        nTreeCount = 0;
                                        var tree:CObj = CResourceManager.NewObj();
                                        tree.SetTexture( RES.getRes( "shu_png" ) );
                                        this.m_containerTrees.addChild( tree );
                                        tree.anchorOffsetX = tree.width / 2;
                                        tree.anchorOffsetY = tree.height;
                                        tree.x = grid.x;
                                        tree.y = grid.y;

                                        var fRandomScale:number = Math.random();
                                        if ( fRandomScale < 0.3 )
                                        {
                                          tree.scaleX += fRandomScale;
                                          tree.scaleY += fRandomScale;
                                        }
                                        else if ( fRandomScale > 0.7 )
                                        {
                                          tree.scaleX -= (1 - fRandomScale );
                                          tree.scaleY -= (1 - fRandomScale );
                                        }

                                        tree.scaleX *= 0.5;
                                        tree.scaleY *= 0.5;

                                        if ( nY < 0  )
                                        {
                                            
                                                 tree.y += Math.random() * CDef.s_nTileHeight *  0.5;
                                        }
                                        else
                                        {tree.y -= Math.random() * CDef.s_nTileHeight *  0.5;

                                        }

                                                 if ( nX< 0  )
                                        {
                                            
                                                 tree.x += Math.random() * CDef.s_nTileWidth *  0.5;
                                        }
                                        else
                                        {
                                                tree.x -= Math.random() * CDef.s_nTileWidth *  0.5;

                                        }

                                           
                                }
                        }
                }
        }

        public SetCurEditProcessingLot( obj:CObj ):void
        {
                this.m_objCurProcessingLot = obj;
        }

        public GetCurEditProcessingLot(  ):CObj
        {
                return this.m_objCurProcessingLot;
        }

        public ProcessBuyOneLot( nCost:number, eLotProperty:Global.eLotPsroperty ):void
        {
                if ( this.m_objCurProcessingLot == null )
                {
                        console.log( "有bug" );
                        return;
                }

                 this.m_objCurProcessingLot.SetLotProperty( eLotProperty );

                // 开始建造
                this.m_objCurProcessingLot.BeginConstrucingStatus();

                // 消耗金币
                this.SetCoins( this.GetCoins() -  nCost, true);

                this.OnLotBuildingNumChanged();
               
                

        }

        public GetCurBuildingLotNum( eSizeType:Global.eObjSize ):number
        {
                return this.m_dicBulildLotNum[eSizeType];
        }

        // 用“统计法”计数，而不是用“增量法”计数。这样可以最大限度的减少出Bug
        public OnLotBuildingNumChanged():void
        {
                this.UpdateCurLotStatus();

                // 建筑物数量变了，则更新人口数
                // 规定：建筑数量是决定人口数量的唯一参数。
                var nCurPopulation:number = CConfigManager.GetLot2Poulation(Global.eObjSize.size_2x2 ) * this.GetBuildingNum(Global.eObjSize.size_2x2)
                +CConfigManager.GetLot2Poulation(Global.eObjSize.size_4x4 ) * this.GetBuildingNum(Global.eObjSize.size_4x4);

             
                this.OnBuildingCPSChanged();
               
                
                this.SetPopulation( nCurPopulation );

              
                var objectRet:Object = this.CheckIfReachThePopulationToNextCity();
                if (  objectRet["Result"] )
                {
                        CSoundManager.PlaySE( Global.eSE.congratulations );
                }

        }

     

        public GetBuildingNum( eSizeType:Global.eObjSize ):number
        {
                if ( this.m_dicBulildLotNum[eSizeType] == undefined )
                {
                        return 0;
                }
                return this.m_dicBulildLotNum[eSizeType];
        }

        public GetTotalCPSableBuilding():number
        {
                return this.m_nCurCPSableBuildingNum;
        }

       public GetEmptyLotNum( eSizeType:Global.eObjSize ):void
       {
               return this.m_dicEmptyLotNum[eSizeType];
       }

        // 典型的“统计法”而不是“增量法”。前者不容易出Bug
        public UpdateCurLotStatus():void
        {
                this.m_dicEmptyLotNum[Global.eObjSize.size_2x2] = 0;
                this.m_dicEmptyLotNum[Global.eObjSize.size_4x4] = 0;
                this.m_dicBulildLotNum[Global.eObjSize.size_2x2] = 0;
                this.m_dicBulildLotNum[Global.eObjSize.size_4x4] = 0;

                this.m_dicPropertyPercent[Global.eLotPsroperty.residential] = 0;
                this.m_dicPropertyPercent[Global.eLotPsroperty.business] = 0;
                this.m_dicPropertyPercent[Global.eLotPsroperty.service] = 0;

                // 更新本城镇建筑物的总产钱能力
                this.m_nBuildingCPS = 0;

                this.m_nCurCPSableBuildingNum = 0;

                for ( var i:number = 0; i < this.m_containerObjs.numChildren; i++ )
                {
                        var obj:CObj = this.m_containerObjs.getChildAt(i) as CObj;
                        if ( obj.GetFuncType() != Global.eObjFunc.building ) // 不是建筑用地
                        {
                                continue;
                        }

                        // 该地块已经有建筑了，不能再建(除非执行了“推平”功能)
                        // 注：正在建设中的状态也算“已有建筑了”，同时这种状态就开始产钱了
                        if ( obj.HasBuilding() ) 
                        {
                                if ( !obj.IsBuildinSpecial() ) // 不是特殊职能建筑（如市政厅）
                                {
                                        this.m_dicBulildLotNum[obj.GetSizeType()]++;

                                        // 本建筑的cps
                                         var nCps:number = obj.GetLotCPS();/*(CConfigManager.GetBuildingCoinBySizeAndLevel( obj.GetSizeType(), obj.GetLotLevel() )*/;
                                         this.m_nBuildingCPS += nCps;
                                         this.m_nCurCPSableBuildingNum++;
                                        
                                         this.m_dicPropertyPercent[obj.GetLotProperty()]++;
                                }

                                continue;
                        }
                        
                        this.m_dicEmptyLotNum[obj.GetSizeType()]++;

                       
                        
                } // end i

                var nTotal:number = this.m_dicPropertyPercent[Global.eLotPsroperty.residential] + 
                                    this.m_dicPropertyPercent[Global.eLotPsroperty.business] + 
                                    this.m_dicPropertyPercent[Global.eLotPsroperty.service];
                this.m_dicPropertyPercent[Global.eLotPsroperty.residential] /=      nTotal;
                this.m_dicPropertyPercent[Global.eLotPsroperty.business] /=      nTotal;
                this.m_dicPropertyPercent[Global.eLotPsroperty.service] /=      nTotal;       


             //   console.log( "空地数：" +  this.m_dicEmptyLotNum[Global.eObjSize.size_2x2] + "_" +  this.m_dicEmptyLotNum[Global.eObjSize.size_4x4]);
             //   console.log( "有自建建筑数：" +  this.m_dicBulildLotNum[Global.eObjSize.size_2x2] + "_" +  this.m_dicBulildLotNum[Global.eObjSize.size_4x4]);
             //   console.log( "建筑CPS:" + this.m_nBuildingCPS );

                
                


        } 

       
        public OnPopulationChanged():void
        {
                // 计算人口贡献的金币产出率
                var EaringPerBuilding:number = CConfigManager.GetPopulation2CoinXiShu() * this.GetPopulation() ;
                this.m_nPopulationCPS = EaringPerBuilding * this.GetTotalCPSableBuilding();
              
                CUIManager.s_uiWindTurbine.SetParams(  EaringPerBuilding, this.m_nPopulationCPS ) ;

                this.UpdateCPS(); // 人口数变了，则总CPS更新
        }

        public  OnBuildingCPSChanged():void
        {
                this.UpdateCPS(); // 建筑CPS变了，则总CPS更新
        }

        public GetBuildingCPS():number
        {
                return this.m_nBuildingCPS;
        }

        public SetBuildingCPS( nBuildingCPS:number ):void
        {
                this.m_nBuildingCPS = nBuildingCPS;
        }

        public GetPopulationCPS():number
        {
               return this.m_nPopulationCPS;
        }

        public SetPopulationCPS( nPopulationCPS:number ):void
        {
               this.m_nPopulationCPS = nPopulationCPS;
        }

 public  SetCoins( nCoins:number, bDirect:boolean = false ):void
     {
        this.s_nCoins = nCoins;
        CUIManager.s_uiMainTitle.SetCoins( nCoins, bDirect );
     }

    public  GetCoins( ):number
     {
        return this.s_nCoins;
     }

     public  SetPopulation( nPopulation:number ):void
     {
          this.s_nPopulation = nPopulation;

          CUIManager.s_uiMainTitle.SetPopulation( nPopulation );

           this.OnPopulationChanged();

           
     }

     public GetPopulation():number
     {
             return this.s_nPopulation;
     }

     public GetCPS():number
     {
             return this.s_nCPS;
     }

     public SetCPS( nCPS:number ):void
     {
             this.s_nCPS = nCPS;
     }

     public GetCpsWithoutDoubleTime():number
     {
             return this.s_nCPS_WithOutDoubleTime; 
     }

     public  UpdateCPS():void
     {
         this.s_nCPS = 0; // 以下开始累加

         // 遍历当前的建筑，每个建筑有产金币能力
         this.s_nCPS += Main.s_CurTown.GetBuildingCPS();

         //this计算人口对金币产出的贡献
         this.s_nCPS += Main.s_CurTown.GetPopulationCPS();

         this.s_nCPS_WithOutDoubleTime = this.s_nCPS;

         // ------------------------------ 统计完成

         // Double Time 状态的加成
         if ( CPlayer.IsDoubleTime() )
         {
                this.s_nCPS *= 2;
         }


         // 更新界面显示
         CUIManager.s_uiMainTitle.SetCPS( this.s_nCPS );

         // 计算最新的金钱刷新速度
         this.m_nShowCoinChangeSpeed = this.s_nCPS;

         
     }
   
     public GetShowCoinChangeSpeed():number
     {
             return this.m_nShowCoinChangeSpeed;
     }

     // 一秒计算一次金币收入
     public  UpdateTotalCoins():void
     {
         this.s_nCoins += this.s_nCPS;

         this.SetCoins( this.s_nCoins );
     }

     public AddWindTurbine( grid:CGrid ):void
     {
             var turbine:CWindTurbine = new CWindTurbine();
             this.m_containerTurbine.addChild( turbine );
             turbine.anchorOffsetX = turbine.width / 2;
             turbine.anchorOffsetY = turbine.height;
             turbine.x = grid.x;
             turbine.y = grid.y;
             turbine.touchEnabled = true;
             turbine.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapTurbine, this );
     }

     protected onTapTurbine( evt:egret.TouchEvent ):void
     {
        if ( this.m_eCurOperate == Global.eOperate.del_obj )
        {
                CResourceManager.DeleteObj( evt.currentTarget as CObj );
        }
        else
        {
                CUIManager.SetUiVisible( Global.eUiId.wind_turbine, true );
        }
     }

     public GetLotProperty( eProperty:Global.eLotPsroperty ):number
     {
             var fPercent = this.m_dicPropertyPercent[eProperty];
             if ( fPercent == undefined || isNaN( fPercent ) )
             {
                     return 0;
             }
             return fPercent;
     }

     public SetGarageItemStatus( nIndex:number, eStatus:Global.eGarageItemStatus ):void
     {
        this.m_dicGarageInfo[nIndex] = eStatus;
     }

     public GetGarageItemStatus( nIndex:number):Global.eGarageItemStatus 
     {
        var eStatus:Global.eGarageItemStatus  = this.m_dicGarageInfo[nIndex];
        return eStatus;
     }

     public CostCoinDueToUnlockCar( nIndex:number ):void
     {   
        this.SetCoins( this.GetCoins() - CConfigManager.GetCoinForUnlockCar( nIndex ), true );
     }

     public UpdateGarageStatus():void
     {
        for ( var i:number = 0; i < CConfigManager.s_nTotalCarNum; i++ )
        {
                if ( this.GetGarageItemStatus(i) == Global.eGarageItemStatus.unlocked )
                {
                    continue;
                }
               

                var nCoinForThisCar:number = CConfigManager.GetCoinForUnlockCar(i);

              

                if ( this.GetCoins() < nCoinForThisCar )
                {
                    this.SetGarageItemStatus( i, Global.eGarageItemStatus.can_not_unlock );
                    CUIManager.s_uiGarage.UpdateStatus( i, Global.eGarageItemStatus.can_not_unlock  );
                }
                else
                {
                    this.SetGarageItemStatus( i, Global.eGarageItemStatus.locked_and_can_unlock );
                    CUIManager.s_uiGarage.UpdateStatus( i, Global.eGarageItemStatus.locked_and_can_unlock  );
                }

              

                return;   
        }
     }

     public EndUpgrade(lot:CObj):void
     {
                this.AddUpgradableLot( lot );
                this.RemoveFromUpgradingList( lot );
                this.CheckIfCanUpgrde();

     }

     public AddUpgradableLot( lot:CObj ):void
     {  
            // this.m_aryUpgradableBuildings.push( lot );
            this.m_aryCanUpgradeLots.push( lot );
     }

     public RemoveFromUpgradingList(lot:CObj):void
     {
                  for ( var i:number = 0; i <  this.m_aryUpgradingLots.length; i++ )
             {
                     if ( lot == this.m_aryUpgradingLots[i] )
                     {
                             this.m_aryUpgradingLots.splice( i, 1);
                             return;
                     }
             }


     }

     // revert是超低频操作。
     public RemoveUpgradableLot( lot:CObj ):void
     {
             for ( var i:number = 0; i <  this.m_aryCanUpgradeLots.length; i++ )
             {
                     if ( lot == this.m_aryCanUpgradeLots[i] )
                     {
                             this.m_aryCanUpgradeLots.splice( i, 1);
                             return;
                     }
             }

             for ( var i:number = 0; i <  this.m_aryUpgradingLots.length; i++ )
             {
                     if ( lot == this.m_aryUpgradingLots[i] )
                     {
                             this.m_aryUpgradingLots.splice( i, 1);
                             return;
                     }
             }

             console.log( "RemoveUpgradableLot 有Bug，没找到" );
     }

     public DoUpgrade():void
     {
         var lot:CObj = this.m_aryCanUpgradeLots[0];
         this.m_aryUpgradingLots.push( lot );
         this.m_aryCanUpgradeLots.splice( 0, 1 );

         lot.BeginUpgrade();

         this.CheckIfCanUpgrde();

     }

     protected GetCurUpgradingNum():number
     {
        return this.m_aryUpgradingLots.length;
     }

     public CheckIfCanUpgrde():void
     {
           var nAvailableNum:number = this.m_aryCanUpgradeLots.length;
           var nCurUpgradigNum:number = this.m_aryUpgradingLots.length;
           var nRealCanNum:number = this.m_nSimultaneousUpgradeNum - nCurUpgradigNum;
           if ( nAvailableNum > nRealCanNum )
           {
                   nAvailableNum = nRealCanNum;
           }


           CUIManager.s_uiCPosButton.SetCurCanUpgradeNum( nAvailableNum );


     }


     public AutoUpgradeLoop():void
     {
             return;
        
             
             if ( this.m_aryUpgradableBuildings.length == 0 )
             {
                     return;
             } 

        if ( this.m_objUpgradingLot != null )
        {
            
             this.m_objUpgradingLot.ProgressBarLoop();   
        }
        else
        {
            if ( this.m_nAutoUpgradeIndex >= this.m_aryUpgradableBuildings.length )
             {
                    this.m_nAutoUpgradeIndex = 0; 
             }
             this.SetupgradinLot( this.m_aryUpgradableBuildings[this.m_nAutoUpgradeIndex] );   
             this.m_objUpgradingLot.BeginUpgrade();
             this.m_nAutoUpgradeIndex++;
             if ( this.m_nAutoUpgradeIndex >= this.m_aryUpgradableBuildings.length )
             {
                    this.m_nAutoUpgradeIndex = 0; 
             }
        }
     }

     public SetupgradinLot( lot:CObj ):void
     {
        this.m_objUpgradingLot = lot;
     }

     public RevertLot():void
     {
        var objCurEdit:CObj = this.GetCurEditProcessingLot();
             if ( objCurEdit == null )
             {
                        console.log( "有Bug" );
                     return;
             }

       
           objCurEdit.Revert();  
  this.OnLotBuildingNumChanged();
         this.SetCurEditProcessingLot( null );
     }

     public GetupgradinLot( ):CObj
     {
        return this.m_objUpgradingLot;
     }

     public GetCityHallData( ePageIndex:Global.eCityHallPageType, nItemIndex:number ):CCityHallData
     {
             var dic:Object = null;
             if ( ePageIndex == Global.eCityHallPageType.city ) // city
             {
                dic = this.m_dicCityHallData_City;
             }
             else if (ePageIndex == Global.eCityHallPageType.game) // game
             {
                dic = this.m_dicCityHallData_Game;
             }
             var data:CCityHallData = dic[nItemIndex];
             if ( data == undefined )
             {
                data = new CCityHallData();
                dic[nItemIndex] = data;
             }
             
             return data;
     }

     public SetCityHallItemAffect( nIndex:number, nValue:number ):void
     {
             this.m_dicCityHallItemAffect[nIndex] = nValue;
     }

     public GetCityHallItemAffect( nIndex:number ):number
     {
            return this.m_dicCityHallItemAffect[nIndex];
     }

     protected m_nDeavtovateTime:number = 0;
     public ProcessBankSaving_Begin():void
     {
        var date = new Date();
        this.m_nDeavtovateTime = date.getTime();
     }

     protected m_nBankSaving:number = 0;
     public ProcessBankSaving_End():void
     {
         var date = new Date();
         var nAwayTime:number = ( date.getTime() - this.m_nDeavtovateTime ) / 1000; // 单位是毫秒，所以要除以一千
         if ( this.GetCPS() == 0 )
         {
                 return;
         }
         this.m_nBankSaving = Math.floor( nAwayTime * this.GetCPS() * this.m_BankData.nCurProfitRate );
         CUIManager.s_uiBankBack.visible = true;
         CUIManager.s_uiBankBack.SetBankSaving( this.m_nBankSaving );
         
     }

     public  CollectBankSaving():void
     {
          CPlayer.SetMoney( Global.eMoneyType.coin, CPlayer.GetMoney( Global.eMoneyType.coin) + this.m_nBankSaving  );
     }

     public GetLevel():number
     {
             return this.m_nID;
     }

     public StagePos2TownPos( fStageX:number, fStageY:number ):void
     {
             CTown.s_objectTemp["TownPosX"] =  ( fStageX - this.x ) / this.scaleX;
             CTown.s_objectTemp["TownPosY"] =  (fStageY - this.y ) / this.scaleY;
     }

     public TownPos2StagePos( x:number, y:number ):void
     {
             CTown.s_objectTemp["StagePosX"] =  x * this.scaleX + this.x;
             CTown.s_objectTemp["StagePosY"] =  y * this.scaleY + this.y;
     }
    
     public ProcessClick( fStageX:number, fStageY:number ):void
     {
             if ( CUIManager.IsUiShowing() )
             {
                     return;
             }

             // 奇数行只有奇数列，偶数行只有偶数列。
             this.StagePos2TownPos( fStageX, fStageY );
             var fPosXInTown:number = CTown.s_objectTemp["TownPosX"];
             var fPosYInTown:number = CTown.s_objectTemp["TownPosY"];
   

             var nIndex_X:number = fPosXInTown / CDef.s_nTileWidth;
             if ( fPosXInTown > 0 )
             {
                     nIndex_X = Math.floor( nIndex_X ) * 2 + 1;
             }
             else
             {
                     nIndex_X = Math.ceil( nIndex_X ) * 2 - 1;
             }

             var nIndex_Y:number = fPosYInTown / CDef.s_nTileHeight;
             if ( fPosYInTown > 0 )
             {
                     nIndex_Y = Math.floor( nIndex_Y ) * 2 + 1;
             }
             else
             {
                     nIndex_Y = Math.ceil( nIndex_Y ) * 2 - 1;
             }

            var grid:CGrid = this.GetGridByIndex( nIndex_X.toString(), nIndex_Y.toString() );
            if ( grid == null )
            {
                    return;
            }

            var nRelatedGridIndexX:number = 0;
            var nRelatedGridIndexY:number = 0;
            var grid_related:CGrid = null;
            var x1:number = 0;
            var y1:number = 0;
            var x2:number = 0;
            var y2:number = 0;
            var nRet:number = 0;
             var grid_selected:CGrid = null;
            if ( fPosXInTown >= grid.x ) // 右
            {
                   if ( fPosYInTown >= grid.y ) // 下
                   {
                      ///  console.log( nIndex_X + "," + nIndex_Y + ":" + "右下" );
                      nRelatedGridIndexX = nIndex_X + 1;
                      nRelatedGridIndexY = nIndex_Y + 1;
                      grid_related = this.GetGridByIndex( nRelatedGridIndexX.toString(), nRelatedGridIndexY.toString() );
                      x1 = grid.GetCornerPosX( Global.eGridCornerPosType.right );
                      y1 = grid.GetCornerPosY( Global.eGridCornerPosType.right );
                      x2 = grid.GetCornerPosX( Global.eGridCornerPosType.bottom );
                      y2 = grid.GetCornerPosY( Global.eGridCornerPosType.bottom );
                      nRet= CyberTreeMath.LeftOfLine( fPosXInTown, fPosYInTown, x1, y1, x2, y2 );
                      if ( nRet == 1 ) // 点落在直线左边
                      {
                              grid_selected = grid;
                      }
                      else
                      {
                                grid_selected = grid_related;           
                      }
                   }  
                   else // 上
                   {
                     //   console.log( nIndex_X + "," + nIndex_Y + ":" + "右上" );
                      nRelatedGridIndexX = nIndex_X + 1;
                      nRelatedGridIndexY = nIndex_Y - 1;
                      grid_related = this.GetGridByIndex( nRelatedGridIndexX.toString(), nRelatedGridIndexY.toString() );
                      x1 = grid.GetCornerPosX( Global.eGridCornerPosType.right );
                      y1 = grid.GetCornerPosY( Global.eGridCornerPosType.right );
                      x2 = grid.GetCornerPosX( Global.eGridCornerPosType.top );
                      y2 = grid.GetCornerPosY( Global.eGridCornerPosType.top );
                                            nRet= CyberTreeMath.LeftOfLine( fPosXInTown, fPosYInTown, x1, y1, x2, y2 );
                      if ( nRet == 1 ) // 点落在直线左边
                      {
                              grid_selected = grid;
                      }
                      else
                      {
                                grid_selected = grid_related;           
                      }
                   }
            }
            else // 左
            {
                   if ( fPosYInTown >= grid.y ) // 下
                   {
                    //    console.log( nIndex_X + "," + nIndex_Y + ":" + "左下" );
                      nRelatedGridIndexX = nIndex_X - 1;
                      nRelatedGridIndexY = nIndex_Y + 1;
                      grid_related = this.GetGridByIndex( nRelatedGridIndexX.toString(), nRelatedGridIndexY.toString() );
                      x1 = grid.GetCornerPosX( Global.eGridCornerPosType.left );
                      y1 = grid.GetCornerPosY( Global.eGridCornerPosType.left );
                      x2 = grid.GetCornerPosX( Global.eGridCornerPosType.bottom );
                      y2 = grid.GetCornerPosY( Global.eGridCornerPosType.bottom );
                                            nRet= CyberTreeMath.LeftOfLine( fPosXInTown, fPosYInTown, x1, y1, x2, y2 );
                      if ( nRet == 1 ) // 点落在直线左边
                      {
                               grid_selected = grid_related;       
                           
                      }
                      else
                      {
                                      grid_selected = grid;
                      }
                   }  
                   else // 上
                   {
                   //     console.log( nIndex_X + "," + nIndex_Y + ":" + "左上" );
                      nRelatedGridIndexX = nIndex_X - 1;
                      nRelatedGridIndexY = nIndex_Y - 1;
                      grid_related = this.GetGridByIndex( nRelatedGridIndexX.toString(), nRelatedGridIndexY.toString() );
                      x1 = grid.GetCornerPosX( Global.eGridCornerPosType.left );
                      y1 = grid.GetCornerPosY( Global.eGridCornerPosType.left );
                      x2 = grid.GetCornerPosX( Global.eGridCornerPosType.top );
                      y2 = grid.GetCornerPosY( Global.eGridCornerPosType.top );
                                                nRet= CyberTreeMath.LeftOfLine( fPosXInTown, fPosYInTown, x1, y1, x2, y2 );
                      if ( nRet == 1 ) // 点落在直线左边
                      {
                               grid_selected = grid_related;       
                           
                      }
                      else
                      {
                                      grid_selected = grid;
                      }
                   }
            }

           


            if ( grid_selected == null )
            {
                    return;
            } 

                   this.Processtap( grid_selected, fPosXInTown, fPosYInTown ); 

     }


   
     protected handleTapDibiao( evt:egret.TouchEvent ):void
     {
  
             if ( this.m_eCurOperate == Global.eOperate.del_dibiao )
             {

                  var dibiao:CObj = evt.target as CObj;
                  CResourceManager.DeleteDiBiao( dibiao );   
             }
     }
     
     protected m_bShowGrid:boolean = true;
     public ToggleGrids():void
     {
             this.m_bShowGrid = !this.m_bShowGrid;
        for ( var i:number = 0; i < this.m_aryAllGrids.length; i++ )
        {
                var grid:CGrid = this.m_aryAllGrids[i];
                grid.SetGridVisible( this.m_bShowGrid );
        }


        
     }



     public SetCloudManager( cloud_manager:CCloudManager ):void
     {
             this.m_CloudManager = cloud_manager;
     }

     public ClearAll():void
     {
   
      
              for ( var i:number = 0; i < this.m_aryAllDistricts.length; i++ )
             {
                     var district:CDistrict = this.m_aryAllDistricts[i];
                     district.ClearAll();
                     CResourceManager.DeleteDistrict( district );
             }
                this.m_aryAllDistricts.length = 0;


              for ( var i:number = 0; i < 32; i ++ )
              {
                      this.SetCityHallItemAffect(i,0);
              }

                this.m_dicEmptyLotNum[Global.eObjSize.size_2x2] = 0;
                this.m_dicEmptyLotNum[Global.eObjSize.size_4x4] = 0;
                this.m_dicBulildLotNum[Global.eObjSize.size_2x2] = 0;
                this.m_dicBulildLotNum[Global.eObjSize.size_4x4] = 0;

                this.m_dicPropertyPercent[Global.eLotPsroperty.residential] = 0;
                this.m_dicPropertyPercent[Global.eLotPsroperty.business] = 0;
                this.m_dicPropertyPercent[Global.eLotPsroperty.service] = 0;

             this.m_objCurProcessingLot = null;

             // 清空市政厅中“城市页”数据；注意，“游戏页”数据不能清空，是跟玩家账号绑定的，而不是跟关卡绑定的
             // to do

             

             // 清空特殊职能建筑信息
             this.m_dicFunctionalBuildings[Global.eOperate.add_cityhall] = null;
             this.m_dicFunctionalBuildings[Global.eOperate.add_garage] = null;
                this.m_dicFunctionalBuildings[Global.eOperate.add_bank] = null;

             //  清空当前人口以及人口的金币产出率
             this.SetPopulation(0);
             this.SetPopulationCPS(0);

             // 清空当前的总CPS
             this.SetCPS(0);
             CUIManager.s_uiMainTitle.SetCPS(0);

             // 清空当前的建筑CPS
             this.SetBuildingCPS(0);

               // 清除汽车信息
              this.m_aryAutomobiles.length = 0;

               // 清除建筑工地动画

              // 清除飞机

              // 清除自动升级状态
              this.m_objUpgradingLot = null;
                this.m_aryUpgradableBuildings.length = 0; // “当前可升级建筑”列表清空
                this.m_aryCanUpgradeLots.length = 0;
                this.m_aryUpgradingLots.length = 0;

              // 清除云彩和天空盒
              this.m_CloudManager.Clear();
/*
             // 清除Obj
              for ( var i:number = this.m_containerObjs.numChildren - 1; i >= 0; i-- )
              {
                      var obj:CObj = this.m_containerObjs.getChildAt(i) as CObj;
                      if ( obj.GetFuncType() == Global.eObjFunc.car )
                      {
                        var jinbi:CJinBi = obj.getChildByName( "jinbi" ) as CJinBi;
                        if ( jinbi != null && jinbi != undefined )
                        {
                                CJinBiManager.DeleteJinBi( jinbi );
                        }
                      }

                      CResourceManager.DeleteObj( obj );
              }

              // 清除路面
              for ( var i:number = this.m_containerRoads.numChildren - 1; i >= 0; i-- )
              {
                      var obj:CObj = this.m_containerRoads.getChildAt(i) as CObj;
                      CResourceManager.DeleteObj( obj );
              } 

              // 清除树木
              for ( var i:number = this.m_containerTrees.numChildren - 1; i >= 0; i-- )
              {
                      var obj:CObj = this.m_containerTrees.getChildAt(i) as CObj;
                      CResourceManager.DeleteObj( obj );
              } 

              // 清除地表
              for ( var i:number = this.m_containerDiBiao.numChildren - 1; i >= 0; i-- )
              {
                      var obj:CObj = this.m_containerDiBiao.getChildAt(i) as CObj;
                      CResourceManager.DeleteObj( obj );
              } 

              // 清除风车 
              for ( var i:number = this.m_containerTurbine.numChildren - 1; i >= 0; i-- )
              {
                      var obj:CObj = this.m_containerTurbine.getChildAt(i) as CObj;
                      CResourceManager.DeleteObj( obj );
              } 

              // 清除进度条
              if ( this.m_containerProgressbar )
              {
                for ( var i:number = this.m_containerProgressbar.numChildren - 1; i >= 0; i-- )
                {
                        var bar:CUIBaseProgressBar = this.m_containerProgressbar.getChildAt(i) as CUIBaseProgressBar ;
                        CResourceManager.DeleteProgressBar( bar );
                } 
              }
            

             

              // 清空所有格子上的信息
              for ( var i:number = this.m_containerGrids.numChildren - 1; i >= 0; i-- )
              {
                      var grid:CGrid = this.m_containerGrids.getChildAt(i) as CGrid ;
                      
                      grid.SetBoundObj( null );
   
              }
*/
     }

     public ChangeTown( nNewTownId:number ):void
     {
             if ( this.GetId() ==  nNewTownId )
             {
                     return;
             }

             this.ClearAll();
             this.LoadTownData( nNewTownId );

     }

     public GetDistrictById( nId:number ):CDistrict
     {
             for ( var i:number = 0; i < this.m_aryAllDistricts.length; i++ )
             {
                     var district:CDistrict = this.m_aryAllDistricts[i];
                     if ( nId == district.GetId() )
                     {
                          return district;   
                     }
             } 

             return null;
     }

     public ChangeDistrict( nDistrictId:number):void
     {
        var bLoadData:boolean = false;

        if (  this.m_CurDistrict && this.m_CurDistrict.GetId() == nDistrictId )
        {
                return;
        }

        var src_district:CDistrict = null;
        if ( this.m_CurDistrict )
        {
                src_district = this.m_CurDistrict;
          this.m_containerDistrict.removeChild( this.m_CurDistrict );  
        }

        this.m_CurDistrict = this.GetDistrictById( nDistrictId );
        if ( this.m_CurDistrict == null )
        {
                this.m_CurDistrict = CResourceManager.NewDistrict();
                this.m_aryAllDistricts.push( this.m_CurDistrict );
                this.m_CurDistrict.SetId( nDistrictId );      
                bLoadData = true;
        }
       this.m_containerDistrict.addChild( this.m_CurDistrict );



        this.m_containerGrids = this.m_CurDistrict.m_containerGrids;
        this.m_containerObjs = this.m_CurDistrict.m_containerObjs;
        this.m_dicGrids = this.m_CurDistrict.m_dicGrids;
        this.m_aryAllGrids = this.m_CurDistrict.m_aryAllGrids;
        this.m_containerDiBiao = this.m_CurDistrict.m_containerDiBiao;
        this.m_containerRoads = this.m_CurDistrict.m_containerRoads;
        this.m_containerTrees = this.m_CurDistrict.m_containerTrees;
        this.m_containerTurbine  = this.m_CurDistrict.m_containerTurbine;
        this.m_containerArrows = this.m_CurDistrict.m_containerArrows;
        this.m_containerProgressbar = this.m_CurDistrict.m_containerProgressbar;

        this.m_CurDistrict.addChild( CJinBiManager.s_containerJinBi );
        this.m_CurDistrict.addChild( CTiaoZiManager.s_lstcontainerTiaoZi );


        if ( bLoadData )
        {
            this.m_CurDistrict.LoadTownData( this.m_szTownData );    
        }

        // 先加载地图，再迁移车辆，不然地图中没有道路来承载车辆
        if ( src_district )
        {
                this.MoveCarsFromOneDistrictToAnother( src_district, this.m_CurDistrict );
        }


     }

     public MoveToNextCity():void
     {
          this.ChangeTown( this.GetId() + 1 );   
     }

     
     public CheckIfReachThePopulationToNextCity():Object
     {
        var config:CCofigCityLevel = CConfigManager.GetCityLevelConfig( Main.s_CurTown.GetLevel() );
        CTown.s_objectTemp["NeedPopulation"] = config.nPopulationToLevelUp;
        CTown.s_objectTemp["Result"] =  Main.s_CurTown.GetPopulation() >= config.nPopulationToLevelUp;
        return CTown.s_objectTemp;
     }

} // end class CTown