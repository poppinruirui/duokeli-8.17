// “正在建造”动画效果
class CConstructingAnimation extends egret.DisplayObjectContainer{

    protected m_bmpGround:egret.Bitmap = new egret.Bitmap( RES.getRes( "dibiao-baise_png" ) ); // 底座
    protected m_containerSegs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer(); // 按高度动态分配的段数
    protected m_containerCrane:egret.DisplayObjectContainer = new egret.DisplayObjectContainer(); // 塔吊（帧动画）

    protected m_fCraneFrameAniTimeElapse:number = 0;
    protected m_nCraneAniFrameIndex:number = 0;

    protected m_aryCraneFrams:Array<egret.Bitmap> = new Array<egret.Bitmap>();
    protected m_bmpCrane:egret.Bitmap = new egret.Bitmap();

    protected m_fParentObjWidth:number = 0;
    protected m_fParentObjHeight:number = 0;
    protected m_fParentObjSizeType:number = 0;

    protected m_nCurSegNum:number = 0;

    public constructor() {
            super();


           //this.addChild( this.m_bmpGround );
           //this.addChild( this.m_containerSegs );
           //this.addChild( this.m_bmpCrane );
           

            this.m_bmpGround.anchorOffsetX = this.m_bmpGround.width / 2;
            this.m_bmpGround.anchorOffsetY = this.m_bmpGround.height;
            


            // 滤镜 test

  var colorMatrix = [
    0.411,0,0,0,0,
    0,0.411,0,0,0,
    0,0,0.411,0,0,
    1,1,1,0,0
];

            var colorFlilter = new egret.ColorMatrixFilter(colorMatrix);
            this.m_bmpGround.filters = [colorFlilter];
            // end 滤镜 test


            var bmpCrane:egret.Bitmap = new egret.Bitmap( RES.getRes( "qizhongji1_png" ) );
            bmpCrane.anchorOffsetX = bmpCrane.width / 2;
            bmpCrane.anchorOffsetY = bmpCrane.height;
            this.m_aryCraneFrams.push( bmpCrane );
            bmpCrane = new egret.Bitmap( RES.getRes( "qizhongji2_png" ) );
                        bmpCrane.anchorOffsetX = bmpCrane.width / 2;
            bmpCrane.anchorOffsetY = bmpCrane.height;
            this.m_aryCraneFrams.push( bmpCrane );
            bmpCrane = new egret.Bitmap( RES.getRes( "qizhongji3_png" ) );
                        bmpCrane.anchorOffsetX = bmpCrane.width / 2;
            bmpCrane.anchorOffsetY = bmpCrane.height;
            this.m_aryCraneFrams.push( bmpCrane );

            this.m_bmpCrane.scaleX = 0.5;
            this.m_bmpCrane.scaleY = 0.5;
          
    }

    public SetSegNum( nSegNum:number ):void
    {
        var HurdleTotalHeight:number = 0;

        this.m_nCurSegNum = nSegNum;
        var nNeedNum:number = nSegNum - this.m_containerSegs.numChildren;
        if ( this.m_containerSegs.numChildren < nSegNum )
        {
            for ( var i:number = 0; i < nNeedNum; i++ )
            {
                  
                var seg:egret.Bitmap = new egret.Bitmap();
                this.m_containerSegs.addChild( seg );
                seg.anchorOffsetX = seg.width * 0.5;
                seg.anchorOffsetY = seg.height;

            }
        }
        var nCount:number = 0;
        for ( var i:number = this.m_containerSegs.numChildren - 1; i >= 0; i-- )
        {
            var seg:egret.Bitmap = this.m_containerSegs.getChildAt( i ) as egret.Bitmap;
            if ( nCount >= nSegNum )
            {
                seg.visible = false;
                continue;
            }
            seg.visible  = true;
           
            if ( nCount == 0 )
            {
                seg.texture = CResourceManager.s_bmpSegTop.texture;
            }
            else
            {
                seg.texture = CResourceManager.s_bmpSeg.texture;
            }
            var fSegHeight:number = -(nSegNum - nCount - 1) * seg.height * 0.17;
            seg.y = fSegHeight;
            HurdleTotalHeight += fSegHeight;
            nCount++;
        }


        if ( this.m_fParentObjSizeType == Global.eObjSize.size_2x2 )
        {
            if ( nSegNum == 1 )
            {
                this.m_bmpCrane.y = -seg.height * 0.25;
            }
            else
            {
                    this.m_bmpCrane.y = -seg.height * 0.2 - ( nSegNum - 1 ) * seg.height * 0.225;
            }
           
            this.m_containerSegs.x = -this.m_fParentObjWidth * 0.215;
              this.m_containerSegs.y = -this.m_fParentObjHeight * 0.7;
        }
        else if ( this.m_fParentObjSizeType == Global.eObjSize.size_4x4 ) 
        {
              if ( nSegNum == 1 )
            {
                this.m_bmpCrane.y = -seg.height * 0.2;
            }
            else
            {
                    this.m_bmpCrane.y = -seg.height * 0.2 - ( nSegNum - 1 ) * seg.height * 0.225;
            }
           
            this.m_containerSegs.x = -this.m_fParentObjWidth * 0.1075;
             this.m_containerSegs.y = -this.m_fParentObjHeight * 0.35;
        }
    }

    public SetParentObjPixelSize( eSizeType:Global.eObjSize, fWidth:number, fHeight:number ):void
    {
        this.m_fParentObjSizeType = eSizeType;
        this.m_fParentObjWidth = fWidth;
        this.m_fParentObjHeight = fHeight;
      

        
    }

    public SetSize( eSize:Global.eObjSize ):void
    {
        switch(eSize)
        {
            case Global.eObjSize.size_1x1:
            {
                this.scaleX = 0.5;
                this.scaleY = 0.5;
            }
            break;

            case Global.eObjSize.size_2x2:
            {
                this.scaleX = 1;
                this.scaleY = 1;
            }
            break;

            case Global.eObjSize.size_4x4:
            {
                this.scaleX = 2;
                this.scaleY = 2;
            }
            break;
        } // end switch
    }

    public FixedUpdate():void
    {
        this.CraneFrameAniLoop();
    }

    public CraneFrameAniLoop():void
    {
        this.m_fCraneFrameAniTimeElapse += CDef.s_fBuildingLandLoopInterval;
        if ( this.m_fCraneFrameAniTimeElapse < CDef.s_fCraneFrameInterval )
        {
            return;
        }
        this.m_fCraneFrameAniTimeElapse = 0;
        
        this.m_nCraneAniFrameIndex = Math.floor( Math.random() * 3 );
       
        if ( this.m_nCraneAniFrameIndex >= this.m_aryCraneFrams.length )
        {
            this.m_nCraneAniFrameIndex = 0;
        }


        var bmpFrame:egret.Bitmap = this.m_aryCraneFrams[this.m_nCraneAniFrameIndex];

        
        this.m_bmpCrane.texture = bmpFrame.texture;

        this.m_bmpCrane.anchorOffsetX = this.m_bmpCrane.width *0.5;
        this.m_bmpCrane.anchorOffsetY = this.m_bmpCrane.height;
    }

    public MainLoop():void
    {
        this.CraneFrameAniLoop();
    }











} // end class 