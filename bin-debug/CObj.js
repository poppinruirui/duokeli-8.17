var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CObj = (function (_super) {
    __extends(CObj, _super);
    //// end Move to Dest
    function CObj() {
        var _this = _super.call(this) || this;
        _this.m_BuildingData = null;
        _this.m_eBuildinSpecial = Global.eBuildinSpeical.none;
        _this.m_objBuilding = null;
        _this.m_bmpMainPic = new egret.Bitmap(); // 一个Obj的核心图片
        _this.m_nGuid = 0;
        _this.m_Town = null;
        _this.m_eSizeType = Global.eObjSize.size_1x1;
        _this.m_eFuncType = Global.eObjFunc.none;
        _this.m_eOperateType = Global.eOperate.edit_obj;
        _this.m_eBuildingLandStatus = Global.eBuildingLandStatus.empty;
        _this.m_ConstructingAni = null;
        _this.m_fSortPosY = 0;
        _this.m_txtDebugInfo = new egret.TextField();
        _this.m_bHasBuilding = false;
        _this.m_nLotLevel = 0; // 建筑物其实是没有等级的概念的，等级和产出其实是那个地块的
        _this.m_nCurBuildingResIndex = 0;
        _this.m_bHistorical = false;
        _this.m_eLotProperty = Global.eLotPsroperty.residential;
        _this.m_progressBar = null;
        _this.m_eProcessType = Global.eProgressBarType.none;
        _this.m_nProcessingTimeElapse = 0;
        _this.m_nProcessingTotalTime = 0;
        _this.m_CurBuildingConfig = null;
        _this.m_fSpeed = 0;
        _this.m_fTime = 0;
        _this.m_objectParams = new Object();
        //// Move to Dest
        _this.m_vecDestPos = new CVector();
        _this.m_vecSpeed = new CVector();
        _this.m_objectPos = new Object();
        _this.m_gridLocation = null;
        _this.m_aryBoundGridIndex = new Array();
        // 获取这个地块的CPS
        // CTown.ts中的GetBuildingCPS()函数，是获取当前所有地块CPS的总和
        _this.m_nRealCPSOfThisLot = 0;
        _this.m_BelongToDistrict = null;
        _this.addChild(_this.m_bmpMainPic);
        return _this;
    }
    CObj.prototype.SetSizeType = function (eSizeType) {
        this.m_eSizeType = eSizeType;
    };
    CObj.prototype.GetSizeType = function () {
        return this.m_eSizeType;
    };
    CObj.GetPixelSizeBySizeType = function (eType) {
        switch (eType) {
            case Global.eObjSize.size_1x1:
                {
                    CObj.objectTemp["x"] = CDef.s_nTileWidth;
                    CObj.objectTemp["y"] = CDef.s_nTileHeight;
                }
                break;
            case Global.eObjSize.size_2x2:
                {
                    CObj.objectTemp["x"] = CDef.s_nTileWidth;
                    CObj.objectTemp["y"] = CDef.s_nTileHeight;
                }
                break;
            case Global.eObjSize.size_4x4:
                {
                    CObj.objectTemp["x"] = CDef.s_nTileWidth * 2;
                    CObj.objectTemp["y"] = CDef.s_nTileHeight * 2;
                }
                break;
        }
        return CObj.objectTemp;
    };
    CObj.prototype.SetFuncType = function (eFuncType) {
        this.m_eFuncType = eFuncType;
        if (this.GetFuncType() == Global.eObjFunc.building) {
            /*
            this.scaleX = 1.5;
            this.scaleY = 1.5;
        
            var fLocalOffsetY:number = 0;
          switch( this.GetSizeType() )
          {
            case Global.eObjSize.size_2x2:
            {

                fLocalOffsetY = 2 * CDef.s_nTileHeight * 0.25;
            }
            break;
            case Global.eObjSize.size_4x4:
            {

                fLocalOffsetY = 4 * CDef.s_nTileHeight* 0.25 ;
            }
            break;
          }

          
           this.y += fLocalOffsetY;
*/
        }
        else {
        }
    };
    CObj.prototype.GetFuncType = function () {
        return this.m_eFuncType;
    };
    CObj.prototype.Reset = function () {
        this.m_BelongToDistrict = null;
        this.alpha = 1;
        this.rotation = 0;
        this.m_bmpMainPic.filters = null;
        this.m_eProcessType = Global.eProgressBarType.none;
        this.m_Town = null;
        // 跟本Obj绑定的格子
        this.m_aryBoundGridIndex.length = 0;
        this.m_gridLocation = null;
        // 跟本Obj绑定的建筑
        this.m_objBuilding = null;
        this.m_bHasBuilding = false;
        this.m_eBuildinSpecial = Global.eBuildinSpeical.none;
        this.scaleX = 1;
        this.scaleY = 1;
        this.m_nLotLevel = 0;
        this.m_nCurBuildingResIndex = 0;
        this.m_bmpMainPic.texture = null;
    };
    CObj.prototype.SetPos = function (x, y) {
        this.x = x;
        this.y = y;
    };
    CObj.prototype.GetPos = function () {
        this.m_objectPos["x"] = this.x;
        this.m_objectPos["y"] = this.y;
        return this.m_objectPos;
    };
    CObj.prototype.SetGuid = function (nGuid) {
        this.m_nGuid = nGuid;
    };
    CObj.prototype.GetGuid = function () {
        return this.m_nGuid;
    };
    CObj.prototype.SetBitmap = function (bitmap) {
        this.m_bmpMainPic = bitmap;
    };
    CObj.prototype.SetTexture = function (tex) {
        this.m_bmpMainPic.texture = tex;
    };
    CObj.prototype.GetTexture = function () {
        return this.m_bmpMainPic.texture;
    };
    CObj.prototype.SetTown = function (town) {
        this.m_Town = town;
    };
    CObj.prototype.ClearBoundGrid = function () {
        for (var i = 0; i < this.m_aryBoundGridIndex.length - 1; i += 2) {
            // console.log( "clear bound grid:" + this.m_aryBoundGridIndex[i] + "_" + this.m_aryBoundGridIndex[i+1] );
            var grid = Main.s_CurTown.GetGridByIndex(this.m_aryBoundGridIndex[i].toString(), this.m_aryBoundGridIndex[i + 1].toString());
            if (grid == null || grid == undefined) {
                console.log("有Bug:" + this.m_aryBoundGridIndex[i] + "," + this.m_aryBoundGridIndex[i + 1]);
                continue;
            }
            if (grid.GetZhiNengJianZhu()) {
                console.log("不得哦aaaaaaaaaa");
            }
            grid.SetBoundObj(null);
        }
    };
    CObj.prototype.SetLocationGrid = function (grid) {
        this.m_gridLocation = grid;
    };
    CObj.prototype.GetLocationGrid = function () {
        return this.m_gridLocation;
    };
    CObj.prototype.SetBoundGrid = function (grid) {
        this.m_aryBoundGridIndex.push(grid.GetId()["nIndexX"]);
        this.m_aryBoundGridIndex.push(grid.GetId()["nIndexY"]);
        //  console.log( "set bound grid:" + grid.GetId()["nIndexX"] + "," + grid.GetId()["nIndexY"] );
    };
    CObj.prototype.SetOperateType = function (eOperate) {
        this.m_eOperateType = eOperate;
    };
    CObj.prototype.GetOperateType = function () {
        return this.m_eOperateType;
    };
    CObj.prototype.SetBuildingLandStatus = function (eStatus) {
        this.m_eBuildingLandStatus = eStatus;
    };
    CObj.prototype.GetBuildingLandStatus = function () {
        return this.m_eBuildingLandStatus;
    };
    CObj.prototype.ProcessAddSpecialBuilding = function (op) {
        // this.m_objBuilding = new CObj();
        var szResName = "";
        switch (op) {
            case Global.eOperate.add_cityhall:
                {
                    szResName = "shizhengting_png";
                    this.SetBuildinSpecial(Global.eBuildinSpeical.cityhall);
                }
                break;
            case Global.eOperate.add_bank:
                {
                    szResName = "yinhang_png";
                    this.SetBuildinSpecial(Global.eBuildinSpeical.bank);
                }
                break;
            case Global.eOperate.add_garage:
                {
                    szResName = "chezhan_png";
                    this.SetBuildinSpecial(Global.eBuildinSpeical.garage);
                }
                break;
        }
        this.SetTexture(RES.getRes(szResName));
        this.anchorOffsetX = this.width / 2;
        this.anchorOffsetY = this.height;
        var fLocalOffsetX = 0;
        var fLocalOffsetY = 0;
        this.scaleX = 1;
        this.scaleY = 1;
        this.SetHasBuilding(true);
        return true;
    };
    CObj.prototype.onTapgarage = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.garage, true);
    };
    CObj.prototype.GetLotCPS = function () {
        // poppin to do 
        // 可以预见的，接下里会影响地块产出的因素还有：特殊建筑、市政厅中的项目加成
        var nLotCps = CConfigManager.GetBuildingCoinBySizeAndLevel(this.GetSizeType(), this.GetLotLevel());
        var nCPS_temp = nLotCps;
        var nCityHallItemAffect = Main.s_CurTown.GetCityHallItemAffect(0);
        if (nCityHallItemAffect != undefined) {
            nLotCps *= (1 + nCityHallItemAffect);
        }
        if (this.m_BuildingData != null) {
            if (this.m_BuildingData.bSpecial) {
                nLotCps *= (1 + this.m_BuildingData.fGainRate);
            }
        }
        this.m_nRealCPSOfThisLot = Math.ceil(nLotCps);
        return this.m_nRealCPSOfThisLot;
    };
    CObj.prototype.ProcessUpgrade = function () {
        CUIManager.s_uiLotUpgrade.UpdateInstanceUpgradeButtonStatus();
        CUIManager.s_uiLotUpgrade.SetCurLotLevel(this.GetLotLevel());
        CUIManager.s_uiLotUpgrade.SetCurLotCPS(this.m_nRealCPSOfThisLot);
        CUIManager.s_uiLotUpgrade.SetBuildingAvatar(this.GetTexture());
        CUIManager.s_uiLotUpgrade.SetBuildingName(this.m_BuildingData /*m_CurBuildingConfig*/.szName);
        CUIManager.s_uiLotUpgrade.UpdatePanelStatusWhenOpen();
    };
    CObj.prototype.ProcessEdit = function () {
        if (Main.s_CurTown.GetOperate() != Global.eOperate.edit_obj) {
            return;
        }
        if (this.IsBuildinSpecial()) {
            if (this.GetBuildinSpecial() == Global.eBuildinSpeical.garage) {
                CUIManager.SetUiVisible(Global.eUiId.garage, true);
                Main.s_CurTown.UpdateGarageStatus();
            }
            else if (this.GetBuildinSpecial() == Global.eBuildinSpeical.cityhall) {
                CUIManager.SetUiVisible(Global.eUiId.city_hall, true);
                CUIManager.s_uiCityHall.UpdateStatus();
            }
            else if (this.GetBuildinSpecial() == Global.eBuildinSpeical.bank) {
                CUIManager.SetUiVisible(Global.eUiId.bank, true);
                CUIManager.s_uiBank.UpdateStatus();
            }
            return;
        }
        // 目前只有房屋类的Obj可以操作。
        if (this.GetFuncType() != Global.eObjFunc.building) {
            return;
        }
        if (this.m_eProcessType == Global.eProgressBarType.constructing) {
            return;
        }
        Main.s_CurTown.SetCurEditProcessingLot(this);
        if (this.GetBuildingLandStatus() == Global.eBuildingLandStatus.building_exist) {
            CUIManager.SetUiVisible(Global.eUiId.lot_upgrade, true);
            this.ProcessUpgrade();
            Main.s_CurTown.SetCurEditProcessingLot(this);
            return;
        }
        CUIManager.SetUiVisible(Global.eUiId.buy_lot_panel, true);
        Main.s_CurTown.UpdateCurLotStatus();
        var nCurNumOfThisSize = Main.s_CurTown.GetCurBuildingLotNum(this.GetSizeType());
        var nBulkNum = 1;
        var nTotalCost = 0;
        var nBuyOnePrice = (CConfigManager.s_dicLotPrice[this.GetSizeType()])[nCurNumOfThisSize];
        var nBulkCost = 0;
        while (nTotalCost <= Main.s_CurTown.GetCoins()) {
            nBulkNum++;
            nTotalCost = 0;
            for (var i = 0; i < nBulkNum; i++) {
                var nNum = nCurNumOfThisSize + i;
                nTotalCost += (CConfigManager.s_dicLotPrice[this.GetSizeType()])[nNum];
            }
        } // end while
        nBulkNum--;
        // 至此，nBulkNum表示当前拥有的金币能批量建设的最大个数
        // 但是，还要检查一下剩余多少空地。就算有钱，没有空地也没法批量建设
        console.log("当前剩余这型空地:" + Main.s_CurTown.GetEmptyLotNum(this.GetSizeType()));
        if (nBulkNum >= 2) {
            for (var i = 0; i < nBulkNum; i++) {
                var nNum = nCurNumOfThisSize + i;
                nBulkCost += (CConfigManager.s_dicLotPrice[this.GetSizeType()])[nNum];
            }
            console.log("bulk buy:" + nBulkNum + " , " + nBulkCost);
        }
        else {
            console.log("can not bulk buy");
        }
        CUIManager.s_uiBuyLotPanel.SetParams(nBuyOnePrice, 0, 0, Main.s_CurTown.GetLotProperty(Global.eLotPsroperty.residential), Main.s_CurTown.GetLotProperty(Global.eLotPsroperty.business), Main.s_CurTown.GetLotProperty(Global.eLotPsroperty.service));
        this.SetLotLevel(1);
    };
    // 开始升级
    CObj.prototype.BeginUpgrade = function () {
        // CSoundManager.PlaySE( Global.eSE.begin_upgrade );
        // 生成一个进度条
        if (this.m_progressBar == null) {
            this.m_progressBar = CResourceManager.NewProgressBar();
            Main.s_CurTown.AddProgressBar(this.GetDistrict(), this.m_progressBar);
        }
        this.m_ConstructingAni = CResourceManager.NewConstructingAni();
        this.m_ConstructingAni.SetSize(this.GetSizeType());
        this.addChild(this.m_ConstructingAni);
        CObj.objectTemp = CObj.GetPixelSizeBySizeType(this.GetSizeType());
        this.m_ConstructingAni.x = CObj.objectTemp["x"] * 0.5;
        this.m_ConstructingAni.y = CObj.objectTemp["y"];
        this.m_ConstructingAni.SetParentObjPixelSize(this.GetSizeType(), CObj.objectTemp["x"], CObj.objectTemp["y"]);
        this.m_ConstructingAni.SetSegNum(1); // 暂时统一用一层的建筑工地  
        this.m_progressBar.scaleX = 2.5;
        this.m_progressBar.scaleY = 2.5;
        if (this.GetSizeType() == Global.eObjSize.size_2x2) {
            this.m_progressBar.x = this.GetLocationGrid().x - CDef.s_nConstructionProgressBarPosOffsetX;
            this.m_progressBar.y = this.GetLocationGrid().y - CDef.s_nConstructionProgressBarPosOffsetY;
        }
        else if (this.GetSizeType() == Global.eObjSize.size_4x4) {
            this.m_progressBar.x = this.GetLocationGrid().x - CDef.s_nConstructionProgressBarPosOffsetX;
            this.m_progressBar.y = this.GetLocationGrid().y - CDef.s_nConstructionProgressBarPosOffsetY * 1.5;
        }
        this.m_progressBar.Begin(this.m_nProcessingTotalTime);
        this.m_progressBar.SetTitle("升级");
        // poppin test 8.24
        this.m_eProcessType = Global.eProgressBarType.upgrading;
        this.m_nProcessingTotalTime = CConfigManager.GetUpgradeTimeByLotLevel(this.GetLotLevel());
        this.m_nProcessingTimeElapse = 0;
    };
    // 开始建造
    CObj.prototype.BeginConstrucingStatus = function () {
        if (this.GetBuildingLandStatus() != Global.eBuildingLandStatus.empty) {
            console.log("有bug");
            return;
        }
        // 注意：只要一开始建设，就已经算是“本地块有建筑”了，同时就开始产钱了。
        this.SetHasBuilding(true);
        this.SetBuildingLandStatus(Global.eBuildingLandStatus.contructing);
        this.m_ConstructingAni = CResourceManager.NewConstructingAni();
        this.m_ConstructingAni.SetSize(this.GetSizeType());
        this.addChild(this.m_ConstructingAni);
        CObj.objectTemp = CObj.GetPixelSizeBySizeType(this.GetSizeType());
        this.m_ConstructingAni.x = CObj.objectTemp["x"] * 0.5;
        this.m_ConstructingAni.y = CObj.objectTemp["y"];
        this.m_ConstructingAni.SetParentObjPixelSize(this.GetSizeType(), CObj.objectTemp["x"], CObj.objectTemp["y"]);
        this.m_ConstructingAni.SetSegNum(1); // 暂时统一用一层的建筑工地  
        // 生成一个进度条
        if (this.m_progressBar == null) {
            this.m_progressBar = CResourceManager.NewProgressBar();
            Main.s_CurTown.AddProgressBar(this.GetDistrict(), this.m_progressBar);
        }
        this.m_progressBar.scaleX = 2.5;
        this.m_progressBar.scaleY = 2.5;
        if (this.GetSizeType() == Global.eObjSize.size_2x2) {
            this.m_progressBar.x = this.GetLocationGrid().x - CDef.s_nConstructionProgressBarPosOffsetX;
            this.m_progressBar.y = this.GetLocationGrid().y - CDef.s_nConstructionProgressBarPosOffsetY;
        }
        else if (this.GetSizeType() == Global.eObjSize.size_4x4) {
            this.m_progressBar.x = this.GetLocationGrid().x - CDef.s_nConstructionProgressBarPosOffsetX;
            this.m_progressBar.y = this.GetLocationGrid().y - CDef.s_nConstructionProgressBarPosOffsetY * 1.5;
        }
        this.m_progressBar.Begin(CConfigManager.GetConstructingTime());
        this.m_progressBar.SetTitle("新建");
        this.m_eProcessType = Global.eProgressBarType.constructing;
        this.m_nProcessingTotalTime = 1;
        this.m_nProcessingTimeElapse = 0;
    };
    CObj.prototype.ProgressBarLoop = function () {
        if (this.m_eProcessType == Global.eProgressBarType.none) {
            return;
        }
        this.m_nProcessingTimeElapse += CDef.s_fFixedDeltaTime;
        var fPercent = this.m_nProcessingTimeElapse / this.m_nProcessingTotalTime;
        if (this.m_progressBar != null) {
            this.m_progressBar.Loop(fPercent);
        }
        if (fPercent >= 1) {
            if (this.m_eProcessType == Global.eProgressBarType.constructing) {
                this.EndConstrucingStatus();
            }
            else if (this.m_eProcessType == Global.eProgressBarType.upgrading) {
                this.EndUUpgrade();
                Main.s_CurTown.SetupgradinLot(null);
            }
        }
    };
    CObj.prototype.ProgressBarEnd = function () {
        CResourceManager.DeleteProgressBar(this.m_progressBar);
        this.m_progressBar = null;
    };
    // 取消升级 
    CObj.prototype.CancelUpgrade = function () {
        Main.s_CurTown.SetupgradinLot(null);
        this.m_eProcessType = Global.eProgressBarType.none;
        this.ProgressBarEnd();
        CResourceManager.DeleteConstructingAni(this.m_ConstructingAni);
        Main.s_CurTown.UpdateCurLotStatus();
        Main.s_CurTown.OnBuildingCPSChanged();
    };
    // 结束升级
    CObj.prototype.EndUUpgrade = function () {
        this.m_eProcessType = Global.eProgressBarType.none;
        this.ProgressBarEnd();
        CResourceManager.DeleteConstructingAni(this.m_ConstructingAni);
        this.SetLotLevel(this.GetLotLevel() + 1);
        if (!this.GetHistorical()) {
            var cur_building = this.GetBuilding();
            if (cur_building != null && cur_building.bSpecial) {
                CBuildingManager.RecycleSpecialBuilding(cur_building);
            }
            var data = CConfigManager.GetBuildingConfig(this.GetLotProperty(), this.GetSizeType(), this.m_nCurBuildingResIndex++);
            this.SetBuilding(data);
        }
        Main.s_CurTown.UpdateCurLotStatus();
        Main.s_CurTown.OnBuildingCPSChanged();
        if (this.GetDistrict() == Main.s_CurTown.m_CurDistrict) {
            var tiaozi = CTiaoZiManager.NewTiaoZi();
            tiaozi.BeginTiaoZi(Global.eTiaoZiType.level);
            tiaozi.x = this.x;
            tiaozi.y = this.y /* - CDef.s_nTileHeight * 2*/;
            tiaozi.scaleX = 1 /*2*/;
            tiaozi.scaleY = 1 /*2*/;
            // tiaozi.SetTypeAndValue( this.m_eType, this.m_nValue );
            tiaozi.SetText("lv." + this.GetLotLevel());
            // 计算出该汽车的世界坐标。因为汽车是随容易拖动和缩放的，直接取得的它的坐标是局部坐标，在此需要转化成世界坐标（舞台坐标）
            Main.s_CurTown.EndUpgrade(this);
        }
        if (this == Main.s_CurTown.GetCurEditProcessingLot()) {
            this.ProcessUpgrade();
        }
        Main.s_CurTown.SetupgradinLot(null);
    };
    // 推倒成空地
    CObj.prototype.Revert = function () {
        CBuildingManager.RecycleSpecialBuilding(this.m_BuildingData);
        this.SetBuildingLandStatus(Global.eBuildingLandStatus.empty);
        Main.s_CurTown.RemoveUpgradableLot(this);
        this.SetTexture(RES.getRes("dikuai_png"));
        this.anchorOffsetX = this.width / 2;
        this.anchorOffsetY = this.height;
        if (this.GetSizeType() == Global.eObjSize.size_2x2) {
            this.scaleX = 0.5;
            this.scaleY = 0.5;
        }
        else {
            this.scaleX = 1;
            this.scaleY = 1;
        }
        this.SetHasBuilding(false);
        if (this.m_eProcessType == Global.eProgressBarType.upgrading) {
            this.CancelUpgrade();
        }
    };
    // 结束建造
    CObj.prototype.EndConstrucingStatus = function () {
        this.m_eProcessType = Global.eProgressBarType.none;
        this.ProgressBarEnd();
        CResourceManager.DeleteConstructingAni(this.m_ConstructingAni);
        var data = CConfigManager.GetBuildingConfig(this.GetLotProperty(), this.GetSizeType(), this.m_nCurBuildingResIndex++);
        //this.SetTexture( RES.getRes( this.m_CurBuildingConfig.szResName ) );
        this.SetBuilding(data);
        this.SetBuildingLandStatus(Global.eBuildingLandStatus.building_exist);
        Main.s_CurTown.AddUpgradableLot(this);
        Main.s_CurTown.CheckIfCanUpgrde();
    };
    CObj.prototype.BuildingLandLoop = function () {
        this.ConstructingLoop();
    };
    CObj.prototype.ConstructingLoop = function () {
        if (this.m_eProcessType == Global.eProgressBarType.none) {
            return;
        }
        if (this.m_ConstructingAni != null) {
            this.m_ConstructingAni.MainLoop();
        }
    };
    CObj.prototype.GetSortPosY = function () {
        if (this.GetFuncType() == Global.eObjFunc.car) {
            this.SetSortPosY();
        }
        return this.m_fSortPosY;
    };
    CObj.prototype.SetSortPosY = function () {
        if (this.GetFuncType() == Global.eObjFunc.building) {
            if (this.GetSizeType() == Global.eObjSize.size_2x2) {
                this.m_fSortPosY = this.GetLocationGrid().y - CDef.s_nTileHeight;
            }
            else if (this.GetSizeType() == Global.eObjSize.size_4x4) {
                this.m_fSortPosY = this.GetLocationGrid().y - CDef.s_nTileHeight * 2;
            }
        }
        else if (this.GetFuncType() == Global.eObjFunc.car) {
            this.m_fSortPosY = this.y;
        }
    };
    CObj.prototype.SetDebugInfo = function (szDebugInfo) {
        this.m_txtDebugInfo.text = szDebugInfo;
    };
    CObj.prototype.IsBuildinSpecial = function () {
        return this.m_eBuildinSpecial != Global.eBuildinSpeical.none;
    };
    CObj.prototype.SetBuildinSpecial = function (eType) {
        this.m_eBuildinSpecial = eType;
    };
    CObj.prototype.GetBuildinSpecial = function () {
        return this.m_eBuildinSpecial;
    };
    CObj.prototype.HasBuilding = function () {
        return this.m_bHasBuilding;
    };
    CObj.prototype.SetHasBuilding = function (bHas) {
        this.m_bHasBuilding = bHas;
    };
    CObj.prototype.SetLotLevel = function (nLevel) {
        this.m_nLotLevel = nLevel;
    };
    CObj.prototype.GetLotLevel = function () {
        return this.m_nLotLevel;
    };
    CObj.prototype.SetLotProperty = function (eLotProperty) {
        this.m_eLotProperty = eLotProperty;
    };
    CObj.prototype.GetLotProperty = function () {
        return this.m_eLotProperty;
    };
    CObj.prototype.SetHistorical = function (bHistorical) {
        this.m_bHistorical = bHistorical;
    };
    CObj.prototype.GetHistorical = function () {
        return this.m_bHistorical;
    };
    CObj.prototype.SetBuilding = function (data) {
        this.SetTexture(RES.getRes(data.szResName));
        this.m_BuildingData = data;
        this.anchorOffsetX = this.width * 0.5;
        this.anchorOffsetY = this.height;
        this.scaleX = 0.75;
        this.scaleY = 0.75;
    };
    CObj.prototype.GetBuilding = function () {
        return this.m_BuildingData;
    };
    CObj.prototype.GetTreeResId = function () {
        return 0;
    };
    CObj.prototype.handleTapGarage = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.garage, true);
        Main.s_CurTown.UpdateGarageStatus();
    };
    CObj.prototype.SetSpeed = function (fSpeed) {
        this.m_fSpeed = fSpeed;
    };
    CObj.prototype.GetSpeed = function () {
        return this.m_fSpeed;
    };
    CObj.prototype.GetTime = function () {
        return this.m_fTime;
    };
    CObj.prototype.SetTime = function (fTime) {
        this.m_fTime = fTime;
    };
    CObj.prototype.GetParams = function () {
        return this.m_objectParams;
    };
    CObj.prototype.SetColor = function (r, g, b) {
        this.m_bmpMainPic.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b)];
    };
    CObj.prototype.SetParams = function (key, val) {
        this.m_objectParams[key] = val;
    };
    CObj.prototype.GetParamsByKey = function (key) {
        return this.m_objectParams[key];
    };
    CObj.prototype.SetDistrict = function (district) {
        this.m_BelongToDistrict = district;
    };
    CObj.prototype.GetDistrict = function () {
        return this.m_BelongToDistrict;
    };
    CObj.prototype.SetMoveParams = function (vecDestPos, vecCurPos, nTime) {
        this.m_vecDestPos.x = vecDestPos.x;
        this.m_vecDestPos.y = vecDestPos.y;
        this.m_vecSpeed.x = (this.m_vecDestPos.x - vecCurPos.x) / nTime * CDef.s_fFixedDeltaTime;
        this.m_vecSpeed.y = (this.m_vecDestPos.y - vecCurPos.y) / nTime * CDef.s_fFixedDeltaTime;
    };
    CObj.prototype.GetDestPos = function () {
        return this.m_vecDestPos;
    };
    CObj.prototype.MoveToDest = function () {
        this.x += this.m_vecSpeed.x;
        this.y += this.m_vecSpeed.y;
        var bEndX = false;
        var bEndY = false;
        if (this.m_vecSpeed.x > 0) {
            if (this.x >= this.m_vecDestPos.x) {
                bEndX = true;
            }
        }
        else if (this.m_vecSpeed.x < 0) {
            if (this.x <= this.m_vecDestPos.x) {
                bEndX = true;
            }
        }
        else {
            bEndX = true;
        }
        if (this.m_vecSpeed.y > 0) {
            if (this.y >= this.m_vecDestPos.y) {
                bEndY = true;
            }
        }
        else if (this.m_vecSpeed.y < 0) {
            if (this.y <= this.m_vecDestPos.y) {
                bEndY = true;
            }
        }
        else {
            bEndY = true;
        }
        if (bEndX) {
            this.x = this.m_vecDestPos.x;
        }
        if (bEndY) {
            this.y = this.m_vecDestPos.y;
        }
        if (bEndX && bEndY) {
            return true;
        }
        return false;
    };
    CObj.objectTemp = new Object();
    return CObj;
}(egret.DisplayObjectContainer)); // end class CObj
__reflect(CObj.prototype, "CObj");
//# sourceMappingURL=CObj.js.map