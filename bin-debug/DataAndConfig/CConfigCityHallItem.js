var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CConfigCityHallItem = (function (_super) {
    __extends(CConfigCityHallItem, _super);
    function CConfigCityHallItem() {
        var _this = _super.call(this) || this;
        _this.szDesc = "";
        _this.szName = "";
        _this.nGain = 0;
        _this.eShowStyle = Global.eValueShowStyle.directly;
        _this.aryValuesCost = new Array();
        return _this;
    } // end constructor
    CConfigCityHallItem.prototype.GetMaxLevel = function () {
        return this.aryValuesCost.length;
    };
    return CConfigCityHallItem;
}(egret.DisplayObjectContainer)); // end class
__reflect(CConfigCityHallItem.prototype, "CConfigCityHallItem");
//# sourceMappingURL=CConfigCityHallItem.js.map