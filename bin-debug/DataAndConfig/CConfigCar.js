var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CConfigCar = (function (_super) {
    __extends(CConfigCar, _super);
    function CConfigCar() {
        var _this = _super.call(this) || this;
        _this.szDesc = "";
        _this.szName = "";
        _this.nPrice = 0;
        return _this;
    } // end constructor
    return CConfigCar;
}(egret.DisplayObjectContainer)); // end class
__reflect(CConfigCar.prototype, "CConfigCar");
//# sourceMappingURL=CConfigCar.js.map