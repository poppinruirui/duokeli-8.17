var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CGreenMoneymanager = (function (_super) {
    __extends(CGreenMoneymanager, _super);
    function CGreenMoneymanager() {
        return _super.call(this) || this;
    }
    CGreenMoneymanager.SetPigGreenNum = function (num) {
        CGreenMoneymanager.s_nPigGreenNum = num;
        CUIManager.s_uiCPosButton.SetPigGreenNum(num);
    };
    CGreenMoneymanager.GetPigGreenNum = function () {
        return CGreenMoneymanager.s_nPigGreenNum;
    };
    CGreenMoneymanager.FixedUpdate = function () {
        CGreenMoneymanager.FlyLoop();
    };
    CGreenMoneymanager.FlyLoop = function () {
        for (var i = CGreenMoneymanager.s_lstContainerFlyingGreen.numChildren - 1; i >= 0; i--) {
            var green = CGreenMoneymanager.s_lstContainerFlyingGreen.getChildAt(i);
            if (green.MoveToDest()) {
                CResourceManager.DeleteFlyJinBi(green);
                var effect = CFrameAniManager.NewEffect();
                effect.SetParams("1a_", 12, 0, false);
                effect.Play();
                effect.x = green.GetDestPos().x;
                effect.y = green.GetDestPos().y;
                effect.scaleX = 0.8;
                effect.scaleY = 0.8;
                var szType = green.GetParamsByKey("MoveType");
                if (szType == "FlyToMainTitle") {
                }
                else if (szType == "FlyToPig") {
                    CGreenMoneymanager.SetPigGreenNum(CGreenMoneymanager.GetPigGreenNum() + 1);
                }
            }
        }
    };
    CGreenMoneymanager.GenerateOneGreenMoneyToFly = function (nType, nCurPosX, nCurPosY) {
        CGreenMoneymanager.vecTempPos1.x = nCurPosX;
        CGreenMoneymanager.vecTempPos1.y = nCurPosY;
        var green = CResourceManager.NewFlyJinBi();
        green.SetTexture(RES.getRes("diamond_png"));
        if (nType == 0) {
            CGreenMoneymanager.vecTempPos.x = 20;
            CGreenMoneymanager.vecTempPos.y = 20;
            green.SetMoveParams(CGreenMoneymanager.vecTempPos, CGreenMoneymanager.vecTempPos1, 0.5);
            green.SetParams("MoveType", "FlyToMainTitle");
        }
        else if (nType == 1) {
            CGreenMoneymanager.vecTempPos.x = CUIManager.s_uiCPosButton.GetPigButton().x + 49;
            CGreenMoneymanager.vecTempPos.y = CUIManager.s_uiCPosButton.GetPigButton().y + 49;
            green.SetMoveParams(CGreenMoneymanager.vecTempPos, CGreenMoneymanager.vecTempPos1, 0.3);
            green.SetParams("MoveType", "FlyToPig");
        }
        CGreenMoneymanager.s_lstContainerFlyingGreen.addChild(green);
        green.scaleX = 0.8;
        green.scaleY = 0.8;
        green.anchorOffsetX = green.width * 0.5;
        green.anchorOffsetY = green.height * 0.5;
        green.x = nCurPosX;
        green.y = nCurPosY;
    };
    CGreenMoneymanager.vecTempPos = new CVector();
    CGreenMoneymanager.vecTempPos1 = new CVector();
    CGreenMoneymanager.s_lstContainerFlyingGreen = new egret.DisplayObjectContainer();
    CGreenMoneymanager.s_nPigGreenNum = 0;
    return CGreenMoneymanager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CGreenMoneymanager.prototype, "CGreenMoneymanager");
//# sourceMappingURL=CGreenMoneyManager.js.map