var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CConfigManager = (function (_super) {
    __extends(CConfigManager, _super);
    //// end 建筑相关配置
    function CConfigManager() {
        return _super.call(this) || this;
    }
    CConfigManager.Init = function () {
        // 地块购买的价格
        var ary = new Array();
        ary.push(2000);
        ary.push(6112);
        ary.push(68800);
        ary.push(207861);
        ary.push(460914);
        ary.push(869595);
        ary.push(1480000);
        ary.push(2340000);
        ary.push(3500000);
        ary.push(5010000);
        ary.push(6930000);
        ary.push(9320000);
        ary.push(12240000);
        ary.push(15760000);
        ary.push(19930000);
        ary.push(24820000);
        ary.push(30500000);
        ary.push(37050000);
        ary.push(44530000);
        ary.push(53020000);
        ary.push(62590000);
        ary.push(73310000);
        ary.push(85280000);
        ary.push(98560000);
        ary.push(113230000);
        ary.push(129390000);
        ary.push(187560000);
        ary.push(210480000);
        ary.push(235310000);
        ary.push(262130000);
        ary.push(291040000);
        ary.push(322140000);
        ary.push(355500000);
        ary.push(391240000);
        ary.push(429440000);
        ary.push(470200000);
        ary.push(513610000);
        ary.push(559790000);
        ary.push(608810000);
        ary.push(660800000);
        ary.push(715840000);
        ary.push(774040000);
        // 2x2地块的价格列表
        CConfigManager.s_dicLotPrice[Global.eObjSize.size_2x2] = ary;
        // 4x4地块的价格列表
        ary = new Array();
        ary.push(314773);
        ary.push(5190000);
        ary.push(24800000);
        CConfigManager.s_dicLotPrice[Global.eObjSize.size_4x4] = ary;
        CConfigManager.s_dicLot2Population[Global.eObjSize.size_2x2] = 1000;
        // CConfigManager.s_dicLot2Population[Global.eObjSize.size_4x4] = ??;
        // 地块每个等级的金币贡献率
        ary = new Array();
        for (var i = 1; i <= 100; i++) {
            ary.push(25 * i);
        }
        CConfigManager.s_dicLevel2Coin[Global.eObjSize.size_2x2] = ary;
        ary = new Array();
        for (var i = 1; i <= 100; i++) {
            ary.push(25 * i);
        }
        CConfigManager.s_dicLevel2Coin[Global.eObjSize.size_4x4] = ary;
        // 解锁汽车所需要的金币数
        /*
        CConfigManager.s_aryCoinForUnlockCar.push(12664);
        CConfigManager.s_aryCoinForUnlockCar.push(14843);
        CConfigManager.s_aryCoinForUnlockCar.push(22399);
         CConfigManager.s_aryCoinForUnlockCar.push(48594);
       CConfigManager.s_aryCoinForUnlockCar.push(139409);
          */
        for (var i = 0; i < CConfigManager.s_nCarTotalNum; i++) {
            // CConfigManager.s_aryCoinForUnlockCar.push(100 * i);
            var car_config = new CConfigCar();
            car_config.nPrice = 100 * (i + 1);
            car_config.szName = CConfigManager.s_aryCarName[i];
            car_config.szDesc = CConfigManager.s_aryCarDesc[i];
            CConfigManager.s_aryCars.push(car_config);
        }
        // 钱币相关配置
        CConfigManager.s_dicMoneyRateOfCPS[Global.eMoneySubType.small_coin] = 0.5;
        CConfigManager.s_dicMoneyRateOfCPS[Global.eMoneySubType.big_coin] = 5;
        CConfigManager.s_dicMoneyRateOfCPS[Global.eMoneySubType.small_diamond] = 1;
        CConfigManager.s_dicMoneyRateOfCPS[Global.eMoneySubType.big_diamond] = 10;
        // 升级相关配置
        for (var i = 1; i < 1000; i++) {
            var nTime = i;
            if (nTime < 3) {
                nTime = 3;
            }
            CConfigManager.s_aryUpgadeTime.push(nTime);
        }
        var aryLotCPSbyLevel = new Array();
        for (var i = 1; i < 100; i++) {
            aryLotCPSbyLevel.push(i * 25);
        }
        CConfigManager.s_dicLotCPSByLevel[Global.eObjSize.size_2x2] = aryLotCPSbyLevel;
        CConfigManager.s_dicLotCPSByLevel[Global.eObjSize.size_4x4] = aryLotCPSbyLevel;
        var aryDiamondForInstantUpgrade = new Array();
        for (var i = 1; i < 100; i++) {
            aryDiamondForInstantUpgrade.push(Math.sqrt(5 * i));
        }
        CConfigManager.s_dicNeedDiamondsForInstantUpgrade[Global.eObjSize.size_2x2] = aryDiamondForInstantUpgrade;
        CConfigManager.s_dicNeedDiamondsForInstantUpgrade[Global.eObjSize.size_4x4] = aryDiamondForInstantUpgrade;
        CConfigManager.InitCityHall_City();
        CConfigManager.InitCityHall_Game();
        //// 银行相关
        var nCurBankCost = 0;
        for (var i = 1; i < 50; i++) {
            if (i == 1) {
                nCurBankCost = CConfigManager.s_nBankCostStartValue;
            }
            else {
                nCurBankCost *= CConfigManager.s_nBankCostChangeRate;
                CConfigManager.s_dicBankCost[i] = nCurBankCost;
            }
            CConfigManager.s_dicBankCost[i] = nCurBankCost;
        }
        //// end 银行相关
        CConfigManager.InitCityLevel();
        //// 建筑相关
        CConfigManager.InitBuildings();
        CBuildingManager.LoadConfig();
        //// end 建筑相关 
    };
    CConfigManager.GenShowStyle = function (eStyle, nValue) {
        if (nValue < 0) {
            nValue = -nValue;
        }
        var szContent = nValue.toString();
        switch (eStyle) {
            case Global.eValueShowStyle.percent:
                {
                    var nShowValue = Math.floor(nValue * 100);
                    szContent = nShowValue + "%";
                }
                break;
        }
        return szContent;
    };
    CConfigManager.GetCityHallItemConfig = function (nPageIndex, nItemIndex) {
        var dic = null;
        if (nPageIndex == 0) {
            dic = CConfigManager.s_dicCityHallCity;
        }
        else {
            dic = CConfigManager.s_dicCityHallGame;
        }
        if (dic == null || dic == undefined) {
            return null;
        }
        var config = dic[nItemIndex];
        if (config == undefined) {
            config = null;
        }
        return config;
    };
    CConfigManager.InitCityHall_Game = function () {
        var config = new CConfigCityHallItem();
        config.szName = "日间交易";
        config.szDesc = "银行持续产出时间提升";
        config.nGain = 0.25;
        config.eShowStyle = Global.eValueShowStyle.percent;
        var nCurValue = 0;
        var bFirst = true;
        for (var i = 0; i < 10; i++) {
            if (bFirst) {
                nCurValue = 40;
                bFirst = false;
            }
            else {
                nCurValue = nCurValue * 1.397;
            }
            config.aryValuesCost.push(nCurValue);
        } // end for 
        CConfigManager.s_dicCityHallGame[0] = config;
    };
    CConfigManager.InitCityHall_City = function () {
        var config = new CConfigCityHallItem();
        config.szName = "物业税";
        config.szDesc = "建筑的金币收益提升";
        config.nGain = 0.1;
        config.eShowStyle = Global.eValueShowStyle.percent;
        var nCurValue = 0;
        var bFirst = true;
        for (var i = 0; i < 50; i++) {
            if (bFirst) {
                nCurValue = 25;
                bFirst = false;
            }
            else {
                nCurValue = nCurValue * 1.16;
            }
            config.aryValuesCost.push(nCurValue);
        } // end for 
        CConfigManager.s_dicCityHallCity[0] = config;
    };
    CConfigManager.GetBuildingConfigByType = function (eType) {
        return CConfigManager.s_aryBuildingConfig[eType];
    };
    CConfigManager.GetBuyLotInfo = function (eSizeType, nCurTotalNum) {
        CConfigManager.s_dicTempData["BuyOnePrice"] = (CConfigManager.s_dicLotPrice[eSizeType])[nCurTotalNum];
        return CConfigManager.s_dicTempData;
    };
    CConfigManager.GetLot2Poulation = function (eSizeType) {
        if (CConfigManager.s_dicLot2Population[eSizeType] == undefined) {
            return 0;
        }
        return CConfigManager.s_dicLot2Population[eSizeType];
    };
    CConfigManager.GetBuildingCoinBySizeAndLevel = function (eSizeType, nLotLevel) {
        if (CConfigManager.s_dicLevel2Coin[eSizeType] == undefined) {
            return 0;
        }
        var ary = CConfigManager.s_dicLevel2Coin[eSizeType];
        var nIdx = nLotLevel - 1;
        if (nIdx < 0 || nIdx >= ary.length) {
            return 0;
        }
        return ary[nIdx];
    };
    CConfigManager.GetPopulation2CoinXiShu = function () {
        return CConfigManager.s_nPopulation2CoinXiShu;
    };
    CConfigManager.GetCoinForUnlockCar = function (nCarNum) {
        if (nCarNum >= CConfigManager.s_aryCars.length) {
            return 10000000000;
        }
        var car_config = CConfigManager.s_aryCars[nCarNum];
        return car_config.nPrice;
    };
    CConfigManager.GetCarConfig = function (nIndex) {
        if (nIndex >= CConfigManager.s_aryCars.length) {
            return null;
        }
        return CConfigManager.s_aryCars[nIndex];
    };
    CConfigManager.GetCarJinBiValueRate = function (eType) {
        return CConfigManager.s_dicMoneyRateOfCPS[eType];
    };
    CConfigManager.GetConstructingTime = function () {
        return CConfigManager.s_nConstructingTime;
    };
    CConfigManager.GetBuildingTextureByLevel = function (nLotLevel) {
        return "yigoudi_png";
    };
    CConfigManager.GetEmptyLotTextureBySize = function (eSizeType) {
        if (eSizeType == Global.eObjSize.size_2x2) {
            return RES.getRes("dikuai_2_2_png");
        }
        else if (eSizeType == Global.eObjSize.size_4x4) {
            return RES.getRes("dikuai_4_4_png");
        }
        return null;
    };
    CConfigManager.GetUpgradeTimeByLotLevel = function (nLotLevel) {
        return 1; // ( nLotLevel );
    };
    CConfigManager.GetLotCPSByLevel = function (eSizeType, nLotLevel) {
        var ary = CConfigManager.s_dicLotCPSByLevel[eSizeType];
        return ary[nLotLevel];
    };
    CConfigManager.GetNeedDiamondBySizeAndLevel = function (eSizeType, nLotLevel) {
        var ary = CConfigManager.s_dicNeedDiamondsForInstantUpgrade[eSizeType];
        return ary[nLotLevel];
    };
    CConfigManager.GetFastforwardCost = function (nIndex) {
        switch (nIndex) {
            case 0:
                {
                    return 50;
                }
                break;
            case 1:
                {
                    return 250;
                }
                break;
        }
    };
    /////// 银行相关 
    CConfigManager.GetBankTimeChangeAmount = function () {
        return CConfigManager.s_nBankTimeChangeAmount;
    };
    CConfigManager.GetBankProfitRate = function () {
        return CConfigManager.s_nBankSaveRate;
    };
    CConfigManager.GetBankCostByLevel = function (nLevel) {
        return CConfigManager.s_dicBankCost[nLevel];
    };
    /////// end 银行相关
    ////  城市升级相关
    CConfigManager.InitCityLevel = function () {
        // 先写死，稍后做读配置表流程
        var config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 4000;
        config.nStartCoins = 50000;
        config.nKeys = 1;
        config.szName = "浮云小镇";
        CConfigManager.s_dicCityLevelConfig[1] = config;
        config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 10000;
        config.nStartCoins = 100000;
        config.nKeys = 2;
        config.szName = "闪金平原";
        CConfigManager.s_dicCityLevelConfig[2] = config;
        config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 33000;
        config.nStartCoins = 150000;
        config.nKeys = 2;
        config.szName = "镖客镇";
        CConfigManager.s_dicCityLevelConfig[3] = config;
        config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 69000;
        config.nStartCoins = 200000;
        config.nKeys = 2;
        config.szName = "宝石湾";
        CConfigManager.s_dicCityLevelConfig[4] = config;
        config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 138000;
        config.nStartCoins = 250000;
        config.nKeys = 2;
        config.szName = "灰烬之海";
        CConfigManager.s_dicCityLevelConfig[5] = config;
        config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 276000;
        config.nStartCoins = 300000;
        config.nKeys = 2;
        config.szName = "银白山峰";
        CConfigManager.s_dicCityLevelConfig[6] = config;
        config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 544000;
        config.nStartCoins = 300000;
        config.nKeys = 2;
        config.szName = "月夜森林";
        CConfigManager.s_dicCityLevelConfig[7] = config;
        config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 1110000;
        config.nStartCoins = 400000;
        config.nKeys = 2;
        config.szName = "亚特兰蒂斯";
        CConfigManager.s_dicCityLevelConfig[8] = config;
    };
    CConfigManager.GetCityLevelConfig = function (nLevel) {
        return CConfigManager.s_dicCityLevelConfig[nLevel];
    };
    CConfigManager.GenerateBuildingConfigGuid = function () {
        return (CConfigManager.s_nBuildingConfigGuid++);
    };
    CConfigManager.PushBuildingConfig2Array = function (ary, config) {
        ary.push(config);
        config.nID = CConfigManager.GenerateBuildingConfigGuid();
    };
    //// 建筑物相关
    CConfigManager.InitBuildings = function () {
        //// 普通建筑
        var aryResidential = new Array();
        var aryResidential_4x4 = new Array();
        var config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "yigoudi_png";
        config.szName = "荒芜之地";
        //aryResidential.push(config);
        CConfigManager.PushBuildingConfig2Array(aryResidential, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "zhangpeng_png";
        config.szName = "帐篷";
        // aryResidential.push(config);
        CConfigManager.PushBuildingConfig2Array(aryResidential, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "mingjun_png";
        config.szName = "小民居";
        //aryResidential.push(config);
        CConfigManager.PushBuildingConfig2Array(aryResidential, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "shangpinglou_png";
        config.szName = "豪华公寓";
        //aryResidential.push(config);
        CConfigManager.PushBuildingConfig2Array(aryResidential, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "bieshu_png";
        config.szName = "别墅1";
        //aryResidential.push(config);
        CConfigManager.PushBuildingConfig2Array(aryResidential, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "fangche_png";
        config.szName = "房车";
        CConfigManager.PushBuildingConfig2Array(aryResidential, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "guoyuanxiaozhen_png";
        config.szName = "果园小镇";
        CConfigManager.PushBuildingConfig2Array(aryResidential, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "libaxiaozhen_png";
        config.szName = "篱笆小镇";
        CConfigManager.PushBuildingConfig2Array(aryResidential, config);
        CConfigManager.s_dicBuildings[Global.eLotPsroperty.residential + "_" + Global.eObjSize.size_2x2] = aryResidential;
        ////////
        var aryBusiness = new Array();
        var aryBusiness_4x4 = new Array();
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "zidongshouhuoji_png";
        config.szName = "自动售货机";
        // aryBusiness.push(config);  
        CConfigManager.PushBuildingConfig2Array(aryBusiness, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "ditan-shucai_png";
        config.szName = "低碳蔬菜";
        // aryBusiness.push(config); 
        CConfigManager.PushBuildingConfig2Array(aryBusiness, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "kuaichandian_png";
        config.szName = "快餐店";
        // aryBusiness.push(config); 
        CConfigManager.PushBuildingConfig2Array(aryBusiness, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "bianlidian_png";
        config.szName = "便利店";
        //  aryBusiness.push(config);
        CConfigManager.PushBuildingConfig2Array(aryBusiness, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "kafeidian_png";
        config.szName = "咖啡店";
        CConfigManager.PushBuildingConfig2Array(aryBusiness, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "naichadian_png";
        config.szName = "奶茶店";
        CConfigManager.PushBuildingConfig2Array(aryBusiness, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "shousidian_png";
        config.szName = "寿司店";
        CConfigManager.PushBuildingConfig2Array(aryBusiness, config);
        /*
                config = new CConfigBuilding();
                config.ePropertyType = Global.eLotPsroperty.business;
                config.szResName = "shaoshi_png";
                config.szName = "超市";
                //aryBusiness.push(config);
                 CConfigManager.PushBuildingConfig2Array( aryBusiness, config );
        */
        CConfigManager.s_dicBuildings[Global.eLotPsroperty.business + "_" + Global.eObjSize.size_2x2] = aryBusiness;
        //////
        ////////
        var aryService = new Array();
        var aryService_4x4 = new Array();
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "shuita_png";
        config.szName = "水塔";
        // aryService.push(config);  
        CConfigManager.PushBuildingConfig2Array(aryService, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "changku_png";
        config.szName = "仓库";
        //aryService.push(config);  
        CConfigManager.PushBuildingConfig2Array(aryService, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "xuexiao_png";
        config.szName = "学校";
        // aryService.push(config);  
        CConfigManager.PushBuildingConfig2Array(aryService, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "youju_png";
        config.szName = "邮局";
        // aryService.push(config); 
        CConfigManager.PushBuildingConfig2Array(aryService, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "bashichezhan_png";
        config.szName = "巴士车站";
        CConfigManager.PushBuildingConfig2Array(aryService, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "jingchaju_png";
        config.szName = "警察局";
        CConfigManager.PushBuildingConfig2Array(aryService, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "gonggongzaotang_png";
        config.szName = "公共澡堂";
        CConfigManager.PushBuildingConfig2Array(aryService, config);
        /*
              config = new CConfigBuilding();
              config.ePropertyType = Global.eLotPsroperty.service;
              config.szResName = "huodianchang_png";
              config.szName = "火电厂";
              aryService.push(config);
              CConfigManager.PushBuildingConfig2Array( aryService, config );
      */
        CConfigManager.s_dicBuildings[Global.eLotPsroperty.service + "_" + Global.eObjSize.size_2x2] = aryService;
        //// end 普通建筑 ==================================================
        //// ================================== 以下为“特殊建筑” ==========================================
        //// Service
        var aryService_Special = new Array();
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "huodianchang_png";
        config.szName = "火电厂";
        config.fGainRate = 1;
        config.nPrice = 3;
        config.bSpecial = true;
        CConfigManager.PushBuildingConfig2Array(aryService_Special, config);
        CConfigManager.s_dicSpecialBuildings[Global.eLotPsroperty.service + "_" + Global.eObjSize.size_2x2] = aryService_Special;
        //// Business
        var aryBusiness_Special = new Array();
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "shaoshi_png";
        config.szName = "超市";
        config.fGainRate = 1;
        config.nPrice = 3;
        config.bSpecial = true;
        CConfigManager.PushBuildingConfig2Array(aryBusiness_Special, config);
        CConfigManager.s_dicSpecialBuildings[Global.eLotPsroperty.business + "_" + Global.eObjSize.size_2x2] = aryBusiness_Special;
        //// Residential
        var aryResidential_Special = new Array();
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "bieshu_png";
        config.szName = "别墅";
        config.fGainRate = 1;
        config.nPrice = 3;
        config.bSpecial = true;
        CConfigManager.PushBuildingConfig2Array(aryResidential_Special, config);
        CConfigManager.s_dicSpecialBuildings[Global.eLotPsroperty.residential + "_" + Global.eObjSize.size_2x2] = aryResidential_Special;
        //////////////////////////////////// 以下为4z4 ///////////////////////////////////
        //////////////////////////////////// 以下为4z4 ///////////////////////////////////
        //////////////////////////////////// 以下为4z4 ///////////////////////////////////
        //////// ！---- 住宅类
        //// 普通
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "mogudi_png";
        config.szName = "蘑菇地";
        CConfigManager.PushBuildingConfig2Array(aryResidential_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "kaixinxiaoqu_png";
        config.szName = "开心小区";
        CConfigManager.PushBuildingConfig2Array(aryResidential_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "qiuribieshu_png";
        config.szName = "秋日别墅";
        CConfigManager.PushBuildingConfig2Array(aryResidential_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "youxianxiaoqu_png";
        config.szName = "悠闲小区";
        CConfigManager.PushBuildingConfig2Array(aryResidential_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "xiaozhen_png";
        config.szName = "小镇";
        CConfigManager.PushBuildingConfig2Array(aryResidential_4x4, config);
        CConfigManager.s_dicBuildings[Global.eLotPsroperty.residential + "_" + Global.eObjSize.size_4x4] = aryResidential_4x4;
        //////// ！---- 商业
        //// 普通
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "dimiantingchechang_png";
        config.szName = "地面停车场";
        CConfigManager.PushBuildingConfig2Array(aryBusiness_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "baihuoshangdian_png";
        config.szName = "百货商店";
        CConfigManager.PushBuildingConfig2Array(aryBusiness_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "songlinmuchang_png";
        config.szName = "松林木场";
        CConfigManager.PushBuildingConfig2Array(aryBusiness_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "xiezilou_png";
        config.szName = "写字楼";
        CConfigManager.PushBuildingConfig2Array(aryBusiness_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "shangyequ_png";
        config.szName = "商业区";
        CConfigManager.PushBuildingConfig2Array(aryBusiness_4x4, config);
        CConfigManager.s_dicBuildings[Global.eLotPsroperty.business + "_" + Global.eObjSize.size_4x4] = aryBusiness_4x4;
        //////// ！---- 服务
        //// 服务
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "hupo_png";
        config.szName = "湖泊";
        CConfigManager.PushBuildingConfig2Array(aryService_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "dashu_png";
        config.szName = "大树";
        CConfigManager.PushBuildingConfig2Array(aryService_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "hupangongyuan_png";
        config.szName = "湖畔公园";
        CConfigManager.PushBuildingConfig2Array(aryService_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "zongheyiyuan_png";
        config.szName = "综合医院";
        CConfigManager.PushBuildingConfig2Array(aryService_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "zuqiuyundongchang_png";
        config.szName = "足球场";
        CConfigManager.PushBuildingConfig2Array(aryService_4x4, config);
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "xiaofangdui_png";
        config.szName = "消防队";
        CConfigManager.PushBuildingConfig2Array(aryService_4x4, config);
        CConfigManager.s_dicBuildings[Global.eLotPsroperty.service + "_" + Global.eObjSize.size_4x4] = aryService_4x4;
        ////  特殊
        /*
         var aryResidential_Special_4x4:Array<CConfigBuilding> = new Array<CConfigBuilding>();
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "qiuribieshu_png";
        config.szName = "秋日别墅";
        aryResidential_Special_4x4.push(config);
        CConfigManager.PushBuildingConfig2Array( aryResidential_Special_4x4, config );
        CConfigManager.s_dicSpecialBuildings[Global.eLotPsroperty.residential + "_" + Global.eObjSize.size_4x4] = aryResidential_Special_4x4;
*/
    };
    CConfigManager.GetBuildingConfigDic = function () {
        return CConfigManager.s_dicBuildings;
    };
    CConfigManager.GetSpecialBuildingConfigDic = function () {
        return CConfigManager.s_dicSpecialBuildings;
    };
    CConfigManager.GetBuildingConfig = function (eType, eSizeType, nIndex) {
        var ary = CConfigManager.s_dicBuildings[eType + "_" + eSizeType];
        if (ary == undefined || ary == null) {
            return null;
        }
        if (ary.length == 0) {
            return;
        }
        if (nIndex >= ary.length) {
            nIndex = nIndex % ary.length;
        }
        return ary[nIndex];
    };
    CConfigManager.s_dicLotPrice = new Object(); // 每一个地块的价格
    CConfigManager.s_dicLot2Population = new Object(); // 每一个地块增加的人口数
    CConfigManager.s_nPopulation2CoinXiShu = 0.05; // 人口对金币产出的系数。 比如当前人口3000，则金币产出能力是3000 * 0.05 = 150/秒
    CConfigManager.s_dicLevel2Coin = new Object(); // 每个等级的建筑（其实跟建筑没多大关系，是地块）每秒产出金币数量
    CConfigManager.s_dicTempData = new Object();
    //  protected static s_aryCoinForUnlockCar:Array<number> = new Array<number>();
    CConfigManager.s_aryCars = new Array();
    CConfigManager.s_nCarTotalNum = 10;
    CConfigManager.s_aryCarName = [
        "小汽车（蓝）",
        "小汽车（粉）",
        "SUV（白）",
        "面包车",
        "卡车",
        "跑车（红）",
        "警车",
        "出租车",
        "园丁车",
        "冰淇淋车",
    ];
    CConfigManager.s_aryCarDesc = [
        "随处可见的小汽车（蓝）",
        "惊艳的颜色",
        "一家人去春游吧",
        "和面包没有一点关系",
        "喂？搬家公司吗？",
        "做一个风一样的男子",
        "乌拉乌拉乌拉，制服真帅气",
        "最地道的当地导游~",
        "路过都是花香",
        "哇~香甜的味道，我想要草莓牛奶味的。",
    ];
    CConfigManager.s_nTotalCarNum = 14;
    //// 钱币相关配置 
    CConfigManager.s_dicMoneyRateOfCPS = new Object();
    //// 建筑升级的相关配置
    CConfigManager.s_nConstructingTime = 3;
    CConfigManager.s_aryUpgadeTime = new Array();
    CConfigManager.s_dicLotCPSByLevel = new Object();
    CConfigManager.s_dicNeedDiamondsForInstantUpgrade = new Object();
    //// “CityHall相关配置”
    CConfigManager.s_dicCityHallCity = new Object();
    CConfigManager.s_dicCityHallGame = new Object();
    //// 银行相关配置
    CConfigManager.s_nBankTimeChangeAmount = 15;
    CConfigManager.s_nBankSaveRate = 0.1;
    CConfigManager.s_nBankCostStartValue = 31623;
    CConfigManager.s_nBankCostChangeRate = 1.3162;
    CConfigManager.s_dicBankCost = new Object();
    //// end  银行相关配置
    //// 城市升级相关配置
    CConfigManager.s_dicCityLevelConfig = new Object();
    //// 
    //// 建筑相关配置
    CConfigManager.s_dicBuildings = new Object();
    CConfigManager.s_dicSpecialBuildings = new Object();
    CConfigManager.s_aryBuildingConfig = new Array();
    //// end  城市升级相关
    CConfigManager.s_nBuildingConfigGuid = 0;
    return CConfigManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CConfigManager.prototype, "CConfigManager");
//# sourceMappingURL=CConfigManager.js.map