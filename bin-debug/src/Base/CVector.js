var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CVector = (function (_super) {
    __extends(CVector, _super);
    function CVector() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.id = 0;
        _this.x = 0;
        _this.y = 0;
        _this.z = 0;
        return _this;
    }
    return CVector;
}(egret.DisplayObjectContainer)); // end class
__reflect(CVector.prototype, "CVector");
//# sourceMappingURL=CVector.js.map