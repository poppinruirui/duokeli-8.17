var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CAutomobile = (function (_super) {
    __extends(CAutomobile, _super);
    function CAutomobile() {
        var _this = _super.call(this) || this;
        _this.m_shapeFortap = new egret.Shape();
        _this.m_bmpMoney = new egret.Bitmap(); // （头顶）钱币
        _this.m_Dir = Global.eAutomobileDir.youshang;
        _this.m_CurRoad = null;
        _this.m_NextRoad = null;
        _this.m_shapeLocationPoint = new egret.Shape();
        _this.m_eStatus = Global.eAutomobileStatus.none;
        _this.m_nSubStatus = 0;
        _this.m_nTempMoveAmount = 0;
        _this.m_fTempGridPosX = 0;
        _this.m_fTempGridPosY = 0;
        _this.m_JinBi = null;
        _this.m_bHasCoinNow = false;
        _this.m_nCarIndex = 0;
        _this.m_fBoundary_P1_X = 0;
        _this.m_fBoundary_P1_Y = 0;
        _this.m_fBoundary_P2_X = 0;
        _this.m_fBoundary_P2_Y = 0;
        _this.m_fReInsertSortTimeElapse = 0;
        _this.m_nGenCoinTimeElapse = 0;
        _this.addChild(_this.m_bmpMoney);
        _this.addChild(_this.m_bmpMainPic);
        _this.scaleX = 0.5;
        _this.scaleY = 0.5;
        //this.m_bmpMoney.texture = RES.getRes ( "" );
        /*
                 this.m_shapeLocationPoint.graphics.beginFill(0xFF0000, 0.5);
                 this.m_shapeLocationPoint.graphics.drawRect(0, 0, 10, 10);
                 this.m_shapeLocationPoint.graphics.endFill();
                 this.addChild( this.m_shapeLocationPoint );
                 */
        _this.touchEnabled = false;
        //  this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapCar, this);
        // this.m_shapeFortap.touchEnabled = true;
        _this.m_shapeFortap.graphics.beginFill(0xFFFFFF, 0.01);
        _this.m_shapeFortap.graphics.drawRect(-50, 180, 250, -450);
        _this.m_shapeFortap.graphics.endFill();
        _this.addChild(_this.m_shapeFortap);
        _this.m_shapeFortap.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onTapCar, _this);
        return _this;
    } // end constructor()
    CAutomobile.prototype.onTapCar = function (e) {
        Main.s_nTapOtherPlaceBeforeTapStage = true;
        if (this.m_JinBi != null) {
            this.m_JinBi.OnClick();
            this.m_shapeFortap.touchEnabled = false;
        }
    };
    CAutomobile.prototype.ClearBoundJinBi = function () {
        this.m_JinBi = null;
        this.SetHasCoin(false);
    };
    CAutomobile.prototype.Reset = function () {
        this.m_CurRoad = null;
        this.m_NextRoad = null;
    };
    CAutomobile.prototype.SetJinBi = function (jinbi) {
        this.m_JinBi = jinbi;
        this.m_shapeFortap.touchEnabled = true;
    };
    CAutomobile.prototype.GetDir = function () {
        return this.m_Dir;
    };
    CAutomobile.prototype.UpdateDir = function (dir) {
        this.m_Dir = dir;
        this.m_bmpMainPic.texture = CAutomobile.GetAutoMobileResByDir(dir, this.GetCarIndex());
    };
    CAutomobile.GetAutoMobileResByDir = function (dir, nCarIndex) {
        switch (dir) {
            case Global.eAutomobileDir.youshang:
                {
                    return RES.getRes("car_" + nCarIndex + "_2" + "_png");
                }
                break;
            case Global.eAutomobileDir.zuoshang:
                {
                    return RES.getRes("car_" + nCarIndex + "_1" + "_png");
                }
                break;
            case Global.eAutomobileDir.zuoxia:
                {
                    return RES.getRes("car_" + nCarIndex + "_0" + "_png");
                }
                break;
            case Global.eAutomobileDir.youxia:
                {
                    return RES.getRes("car_" + nCarIndex + "_3" + "_png");
                }
                break;
        } // end switch
        return null;
    };
    CAutomobile.prototype.SetPos = function (posX, posY) {
        this.x = posX;
        this.y = posY;
    };
    CAutomobile.prototype.GetPosX = function () {
        return this.x;
    };
    CAutomobile.prototype.GetPosY = function () {
        return this.y;
    };
    CAutomobile.prototype.SetRaod = function (road) {
        this.m_CurRoad = road;
        // 预判即将踩到的下一块路面
        var nIndexX = road.GetLocationGrid().GetId()["nIndexX"];
        var nIndexY = road.GetLocationGrid().GetId()["nIndexY"];
        var nIndexX_Next = 0;
        var nIndexY_Next = 0;
        switch (this.m_Dir) {
            case Global.eAutomobileDir.youshang:
                {
                    nIndexX_Next = nIndexX + 1;
                    nIndexY_Next = nIndexY - 1;
                }
                break;
            case Global.eAutomobileDir.youxia:
                {
                    nIndexX_Next = nIndexX + 1;
                    nIndexY_Next = nIndexY + 1;
                }
                break;
            case Global.eAutomobileDir.zuoshang:
                {
                    nIndexX_Next = nIndexX - 1;
                    nIndexY_Next = nIndexY - 1;
                }
                break;
            case Global.eAutomobileDir.zuoxia:
                {
                    nIndexX_Next = nIndexX - 1;
                    nIndexY_Next = nIndexY + 1;
                }
                break;
        } // end switch
        var grid_next = Main.s_CurTown.GetGridByIndex(nIndexX_Next.toString(), nIndexY_Next.toString());
        var road_next = null;
        if (grid_next == undefined || grid_next == null) {
        }
        else {
            road_next = grid_next.GetBoundObj();
            if (road_next == undefined || road_next == null || road_next.GetFuncType() != Global.eObjFunc.road) {
                if (road_next != null && road_next != undefined) {
                }
                road_next = null;
            }
        }
        this.m_NextRoad = road_next; // null表示断头路：下一个地块不是马路，车辆不能通行
    };
    CAutomobile.prototype.GetRaod = function () {
        return this.m_CurRoad;
    };
    CAutomobile.prototype.GetNextRoad = function () {
        return this.m_NextRoad;
    };
    CAutomobile.prototype.GetHeight = function () {
        return this.height;
    };
    CAutomobile.prototype.Turn_Right = function () {
        if (this.m_eStatus != Global.eAutomobileStatus.turn_right) {
            return;
        }
        CAutomobile.GetSpeedByDir(this.m_Dir);
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];
        // check if Turn-left completed
        var nRet = 0;
        var grid = this.m_NextRoad.GetLocationGrid();
        var bCanCompleted = false;
        switch (this.m_Dir) {
            case Global.eAutomobileDir.youshang:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPos()["x"], this.GetPos()["y"], grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_0), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_0), grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_5), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_5));
                    if (nRet == -1) {
                        this.UpdateDir(Global.eAutomobileDir.youxia);
                        bCanCompleted = true;
                    }
                }
                break;
            case Global.eAutomobileDir.youxia:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPos()["x"], this.GetPos()["y"], grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_3), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_3), grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_6), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_6));
                    if (nRet == -1) {
                        this.UpdateDir(Global.eAutomobileDir.zuoxia);
                        bCanCompleted = true;
                    }
                }
                break;
            case Global.eAutomobileDir.zuoshang:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPos()["x"], this.GetPos()["y"], grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_2), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_2), grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_7), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_7));
                    if (nRet == 1) {
                        this.UpdateDir(Global.eAutomobileDir.youshang);
                        bCanCompleted = true;
                    }
                }
                break;
            case Global.eAutomobileDir.zuoxia:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPos()["x"], this.GetPos()["y"], grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_1), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_1), grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_4), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_4));
                    if (nRet == 1) {
                        this.UpdateDir(Global.eAutomobileDir.zuoshang);
                        bCanCompleted = true;
                    }
                }
                break;
        } // end switch
        if (bCanCompleted) {
            this.SetRaod(this.m_NextRoad);
            this.SetStartPosByDir(this.m_Dir, this.m_CurRoad.x, this.m_CurRoad.y);
            this.m_eStatus = Global.eAutomobileStatus.normal;
        }
    }; // end Turn_Left
    CAutomobile.prototype.Turn_Left = function () {
        if (this.m_eStatus != Global.eAutomobileStatus.turn_left) {
            return;
        }
        CAutomobile.GetSpeedByDir(this.m_Dir);
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];
        // check if Turn-left completed
        var nRet = 0;
        var grid = this.m_NextRoad.GetLocationGrid();
        var bCanCompleted = false;
        switch (this.m_Dir) {
            case Global.eAutomobileDir.youshang:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPos()["x"], this.GetPos()["y"], grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_4), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_4), grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_1), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_1));
                    if (nRet == -1) {
                        this.UpdateDir(Global.eAutomobileDir.zuoshang);
                        bCanCompleted = true;
                    }
                }
                break;
            case Global.eAutomobileDir.youxia:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPos()["x"], this.GetPos()["y"], grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_2), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_2), grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_7), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_7));
                    if (nRet == -1) {
                        this.UpdateDir(Global.eAutomobileDir.youshang);
                        bCanCompleted = true;
                    }
                }
                break;
            case Global.eAutomobileDir.zuoshang:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPos()["x"], this.GetPos()["y"], grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_3), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_3), grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_6), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_6));
                    if (nRet == 1) {
                        this.UpdateDir(Global.eAutomobileDir.zuoxia);
                        bCanCompleted = true;
                    }
                }
                break;
            case Global.eAutomobileDir.zuoxia:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPos()["x"], this.GetPos()["y"], grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_0), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_0), grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_5), grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_5));
                    if (nRet == 1) {
                        this.UpdateDir(Global.eAutomobileDir.youxia);
                        bCanCompleted = true;
                    }
                }
                break;
        } // end switch
        if (bCanCompleted) {
            this.SetRaod(this.m_NextRoad);
            this.SetStartPosByDir(this.m_Dir, this.m_CurRoad.x, this.m_CurRoad.y);
            this.SetMoveStatus(Global.eAutomobileStatus.normal);
        }
    }; // end Turn_Left
    CAutomobile.prototype.U_Turn = function () {
        if (this.m_eStatus != Global.eAutomobileStatus.u_turn) {
            return;
        }
        CAutomobile.GetSpeedByDir(this.m_Dir);
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];
        if (this.m_nSubStatus == 0) {
            this.m_nTempMoveAmount += CAutomobile.s_objectSpeedByDir["x"];
            if (Math.abs(this.m_nTempMoveAmount) >= CDef.s_nTileWidth * 0.25) {
                this.m_nSubStatus = 1;
                switch (this.m_Dir) {
                    case Global.eAutomobileDir.youxia:
                        {
                            this.UpdateDir(Global.eAutomobileDir.youshang);
                        }
                        break;
                    case Global.eAutomobileDir.zuoshang:
                        {
                            this.UpdateDir(Global.eAutomobileDir.zuoxia);
                        }
                        break;
                    case Global.eAutomobileDir.zuoxia:
                        {
                            this.UpdateDir(Global.eAutomobileDir.youxia);
                        }
                        break;
                    case Global.eAutomobileDir.youshang:
                        {
                            this.UpdateDir(Global.eAutomobileDir.zuoshang);
                        }
                        break;
                }
                this.SetStartPosByDir(this.m_Dir, this.m_fTempGridPosX, this.m_fTempGridPosY);
            }
        } // end 掉头的第一阶段
        else if (this.m_nSubStatus == 1) {
            var bLeaveTempGrid = CAutomobile.CheckIfLeaveGrid(this.m_Dir, this.m_fTempGridPosY, this.y, this.GetHeight());
            if (bLeaveTempGrid) {
                this.m_eStatus = Global.eAutomobileStatus.normal;
                this.SetRaod(this.m_CurRoad); // 这里必须再重新setroad一次，不然next-road不会刷新 
                //  this.SetStartPosByDir(  this.m_Dir, this.m_CurRoad.x, this.m_CurRoad.y )
            }
        }
    };
    CAutomobile.prototype.SetMoveStatus = function (eStatus) {
        this.m_eStatus = eStatus;
    };
    CAutomobile.prototype.Move = function () {
        if (this.m_eStatus != Global.eAutomobileStatus.normal) {
            return;
        }
        CAutomobile.GetSpeedByDir(this.m_Dir);
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];
        this.UpdateRoadInfo();
    };
    CAutomobile.GetSpeedByDir = function (dir) {
        var speedX = CDef.s_fAutomobileMoveDirX;
        var speedY = CDef.s_fAutomobileMoveDirY;
        switch (dir) {
            case Global.eAutomobileDir.youshang:
                {
                    speedY = -speedY;
                }
                break;
            case Global.eAutomobileDir.youxia:
                {
                }
                break;
            case Global.eAutomobileDir.zuoshang:
                {
                    speedX = -speedX;
                    speedY = -speedY;
                }
                break;
            case Global.eAutomobileDir.zuoxia:
                {
                    speedX = -speedX;
                }
                break;
        } // end switch
        CAutomobile.s_objectSpeedByDir["x"] = speedX;
        CAutomobile.s_objectSpeedByDir["y"] = speedY;
    };
    CAutomobile.prototype.GetLocationPosX = function () {
        return this.x + this.m_shapeLocationPoint.x;
    };
    CAutomobile.prototype.GetLocationPosY = function () {
        return this.y + this.m_shapeLocationPoint.y;
    };
    CAutomobile.prototype.SetStartPosByDir = function (dir, nGridPosX, nGridPosY) {
        if (dir == Global.eAutomobileDir.youshang) {
            this.x = nGridPosX + CDef.s_nTileWidth * 0.125;
            this.y = nGridPosY - CDef.s_nTileHeight * 0.375;
        }
        else if (dir == Global.eAutomobileDir.youxia) {
            this.x = nGridPosX - CDef.s_nTileWidth * 0.125;
            this.y = nGridPosY - CDef.s_nTileHeight * 0.375;
        }
        else if (dir == Global.eAutomobileDir.zuoshang) {
            this.x = nGridPosX + CDef.s_nTileWidth * 0.125;
            this.y = nGridPosY - CDef.s_nTileHeight * 0.625;
        }
        else if (dir == Global.eAutomobileDir.zuoxia) {
            this.x = nGridPosX - CDef.s_nTileWidth * 0.125;
            this.y = nGridPosY - CDef.s_nTileHeight * 0.625;
            ;
        }
        this.y += CDef.s_nTileHeight / 2;
    };
    CAutomobile.prototype.GenerateCurBoundary = function () {
    };
    CAutomobile.prototype.CheckIfLeaveCurGrid = function () {
        var grid = this.GetRaod().GetLocationGrid();
        var nRet = 0;
        switch (this.m_Dir) {
            case Global.eAutomobileDir.youshang:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPosX(), this.GetPosY(), grid.GetCornerPosX(Global.eGridCornerPosType.top), grid.GetCornerPosY(Global.eGridCornerPosType.top), grid.GetCornerPosX(Global.eGridCornerPosType.right), grid.GetCornerPosY(Global.eGridCornerPosType.right));
                    if (nRet == -1) {
                        return true;
                    }
                }
                break;
            case Global.eAutomobileDir.youxia:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPosX(), this.GetPosY(), grid.GetCornerPosX(Global.eGridCornerPosType.bottom), grid.GetCornerPosY(Global.eGridCornerPosType.bottom), grid.GetCornerPosX(Global.eGridCornerPosType.right), grid.GetCornerPosY(Global.eGridCornerPosType.right));
                    if (nRet == -1) {
                        return true;
                    }
                }
                break;
            case Global.eAutomobileDir.zuoshang:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPosX(), this.GetPosY(), grid.GetCornerPosX(Global.eGridCornerPosType.top), grid.GetCornerPosY(Global.eGridCornerPosType.top), grid.GetCornerPosX(Global.eGridCornerPosType.left), grid.GetCornerPosY(Global.eGridCornerPosType.left));
                    if (nRet == 1) {
                        return true;
                    }
                }
                break;
            case Global.eAutomobileDir.zuoxia:
                {
                    nRet = CyberTreeMath.LeftOfLine(this.GetPosX(), this.GetPosY(), grid.GetCornerPosX(Global.eGridCornerPosType.left), grid.GetCornerPosY(Global.eGridCornerPosType.left), grid.GetCornerPosX(Global.eGridCornerPosType.bottom), grid.GetCornerPosY(Global.eGridCornerPosType.bottom));
                    if (nRet == 1) {
                        return true;
                    }
                }
                break;
        }
        return false;
    };
    CAutomobile.CheckIfLeaveGrid = function (dir, nGridPosY, nCarPosY, nCarHeight) {
        /*
        switch( dir )
        {
            case Global.eAutomobileDir.youshang:
            {
               
                if ( nCarPosY < ( nGridPosY - CDef.s_nTileHeight / 2 ) )
                {
                    //console.log( "离开" );
                     return true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
               
                if ( ( nCarPosY - 40 ) < ( nGridPosY - CDef.s_nTileHeight ) )
                {
                    //console.log( "离开" );
                    return  true;
                }
            }
            break;

            case Global.eAutomobileDir.youxia:
            {
               
                if ( ( nCarPosY - nCarHeight + 90 ) >  nGridPosY  )
                {
                    //console.log( "离开youxia" );
                    return  true;
                }
            }
            break;

             case Global.eAutomobileDir.zuoxia:
            {
               
                if ( ( nCarPosY - nCarHeight + 40 ) >  nGridPosY - CDef.s_nTileHeight / 2  )
                {
                   // console.log( "离开zuoxia" );
                   return true;
                }
            }
            break;
        } // end switch
        */
        return false;
    };
    CAutomobile.prototype.UpdateRoadInfo = function () {
        var bLeave = false;
        // 判断离开当前路块没有
        // bLeave = CAutomobile.CheckIfLeaveGrid( this.m_Dir, this.m_CurRoad.y, this.y, this.height);
        bLeave = this.CheckIfLeaveCurGrid();
        if (!bLeave) {
            return;
        }
        if (this.m_NextRoad == null) {
            //  直接掉头
            this.JustUTurn();
            return;
        }
        ////  有路，判断是直行、左转、右转
        //  判断有没有直行的机会
        var nStraightIndexX = 0;
        var nStraightIndexY = 0;
        var nLeftIndexX = 0;
        var nLeftIndexY = 0;
        var nRightIndexX = 0;
        var nRightIndexY = 0;
        var nCurGridX = this.m_CurRoad.GetLocationGrid().GetId()["nIndexX"];
        var nCurGridY = this.m_CurRoad.GetLocationGrid().GetId()["nIndexY"];
        var grid_straight = null;
        var road_straight = null;
        var grid_left = null;
        var road_left = null;
        var grid_right = null;
        var road_right = null;
        switch (this.m_Dir) {
            case Global.eAutomobileDir.youxia:
                {
                    nStraightIndexX = nCurGridX + 2;
                    nStraightIndexY = nCurGridY + 2;
                    nLeftIndexX = nCurGridX + 2;
                    nLeftIndexY = nCurGridY;
                    nRightIndexX = nCurGridX;
                    nRightIndexY = nCurGridY + 2;
                }
                break;
            case Global.eAutomobileDir.zuoxia:
                {
                    nStraightIndexX = nCurGridX - 2;
                    nStraightIndexY = nCurGridY + 2;
                    nLeftIndexX = nCurGridX;
                    nLeftIndexY = nCurGridY + 2;
                    nRightIndexX = nCurGridX - 2;
                    nRightIndexY = nCurGridY;
                }
                break;
            case Global.eAutomobileDir.youshang:
                {
                    nStraightIndexX = nCurGridX + 2;
                    nStraightIndexY = nCurGridY - 2;
                    nLeftIndexX = nCurGridX;
                    nLeftIndexY = nCurGridY - 2;
                    nRightIndexX = nCurGridX + 2;
                    nRightIndexY = nCurGridY;
                }
                break;
            case Global.eAutomobileDir.zuoshang:
                {
                    nStraightIndexX = nCurGridX - 2;
                    nStraightIndexY = nCurGridY - 2;
                    nLeftIndexX = nCurGridX - 2;
                    nLeftIndexY = nCurGridY;
                    nRightIndexX = nCurGridX;
                    nRightIndexY = nCurGridY - 2;
                }
                break;
        } // end switch
        grid_straight = Main.s_CurTown.GetGridByIndex(nStraightIndexX.toString(), nStraightIndexY.toString());
        grid_left = Main.s_CurTown.GetGridByIndex(nLeftIndexX.toString(), nLeftIndexY.toString());
        grid_right = Main.s_CurTown.GetGridByIndex(nRightIndexX.toString(), nRightIndexY.toString());
        CAutomobile.s_aryTempChoice.length = 0;
        if (grid_straight == null || grid_straight == undefined) {
        }
        else {
            road_straight = grid_straight.GetBoundObj();
            if (road_straight != null && road_straight.GetFuncType() == Global.eObjFunc.road) {
                CAutomobile.s_aryTempChoice.push(0);
            }
            else {
                road_straight = null;
            }
        }
        if (grid_left == null || grid_left == undefined) {
        }
        else {
            road_left = grid_left.GetBoundObj();
            if (road_left != null && road_left.GetFuncType() == Global.eObjFunc.road) {
                CAutomobile.s_aryTempChoice.push(1);
            }
            else {
                road_left = null;
            }
        }
        if (grid_right == null || grid_right == undefined) {
        }
        else {
            road_right = grid_right.GetBoundObj();
            if (road_right != null && road_right.GetFuncType() == Global.eObjFunc.road) {
                CAutomobile.s_aryTempChoice.push(2);
            }
            else {
                road_right = null;
            }
        }
        //  console.log( CAutomobile.s_aryTempChoice.length );
        if (CAutomobile.s_aryTempChoice.length == 0) {
            this.JustUTurn();
            return;
        }
        var nChoice = 0;
        if (CAutomobile.s_aryTempChoice.length == 1) {
            nChoice = CAutomobile.s_aryTempChoice[0];
        }
        else if (CAutomobile.s_aryTempChoice.length == 2) {
            var nRan = Math.random();
            if (nRan < 0.5) {
                nChoice = CAutomobile.s_aryTempChoice[0];
            }
            else {
                nChoice = CAutomobile.s_aryTempChoice[1];
            }
        }
        else if (CAutomobile.s_aryTempChoice.length == 3) {
            var nRan = Math.random();
            if (nRan < 0.33) {
                nChoice = CAutomobile.s_aryTempChoice[0];
            }
            else if (nRan < 0.66) {
                nChoice = CAutomobile.s_aryTempChoice[1];
            }
            else {
                nChoice = CAutomobile.s_aryTempChoice[2];
            }
        }
        if (nChoice == 0) {
            this.SetRaod(this.m_NextRoad); // 注意不是road_straight 
            // 继续维持直行状态。即便状态并没改变，也必须重新执行SetMoveStatus（）, 因为当前准备进入下一个地块了，牵涉的路面数据要更新
            this.SetMoveStatus(Global.eAutomobileStatus.normal);
        }
        else if (nChoice == 1) {
            //this.JustTurnLeft();
            // this.SetRaod( this.m_NextRoad ); 
            this.SetMoveStatus(Global.eAutomobileStatus.turn_left);
        }
        else {
            // this.SetRaod( this.m_NextRoad ); 
            this.SetMoveStatus(Global.eAutomobileStatus.turn_right);
        } // end 右转
    }; // end UpdateRoadInfo()
    CAutomobile.prototype.JustTurnLeft = function () {
        this.m_eStatus = Global.eAutomobileStatus.turn_left;
        this.m_nSubStatus = 0;
        this.m_nTempMoveAmount = 0;
    };
    CAutomobile.prototype.JustUTurn = function () {
        switch (this.m_Dir) {
            case Global.eAutomobileDir.zuoxia:
                {
                    this.UpdateDir(Global.eAutomobileDir.youxia);
                    this.m_fTempGridPosX = this.m_CurRoad.x - CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y + CDef.s_nTileHeight * 0.5;
                }
                break;
            case Global.eAutomobileDir.youshang:
                {
                    this.UpdateDir(Global.eAutomobileDir.zuoshang);
                    this.m_fTempGridPosX = this.m_CurRoad.x + CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y - CDef.s_nTileHeight * 0.5;
                }
                break;
            case Global.eAutomobileDir.zuoshang:
                {
                    this.UpdateDir(Global.eAutomobileDir.zuoxia);
                    this.m_fTempGridPosX = this.m_CurRoad.x - CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y - CDef.s_nTileHeight * 0.5;
                }
                break;
            case Global.eAutomobileDir.youxia:
                {
                    this.UpdateDir(Global.eAutomobileDir.youshang);
                    this.m_fTempGridPosX = this.m_CurRoad.x + CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y + CDef.s_nTileHeight * 0.5;
                }
                break;
        } // end switch
        this.m_eStatus = Global.eAutomobileStatus.u_turn;
        this.m_nSubStatus = 0;
        this.m_nTempMoveAmount = 0;
    };
    CAutomobile.prototype.ReInsertSort = function () {
        this.m_fReInsertSortTimeElapse += CDef.s_fFixedDeltaTime;
        if (this.m_fReInsertSortTimeElapse < CDef.s_fCarReInsertSortInterval) {
            return;
        }
        this.m_fReInsertSortTimeElapse = 0;
        this.SetSortPosY();
        Main.s_CurTown.ReInsertSortObj(this);
    };
    CAutomobile.prototype.SetHasCoin = function (bHas) {
        this.m_bHasCoinNow = bHas;
    };
    CAutomobile.prototype.GetHasCoin = function () {
        return this.m_bHasCoinNow;
    };
    CAutomobile.prototype.GenerateCoinLoop = function () {
        if (this.GetHasCoin()) {
            return;
        }
        // Wiki中缺少这部分的数值配置，只有自己凭感觉弄
        var bGenCoin = false;
        this.m_nGenCoinTimeElapse += CDef.s_fFixedDeltaTime;
        if (this.m_nGenCoinTimeElapse < 3) {
            return;
        }
        this.m_nGenCoinTimeElapse = 0;
        var nTemp = Math.random();
        if (nTemp < 0.85) {
            var jinbi = CJinBiManager.NewJinBi();
            jinbi.name = "jinbi";
            this.addChild(jinbi);
            jinbi.x = this.width / 2;
            jinbi.y = -50;
            jinbi.SetBoundObj(this);
            this.SetJinBi(jinbi);
            jinbi.Update();
            jinbi.visible = true;
            this.SetHasCoin(true);
            var nSize = Math.random();
            var eSubSize;
            if (nSize < 0.8) {
                eSubSize = Global.eMoneySubType.small_coin;
                jinbi.scaleX = 1.5;
                jinbi.scaleY = 1.5;
            }
            else {
                jinbi.scaleX = 3;
                jinbi.scaleY = 3;
                eSubSize = Global.eMoneySubType.big_coin;
            }
            var nValue = CConfigManager.GetCarJinBiValueRate(eSubSize) * Main.s_CurTown.GetCPS();
            jinbi.SetSubType(eSubSize);
            jinbi.SetValue(Global.eMoneyType.coin, nValue);
        }
        else {
            eSubSize = Global.eMoneySubType.small_diamond;
            var jinbi = CJinBiManager.NewJinBi();
            jinbi.name = "lvpiao";
            this.addChild(jinbi);
            jinbi.x = this.width / 2;
            jinbi.y = -50;
            jinbi.SetBoundObj(this);
            this.SetJinBi(jinbi);
            jinbi.Update();
            jinbi.visible = true;
            this.SetHasCoin(true);
            jinbi.scaleX = 1.5;
            jinbi.scaleY = 1.5;
            var nSize = Math.random();
            var eSubSize;
            if (nSize < 0.8) {
                eSubSize = Global.eMoneySubType.small_diamond;
                jinbi.scaleX = 1.5;
                jinbi.scaleY = 1.5;
            }
            else {
                jinbi.scaleX = 3;
                jinbi.scaleY = 3;
                eSubSize = Global.eMoneySubType.big_diamond;
            }
            var nValue = CConfigManager.GetCarJinBiValueRate(eSubSize);
            jinbi.SetValue(Global.eMoneyType.diamond, nValue);
            jinbi.SetSubType(eSubSize);
        }
    }; // end       GenerateCoinLoop     
    CAutomobile.prototype.JinBiLoop = function () {
        if (this.m_JinBi == null) {
            return;
        }
        this.m_JinBi.Update();
    };
    CAutomobile.prototype.SetCarIndex = function (nIndex) {
        this.m_nCarIndex = nIndex;
    };
    CAutomobile.prototype.GetCarIndex = function () {
        return this.m_nCarIndex;
    };
    CAutomobile.s_objectSpeedByDir = new Object();
    CAutomobile.s_aryTempChoice = new Array();
    return CAutomobile;
}(CObj)); // end class
__reflect(CAutomobile.prototype, "CAutomobile");
//# sourceMappingURL=CAutomobile.js.map