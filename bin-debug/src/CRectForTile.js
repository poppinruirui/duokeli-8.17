var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CRectForTile = (function (_super) {
    __extends(CRectForTile, _super);
    function CRectForTile() {
        var _this = _super.call(this) || this;
        _this.m_aryRelatedGrids = new Array();
        for (var i = 0; i < 5; i++) {
            _this.m_aryRelatedGrids.push(null);
        }
        return _this;
    } // end constructor
    CRectForTile.prototype.SetRelatedGrid = function (idx, grid) {
        this.m_aryRelatedGrids[idx] = grid;
    };
    return CRectForTile;
}(egret.DisplayObjectContainer)); // end class
__reflect(CRectForTile.prototype, "CRectForTile");
//# sourceMappingURL=CRectForTile.js.map