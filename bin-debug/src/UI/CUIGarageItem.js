var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
“车库”界面中，列表中的一条记录
*/
var CUIGarageItem = (function (_super) {
    __extends(CUIGarageItem, _super);
    function CUIGarageItem() {
        var _this = _super.call(this) || this;
        _this.m_nIndex = 0;
        _this.m_eStatus = Global.eGarageItemStatus.locked_and_can_unlock;
        _this.m_bmpBg = new eui.Image(RES.getRes("1X1_png"));
        _this.m_bmpBg.touchEnabled = true;
        _this.addChild(_this.m_bmpBg);
        _this.m_bmpBg.width = CDef.s_fGarageItemWitdh;
        _this.m_bmpBg.height = CDef.s_fGarageItemHeight;
        _this.m_bmpBg.filters = [CColorFucker.GetColorMatrixFilterByRGBA(0.262, 0.262, 0.262, 1)];
        _this.m_bmpCarAvatar = new eui.Image();
        _this.m_bmpCarAvatar.x = 5;
        _this.m_bmpCarAvatar.y = 10;
        _this.addChild(_this.m_bmpCarAvatar);
        _this.m_txtCarName = new eui.Label();
        _this.m_txtCarName.x = 75;
        _this.m_txtCarName.y = 15;
        _this.m_txtCarName.size = 24;
        _this.m_txtCarName.bold = true;
        _this.addChild(_this.m_txtCarName);
        _this.m_txtCarDesc = new eui.Label();
        _this.m_txtCarDesc.x = 80;
        _this.m_txtCarDesc.y = 50;
        _this.m_txtCarDesc.size = 20;
        _this.m_txtCarDesc.textColor = 0xCBCBCB;
        _this.addChild(_this.m_txtCarDesc);
        _this.m_btnUnlock = new CUIBaseCommonButton2();
        _this.addChild(_this.m_btnUnlock);
        _this.m_btnUnlock.x = 385;
        _this.m_btnUnlock.y = 10;
        _this.m_btnUnlock.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_UnlockThisCar, _this);
        _this.SetUnlockButtonEnabled(false);
        _this.m_bmpGrayMask = new eui.Image(RES.getRes("1X1_png"));
        _this.addChild(_this.m_bmpGrayMask);
        _this.m_bmpGrayMask.width = CDef.s_fGarageItemWitdh;
        _this.m_bmpGrayMask.height = CDef.s_fGarageItemHeight;
        _this.m_bmpGrayMask.filters = [CColorFucker.GetColorMatrixFilterByRGBA(0.412, 0.412, 0.412, 0.7)];
        _this.m_bmpGrayMask.alpha = 0.7;
        return _this;
    } // end constructor
    CUIGarageItem.prototype.SetIndex = function (nIndex) {
        this.m_nIndex = nIndex;
        this.m_bmpCarAvatar.texture = RES.getRes("car_" + nIndex + "_0_png");
        this.m_bmpCarAvatar.scaleX = 0.32;
        this.m_bmpCarAvatar.scaleY = 0.32;
        var car_config = CConfigManager.GetCarConfig(nIndex);
        if (car_config != null) {
            this.m_txtCarName.text = car_config.szName;
            this.m_txtCarDesc.text = car_config.szDesc;
        }
    };
    CUIGarageItem.prototype.GetIndex = function () {
        return this.m_nIndex;
    };
    CUIGarageItem.prototype.SetLock = function (bLock) {
        this.m_btnUnlock.touchEnabled = !bLock;
    };
    CUIGarageItem.prototype.onButtonClick_UnlockThisCar = function (evt) {
        Main.s_CurTown.AddAutomobile(this.GetIndex());
        this.m_btnUnlock.visible = false;
        Main.s_CurTown.SetGarageItemStatus(this.GetIndex(), Global.eGarageItemStatus.unlocked);
        Main.s_CurTown.CostCoinDueToUnlockCar(this.GetIndex());
        Main.s_CurTown.UpdateGarageStatus();
        CUIManager.s_uiGarage.SetMaskVisible(this.GetIndex() + 1, false);
    };
    CUIGarageItem.prototype.SetMaskVisible = function (bVisible) {
        this.m_bmpGrayMask.visible = bVisible;
    };
    CUIGarageItem.prototype.SetUnlockButtonEnabled = function (bEnabled) {
        if (bEnabled) {
            this.m_btnUnlock.filters = null;
            if (!this.m_btnUnlock.hasEventListener(egret.TouchEvent.TOUCH_TAP)) {
                this.m_btnUnlock.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_UnlockThisCar, this);
            }
        }
        else {
            if (this.m_btnUnlock.hasEventListener(egret.TouchEvent.TOUCH_TAP)) {
                this.m_btnUnlock.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_UnlockThisCar, this);
            }
            this.m_btnUnlock.filters = [CColorFucker.GetDisabledGray()];
        }
    };
    CUIGarageItem.prototype.SetStatus = function (eStatus) {
        this.m_eStatus = eStatus;
        switch (this.m_eStatus) {
            case Global.eGarageItemStatus.can_not_unlock:
                {
                    this.m_bmpGrayMask.visible = true;
                    this.SetUnlockButtonEnabled(false);
                    this.m_btnUnlock.visible = true;
                }
                break;
            case Global.eGarageItemStatus.locked_and_can_unlock:
                {
                    this.m_bmpGrayMask.visible = false;
                    this.m_btnUnlock.visible = true;
                    this.SetUnlockButtonEnabled(true);
                }
                break;
            case Global.eGarageItemStatus.unlocked:
                {
                    this.m_bmpGrayMask.visible = false;
                    this.m_btnUnlock.visible = false;
                }
                break;
        }
        // end switch
        this.m_btnUnlock.SetCost(CConfigManager.GetCoinForUnlockCar(this.GetIndex()));
    };
    return CUIGarageItem;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIGarageItem.prototype, "CUIGarageItem");
//# sourceMappingURL=CUIGarageItem.js.map