var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUISpecailPanel = (function (_super) {
    __extends(CUISpecailPanel, _super);
    function CUISpecailPanel() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_nCurPageIndex = 0;
        _this.m_dicBuildingData = new Object();
        _this.m_dicBuildingIndex = new Object();
        _this.m_CurBuildingData = null;
        _this.m_dicPageContainer = new Object();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CSpecailPanel.exml";
        _this.addChild(_this.m_uiContainer);
        var imgTemp = _this.m_uiContainer.getChildByName("img2");
        _this.m_btnSwitchPage0 = new CCosmosImage();
        _this.m_btnSwitchPage0.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_uiContainer.addChild(_this.m_btnSwitchPage0);
        _this.m_btnSwitchPage0.x = imgTemp.x;
        _this.m_btnSwitchPage0.y = imgTemp.y;
        _this.m_btnSwitchPage0.scaleX = imgTemp.scaleX;
        _this.m_btnSwitchPage0.scaleY = imgTemp.scaleY;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnSwitchPage0.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Page0, _this);
        _this.m_btnSwitchPage0.SetTextColor(0, 0xFFFFFF);
        _this.m_dicPageContainer[0] = _this.m_btnSwitchPage0;
        var imgTemp = _this.m_uiContainer.getChildByName("img3");
        _this.m_btnSwitchPage1 = new CCosmosImage();
        _this.m_btnSwitchPage1.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_uiContainer.addChild(_this.m_btnSwitchPage1);
        _this.m_btnSwitchPage1.x = imgTemp.x;
        _this.m_btnSwitchPage1.y = imgTemp.y;
        _this.m_btnSwitchPage1.scaleX = imgTemp.scaleX;
        _this.m_btnSwitchPage1.scaleY = imgTemp.scaleY;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnSwitchPage1.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Page1, _this);
        _this.m_btnSwitchPage1.SetTextColor(0, 0xFFFFFF);
        _this.m_dicPageContainer[1] = _this.m_btnSwitchPage1;
        var imgTemp = _this.m_uiContainer.getChildByName("img4");
        _this.m_btnSwitchPage2 = new CCosmosImage();
        _this.m_btnSwitchPage2.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_uiContainer.addChild(_this.m_btnSwitchPage2);
        _this.m_btnSwitchPage2.x = imgTemp.x;
        _this.m_btnSwitchPage2.y = imgTemp.y;
        _this.m_btnSwitchPage2.scaleX = imgTemp.scaleX;
        _this.m_btnSwitchPage2.scaleY = imgTemp.scaleY;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnSwitchPage2.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Page2, _this);
        _this.m_btnSwitchPage2.SetTextColor(0, 0xFFFFFF);
        _this.m_dicPageContainer[2] = _this.m_btnSwitchPage2;
        imgTemp = _this.m_uiContainer.getChildByName("btnClose");
        _this.m_btnClose = new CCosmosImage();
        _this.m_btnClose.SetExml("resource/assets/MyExml/CCosmosButton1.exml");
        _this.m_uiContainer.addChild(_this.m_btnClose);
        _this.m_btnClose.x = imgTemp.x;
        _this.m_btnClose.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnClose.UseColorSolution(0);
        _this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        imgTemp = _this.m_uiContainer.getChildByName("img5");
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(118, 238, 0)];
        imgTemp = _this.m_uiContainer.getChildByName("img6");
        _this.m_btnPrev = new CCosmosImage();
        _this.m_btnPrev.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_btnPrev.x = imgTemp.x;
        _this.m_btnPrev.y = imgTemp.y;
        _this.m_uiContainer.addChild(_this.m_btnPrev);
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnPrev.SetLabelContent(0, "");
        _this.m_btnPrev.SetImageTexture(0, RES.getRes("arrow1_png"));
        _this.m_btnPrev.width = 100;
        _this.m_btnPrev.height = 100;
        _this.m_btnPrev.SetImageSize(0, 100, 100);
        _this.m_btnPrev.SetLabelVisible(0, false);
        _this.m_btnPrev.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Prev, _this);
        imgTemp = _this.m_uiContainer.getChildByName("img8");
        _this.m_btnNext = new CCosmosImage();
        _this.m_btnNext.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_btnNext.x = imgTemp.x;
        _this.m_btnNext.y = imgTemp.y;
        _this.m_uiContainer.addChild(_this.m_btnNext);
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnNext.SetLabelContent(0, "");
        _this.m_btnNext.SetLabelVisible(0, false);
        _this.m_btnNext.SetImageTexture(0, RES.getRes("arrow1_png"));
        _this.m_btnNext.width = 100;
        _this.m_btnNext.height = 100;
        _this.m_btnNext.SetImageSize(0, 100, 100);
        _this.m_btnNext.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Next, _this);
        _this.m_txtBuildingName = _this.m_uiContainer.getChildByName("txt0");
        _this.m_txtBuildingNumber = _this.m_uiContainer.getChildByName("txt1");
        _this.m_txtBuildingPropertyType = _this.m_uiContainer.getChildByName("txt2");
        _this.m_txtBuildingBoostRate = _this.m_uiContainer.getChildByName("txt3");
        _this.m_txtCurProcessingLotType = _this.m_uiContainer.getChildByName("txt4");
        _this.m_imgAvatar = _this.m_uiContainer.getChildByName("img1");
        imgTemp = _this.m_uiContainer.getChildByName("img9");
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(238, 233, 233)];
        imgTemp = _this.m_uiContainer.getChildByName("img7");
        _this.m_btnUnlock = new CCosmosImage();
        _this.m_btnUnlock.SetExml("resource/assets/MyExml/CCosmosButton6.exml");
        _this.m_btnUnlock.x = imgTemp.x;
        _this.m_btnUnlock.y = imgTemp.y;
        _this.m_uiContainer.addChild(_this.m_btnUnlock);
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnUnlock.SetLabelContent(0, "aaaaaa");
        _this.m_btnUnlock.scaleX = 1.5;
        _this.m_btnUnlock.scaleY = 1.5;
        _this.m_btnUnlock.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_UnlockOrUse, _this);
        _this.m_btnUnlock.SetTextColor(0, 0xFFFFFF);
        _this.m_btnUnlock.SetTextColor(1, 0x000000);
        _this.m_btnUnlock.SetImageColor_New(1, 190, 190, 190);
        _this.m_btnUnlock.SetImageColor_New(2, 148, 0, 211);
        _this.m_btnUnlock.SetImageColor_New(3, 186, 85, 211);
        _this.m_btnUnlock.SetImageTexture(4, RES.getRes("zuanshi_png"));
        _this.m_dicBuildingIndex[0] = 0;
        _this.m_dicBuildingIndex[1] = 0;
        _this.m_dicBuildingIndex[2] = 0;
        return _this;
    } // end constructor
    CUISpecailPanel.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.special_panel, false);
    };
    CUISpecailPanel.prototype.onButtonClick_Next = function (evt) {
        var nCurBuildingIndex = this.GetCurBuildingIndex();
        nCurBuildingIndex++;
        if (nCurBuildingIndex > this.GetMaxBuildingIndex()) {
            nCurBuildingIndex = 0;
        }
        this.SetCurBuildingIndex(nCurBuildingIndex);
        this.SelectBuilding();
    };
    CUISpecailPanel.prototype.onButtonClick_Prev = function (evt) {
        var nCurBuildingIndex = this.GetCurBuildingIndex();
        nCurBuildingIndex--;
        if (nCurBuildingIndex < 0) {
            nCurBuildingIndex = this.GetMaxBuildingIndex();
        }
        this.SetCurBuildingIndex(nCurBuildingIndex);
        this.SelectBuilding();
    };
    CUISpecailPanel.prototype.SwitchPage = function (nPageIndex) {
        this.m_nCurPageIndex = nPageIndex;
        for (var i = 0; i < 3; i++) {
            var btnPage = this.m_dicPageContainer[i];
            if (i == nPageIndex) {
                btnPage.SetImageColor_New(0, 25, 25, 25);
            }
            else {
                btnPage.SetImageColor_New(0, 190, 190, 190);
            }
        }
        if (this.m_nCurPageIndex == 0 || this.m_nCurPageIndex == 1) {
            this.m_btnUnlock.SetText(0, "使用");
            this.m_btnUnlock.SetImageVisible(4, false);
            this.m_btnUnlock.SetText(1, "");
        }
        else {
            this.m_btnUnlock.SetText(0, "解锁");
            this.m_btnUnlock.SetImageVisible(4, true);
            this.m_btnUnlock.SetText(1, "");
        }
        this.SelectBuilding();
    };
    CUISpecailPanel.prototype.SelectBuilding = function () {
        // 先把上一轮显示结果清空
        this.m_txtBuildingName.text = "";
        this.m_txtBuildingNumber.text = "";
        this.m_txtBuildingPropertyType.text = "";
        this.m_txtBuildingBoostRate.text = "";
        this.m_imgAvatar.texture = null;
        this.m_CurBuildingData = null;
        var aryBuildingsData = this.GetBuildingsDataByPageIndex(this.m_nCurPageIndex);
        if (aryBuildingsData.length == 0) {
            this.m_btnUnlock.visible = false;
            return;
        }
        this.m_btnUnlock.visible = true;
        this.m_CurBuildingData = aryBuildingsData[this.GetCurBuildingIndex()];
        this.m_txtBuildingName.text = this.m_CurBuildingData.szName;
        this.m_imgAvatar.texture = RES.getRes(this.m_CurBuildingData.szResName);
        this.m_txtBuildingNumber.text = (this.GetCurBuildingIndex() + 1) + " / " + (this.GetMaxBuildingIndex() + 1);
        this.m_txtBuildingPropertyType.text = CBuildingManager.PropertyType2String(this.m_CurBuildingData.ePropertyType);
        this.m_txtBuildingBoostRate.text = (this.m_CurBuildingData.fGainRate * 100) + "%";
        if (this.m_nCurPageIndex == 0 || this.m_nCurPageIndex == 1) {
            var bCanUse = true;
            if (Main.s_CurTown.GetCurEditProcessingLot().GetLotProperty() != this.m_CurBuildingData.ePropertyType) {
                this.m_btnUnlock.SetText(1, "属性不符");
                bCanUse = false;
            }
            if (this.m_CurBuildingData.bSpecial) {
                if (this.m_CurBuildingData.bUsed) {
                    this.m_btnUnlock.SetText(1, "已使用");
                    bCanUse = false;
                }
            }
            if (bCanUse) {
                this.m_btnUnlock.SetText(1, "");
            }
            this.m_btnUnlock.SetEnabled(bCanUse);
        }
        else {
            this.m_btnUnlock.SetText(1, this.m_CurBuildingData.nPrice.toString());
            if (CPlayer.GetDiamond() >= this.m_CurBuildingData.nPrice) {
                this.m_btnUnlock.SetEnabled(true);
            }
            else {
                this.m_btnUnlock.SetEnabled(false);
            }
        }
    };
    CUISpecailPanel.prototype.GetMaxBuildingIndex = function () {
        return this.m_dicBuildingData[this.m_nCurPageIndex].length - 1;
    };
    CUISpecailPanel.prototype.GetCurBuildingIndex = function () {
        return this.m_dicBuildingIndex[this.m_nCurPageIndex];
    };
    CUISpecailPanel.prototype.SetCurBuildingIndex = function (nCurBuildingIndex) {
        this.m_dicBuildingIndex[this.m_nCurPageIndex] = nCurBuildingIndex;
    };
    CUISpecailPanel.prototype.onButtonClick_Page0 = function (evt) {
        this.SwitchPage(0);
    };
    CUISpecailPanel.prototype.onButtonClick_Page1 = function (evt) {
        this.SwitchPage(1);
    };
    CUISpecailPanel.prototype.onButtonClick_Page2 = function (evt) {
        this.SwitchPage(2);
    };
    CUISpecailPanel.prototype.GetBuildingsDataByPageIndex = function (nPageIndex) {
        return this.m_dicBuildingData[nPageIndex];
    };
    CUISpecailPanel.prototype.UpdateInfo = function () {
        CBuildingManager.LoadConfig();
        this.m_dicBuildingData[0] = CBuildingManager.GetCommonBuildings();
        this.m_dicBuildingData[1] = CBuildingManager.GetUnLockSpecialBuildings();
        this.m_dicBuildingData[2] = CBuildingManager.GetLockedSpecialBuildings();
        this.m_btnSwitchPage0.SetText(0, "普通建筑(" + this.GetBuildingsDataByPageIndex(0).length.toString() + ")");
        this.m_btnSwitchPage1.SetText(0, "已解锁(" + this.GetBuildingsDataByPageIndex(1).length.toString() + ")");
        this.m_btnSwitchPage2.SetText(0, "未解锁(" + this.GetBuildingsDataByPageIndex(2).length.toString() + ")");
        this.m_txtCurProcessingLotType.text = "当前选中的地块属性：" + CBuildingManager.PropertyType2String(Main.s_CurTown.GetCurEditProcessingLot().GetLotProperty());
        this.SwitchPage(0);
    };
    ////  解锁或使用
    CUISpecailPanel.prototype.onButtonClick_UnlockOrUse = function (evt) {
        if (this.m_nCurPageIndex == 0 || this.m_nCurPageIndex == 1) {
            CBuildingManager.UseBuilding(this.m_CurBuildingData);
            if (this.m_CurBuildingData.bSpecial) {
                this.m_CurBuildingData.bUsed = true;
                this.SelectBuilding();
            }
        }
        else {
            CBuildingManager.UnlockBuilding(this.m_CurBuildingData);
        }
    };
    CUISpecailPanel.prototype.SelectLastBulding = function () {
        this.SetCurBuildingIndex(this.GetMaxBuildingIndex());
    };
    return CUISpecailPanel;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUISpecailPanel.prototype, "CUISpecailPanel");
//# sourceMappingURL=CUISpecialPanel.js.map