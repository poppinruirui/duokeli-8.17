var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
    C位按钮
*/
var CUICPosBUtton = (function (_super) {
    __extends(CUICPosBUtton, _super);
    function CUICPosBUtton() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/BottomButtons.exml";
        _this.addChild(_this.m_uiContainer);
        var imgTemp = _this.m_uiContainer.getChildByName("btnC");
        _this.m_imgCButton = new CCosmosImage();
        _this.m_imgCButton.SetExml("resource/assets/MyExml/CPosButton.exml");
        _this.m_uiContainer.addChild(_this.m_imgCButton);
        _this.m_imgCButton.x = imgTemp.x;
        _this.m_imgCButton.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了
        _this.m_imgCButton.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onTapCPosButton, _this);
        imgTemp = _this.m_uiContainer.getChildByName("imgCityHall");
        _this.m_imgCityHall = new CCosmosImage();
        _this.m_imgCityHall.SetExml("resource/assets/MyExml/CButtonCityHall.exml");
        _this.m_uiContainer.addChild(_this.m_imgCityHall);
        _this.m_imgCityHall.x = imgTemp.x;
        _this.m_imgCityHall.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了
        _this.m_imgCityHall.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onTapCityHall, _this);
        imgTemp = _this.m_uiContainer.getChildByName("imgTimeBoost");
        _this.m_imgTimeBoost = new CCosmosImage();
        _this.m_imgTimeBoost.SetExml("resource/assets/MyExml/CButtomTimeBooste.exml");
        _this.m_uiContainer.addChild(_this.m_imgTimeBoost);
        _this.m_imgTimeBoost.x = imgTemp.x;
        _this.m_imgTimeBoost.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了
        _this.m_imgTimeBoost.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onTapBoost, _this);
        imgTemp = _this.m_uiContainer.getChildByName("imgBig");
        _this.m_imgPig = new CCosmosImage();
        _this.m_imgPig.SetExml("resource/assets/MyExml/CButtonPig.exml");
        _this.m_uiContainer.addChild(_this.m_imgPig);
        _this.m_imgPig.x = imgTemp.x;
        _this.m_imgPig.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了
        imgTemp = _this.m_uiContainer.getChildByName("imgSettings");
        _this.m_imgSettings = new CCosmosImage();
        _this.m_imgSettings.SetExml("resource/assets/MyExml/CButtonSettings.exml");
        _this.m_uiContainer.addChild(_this.m_imgSettings);
        _this.m_imgSettings.x = imgTemp.x;
        _this.m_imgSettings.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了
        _this.m_imgSettings.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onTapSettings, _this);
        _this.m_imgDoubleTimeLeftBg = _this.m_uiContainer.getChildByName("imgDoubleTimeLeftBg");
        _this.m_imgDoubleTimeLeftBg.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(67, 151, 66)];
        _this.m_LabelDoubleTimeLeft = _this.m_uiContainer.getChildByName("txtDoubleTimeLeft");
        _this.m_uiContainer.addChild(_this.m_imgDoubleTimeLeftBg);
        _this.m_uiContainer.addChild(_this.m_LabelDoubleTimeLeft);
        _this.SetDoubleTimeLeftVisible(false);
        imgTemp = _this.m_uiContainer.getChildByName("imgPigGreenNumBg");
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(46, 139, 87)];
        _this.m_uiContainer.addChild(imgTemp);
        imgTemp.touchEnabled = false;
        _this.m_labelPigGreenNum = _this.m_uiContainer.getChildByName("txtPigGreenNum");
        _this.m_uiContainer.addChild(_this.m_labelPigGreenNum);
        _this.m_labelPigGreenNum.touchEnabled = false;
        imgTemp = _this.m_uiContainer.getChildByName("NumBubble");
        _this.m_nbCanUpgradeNum = new CUIBaseNumBubble();
        _this.m_uiContainer.addChild(_this.m_nbCanUpgradeNum);
        _this.m_nbCanUpgradeNum.x = imgTemp.x;
        _this.m_nbCanUpgradeNum.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_nbCanUpgradeNum.scaleX = 0.6;
        _this.m_nbCanUpgradeNum.scaleY = 0.6;
        return _this;
    } // end constructor
    CUICPosBUtton.prototype.GetPigButton = function () {
        return this.m_imgPig;
    };
    CUICPosBUtton.prototype.onTapSettings = function (evt) {
        CUIManager.s_uiDeveloperEditor.visible = !CUIManager.s_uiDeveloperEditor.visible;
    };
    CUICPosBUtton.prototype.onTapBoost = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.boost_panel, true);
    };
    CUICPosBUtton.prototype.onTapCityHall = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.city_hall, true);
        CUIManager.s_uiCityHall.UpdateStatus();
    };
    CUICPosBUtton.prototype.onTapCPosButton = function (evt) {
        Main.s_CurTown.DoUpgrade();
    };
    CUICPosBUtton.prototype.SetDoubleTimeLeftVisible = function (bVisible) {
        this.m_imgDoubleTimeLeftBg.visible = bVisible;
        this.m_LabelDoubleTimeLeft.visible = bVisible;
    };
    CUICPosBUtton.prototype.SetDoubleTimeLeft = function (szTimeLeftMin) {
        this.m_LabelDoubleTimeLeft.text = szTimeLeftMin + "分钟";
    };
    CUICPosBUtton.prototype.SetPigGreenNum = function (num) {
        this.m_labelPigGreenNum.text = num.toString();
    };
    CUICPosBUtton.prototype.SetCurCanUpgradeNum = function (nCurCanNum) {
        this.m_nbCanUpgradeNum.SetNum(nCurCanNum);
        if (nCurCanNum > 0) {
            this.m_nbCanUpgradeNum.visible = true;
            this.m_imgCButton.SetEnabled(true);
        }
        else {
            this.m_nbCanUpgradeNum.visible = false;
            this.m_imgCButton.SetEnabled(false);
        }
    };
    return CUICPosBUtton;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUICPosBUtton.prototype, "CUICPosBUtton");
//# sourceMappingURL=CUICPosButon.js.map