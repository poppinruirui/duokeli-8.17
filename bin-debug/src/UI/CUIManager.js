var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIManager = (function (_super) {
    __extends(CUIManager, _super);
    function CUIManager() {
        return _super.call(this) || this;
    } // end constructor
    CUIManager.Init = function () {
        CUIManager.s_uiGarage = new CUIGarage();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiGarage);
        CUIManager.s_uiGarage.x = 20;
        CUIManager.s_uiGarage.y = 140;
        CUIManager.s_uiGarage.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.garage] = CUIManager.s_uiGarage;
        CUIManager.s_uiMainTitle = new CUIMainTitle();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiMainTitle);
        CUIManager.s_dicUIs[Global.eUiId.main_title] = CUIManager.s_uiMainTitle;
        CUIManager.s_uiCPosButton = new CUICPosBUtton();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiCPosButton);
        CUIManager.s_dicUIs[Global.eUiId.bottom_buttons] = CUIManager.s_uiCPosButton;
        CUIManager.s_uiDeveloperEditor = new CUIDeveloperEditor();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiDeveloperEditor);
        CUIManager.s_uiDeveloperEditor.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.developer_editor] = CUIManager.s_uiDeveloperEditor;
        CUIManager.s_uiBuyLotPanel = new CUIBuyLotPanel();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiBuyLotPanel);
        CUIManager.s_uiBuyLotPanel.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.buy_lot_panel] = CUIManager.s_uiBuyLotPanel;
        CUIManager.s_uiWindTurbine = new CUIWindTurbine();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiWindTurbine);
        CUIManager.s_uiWindTurbine.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.wind_turbine] = CUIManager.s_uiWindTurbine;
        CUIManager.s_uiLotUpgrade = new CUIUpgeadeLot();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiLotUpgrade);
        CUIManager.s_uiLotUpgrade.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.lot_upgrade] = CUIManager.s_uiLotUpgrade;
        CUIManager.s_uiRevertLotConfirm = new CUIRevertLotConfirm();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiRevertLotConfirm);
        CUIManager.s_uiRevertLotConfirm.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.revert_lot_confirm] = CUIManager.s_uiRevertLotConfirm;
        CUIManager.s_uiBoost = new CUIBoost();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiBoost);
        CUIManager.s_uiBoost.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.boost_panel] = CUIManager.s_uiBoost;
        CUIManager.s_uiCityHall = new CUICityHall();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiCityHall);
        CUIManager.s_uiCityHall.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.city_hall] = CUIManager.s_uiCityHall;
        CUIManager.s_uiBank = new CUIBank();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiBank);
        CUIManager.s_uiBank.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.bank] = CUIManager.s_uiBank;
        CUIManager.s_uiBankBack = new CUIBankBack();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiBankBack);
        CUIManager.s_uiBankBack.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.bank_back] = CUIManager.s_uiBankBack;
        CUIManager.s_uiSpecialPanel = new CUISpecailPanel();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiSpecialPanel);
        CUIManager.s_uiSpecialPanel.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.special_panel] = CUIManager.s_uiSpecialPanel;
        CUIManager.s_uiToNextCityPanel = new CUIToNextCity();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiToNextCityPanel);
        CUIManager.s_uiToNextCityPanel.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.to_next_city] = CUIManager.s_uiToNextCityPanel;
        CUIManager.s_uiWelcome = new CUIWelcome();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiWelcome);
        CUIManager.s_uiWelcome.visible = true;
        CUIManager.s_dicUIs[Global.eUiId.welcome] = CUIManager.s_uiWelcome;
        CUIManager.s_uiDistrict = new CUIDistrict();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiDistrict);
        CUIManager.s_uiDistrict.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.district] = CUIManager.s_uiDistrict;
        CUIManager.s_containerUIs.addChild(CGreenMoneymanager.s_lstContainerFlyingGreen);
    };
    CUIManager.SetUiVisible = function (eId, bVisible) {
        CUIManager.s_dicUIs[eId].visible = bVisible;
        if (eId != Global.eUiId.bottom_buttons) {
            CUIManager.s_dicUIs[Global.eUiId.bottom_buttons].visible = !bVisible;
        }
        CUIManager.s_bUIShowing = bVisible;
    };
    CUIManager.IsUiShowing = function () {
        return CUIManager.s_bUIShowing || CUIManager.s_uiDeveloperEditor.visible;
    };
    CUIManager.MainLoop = function () {
        if (CUIManager.s_uiMainTitle) {
            CUIManager.s_uiMainTitle.MainLoop();
        }
        CGreenMoneymanager.FixedUpdate();
    };
    CUIManager.MainLoop_1 = function () {
        if (CUIManager.s_uiBoost && CUIManager.s_uiBoost.isShowing()) {
            CUIManager.s_uiBoost.UpdateFastForwardInfo();
        }
    };
    CUIManager.s_dicUIs = new Object();
    CUIManager.s_bUIShowing = false;
    return CUIManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIManager.prototype, "CUIManager");
//# sourceMappingURL=CUIManager.js.map