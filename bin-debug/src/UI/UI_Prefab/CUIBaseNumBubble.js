var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBaseNumBubble = (function (_super) {
    __extends(CUIBaseNumBubble, _super);
    function CUIBaseNumBubble() {
        var _this = _super.call(this) || this;
        _this.m_shapeBubble = new egret.Shape();
        _this.m_txtNum = new eui.Label();
        _this.m_nRadius = 32;
        _this.addChild(_this.m_shapeBubble);
        _this.SetExml("resource/assets/MyExml/UIBase/CUIBaseNumBubble.exml");
        _this.m_shapeBubble.graphics.beginFill(0xFF0000);
        _this.m_shapeBubble.graphics.drawCircle(_this.m_nRadius, _this.m_nRadius, _this.m_nRadius);
        _this.m_shapeBubble.graphics.endFill();
        _this.m_txtNum = _this.getChildByName("txtNum");
        _this.m_txtNum.text = "0";
        _this.addChild(_this.m_txtNum);
        return _this;
    } // end class
    CUIBaseNumBubble.prototype.SetExml = function (source) {
        this.skinName = source;
    };
    CUIBaseNumBubble.prototype.SetNum = function (num) {
        this.m_txtNum.text = num.toString();
    };
    return CUIBaseNumBubble;
}(eui.Component)); // end class
__reflect(CUIBaseNumBubble.prototype, "CUIBaseNumBubble");
//# sourceMappingURL=CUIBaseNumBubble.js.map