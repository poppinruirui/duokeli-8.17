var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CCosmosSimpleButton = (function (_super) {
    __extends(CCosmosSimpleButton, _super);
    function CCosmosSimpleButton() {
        var _this = _super.call(this) || this;
        _this.skinName = "resource/assets/MyExml/CCosmosSimpleButton.exml";
        return _this;
    } // constructor
    CCosmosSimpleButton.prototype.SetImgColor = function (nIndex, r, g, b) {
        var img = this.getChildByName("img" + nIndex);
        img.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b)];
    };
    CCosmosSimpleButton.prototype.SetTextColor = function (nIndex, color) {
        var txt = this.getChildByName("txt" + nIndex);
        txt.textColor = color;
    };
    CCosmosSimpleButton.prototype.SetTextContent = function (nIndex, szContent) {
        var txt = this.getChildByName("txt" + nIndex);
        txt.text = szContent;
    };
    return CCosmosSimpleButton;
}(eui.Component)); // end class
__reflect(CCosmosSimpleButton.prototype, "CCosmosSimpleButton");
//# sourceMappingURL=CCosmosSimpleButton.js.map