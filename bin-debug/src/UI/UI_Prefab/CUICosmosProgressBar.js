var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUICosmosProgressBar = (function (_super) {
    __extends(CUICosmosProgressBar, _super);
    function CUICosmosProgressBar() {
        var _this = _super.call(this) || this;
        _this.m_nWidth = 0;
        _this.m_nHeight = 0;
        _this.m_nThickness = 0;
        _this.m_nInnerColor = 0;
        _this.m_nInnerBGColor = 0;
        _this.m_nOutColor = 0;
        _this.m_fPercent = 1;
        _this.m_fAlpha = 1;
        _this.m_barInner = new CUICosmosBar();
        _this.m_barOut = new CUICosmosBar();
        _this.m_barInnerBG = new CUICosmosBar();
        _this.m_Mask = new egret.Shape();
        _this.addChild(_this.m_barOut);
        _this.addChild(_this.m_barInnerBG);
        _this.addChild(_this.m_barInner);
        _this.addChild(_this.m_Mask);
        return _this;
    } // constructor
    CUICosmosProgressBar.prototype.SetParams = function (width, height, thickness, inner_color, out_color, inner_bg_color, alpha) {
        if (alpha === void 0) { alpha = 1; }
        this.m_nWidth = width;
        this.m_nHeight = height;
        this.m_nThickness = thickness;
        this.m_nInnerColor = inner_color;
        this.m_nOutColor = out_color;
        this.m_nInnerBGColor = inner_bg_color;
        this.m_fAlpha = alpha;
        this.ReDraw();
    };
    CUICosmosProgressBar.prototype.ReDraw = function () {
        this.m_barInner.SetParams(this.m_nWidth, this.m_nHeight, this.m_nInnerColor, this.m_fAlpha);
        this.m_barInnerBG.SetParams(this.m_nWidth, this.m_nHeight, this.m_nInnerBGColor, this.m_fAlpha);
        this.m_barOut.SetParams(this.m_nWidth + 2 * this.m_nThickness, this.m_nHeight + 2 * this.m_nThickness, this.m_nOutColor, this.m_fAlpha);
        this.m_barOut.x = -this.m_nThickness;
        this.m_barOut.y = this.m_nHeight * 0.5;
        this.m_Mask.graphics.clear();
        this.m_Mask.graphics.beginFill(0xFFFFFF, 1);
        this.m_Mask.graphics.drawRect(0, -0.5 * this.m_nHeight, this.m_nWidth * this.m_fPercent, this.m_nHeight);
        this.m_Mask.graphics.endFill();
        this.m_Mask.y = this.m_nHeight * 0.5;
        this.m_barInner.mask = this.m_Mask;
        this.m_barInner.y = this.m_nHeight * 0.5;
        this.m_barInnerBG.x = this.m_barInner.x;
        this.m_barInnerBG.y = this.m_barInner.y;
    };
    CUICosmosProgressBar.prototype.SetPercent = function (fPercent) {
        this.m_fPercent = fPercent;
        // 这里就有典型的性能问题。怎么能动不动就重绘呢
        //this.ReDraw();
        this.m_Mask.scaleX = fPercent;
        console.log();
    };
    return CUICosmosProgressBar;
}(eui.Component)); // end class 
__reflect(CUICosmosProgressBar.prototype, "CUICosmosProgressBar");
//# sourceMappingURL=CUICosmosProgressBar.js.map