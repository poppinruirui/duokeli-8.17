var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CCosmosImage = (function (_super) {
    __extends(CCosmosImage, _super);
    function CCosmosImage() {
        var _this = _super.call(this) || this;
        _this.m_progressBar = null;
        _this.addEventListener(egret.TouchEvent.TOUCH_BEGIN, _this.onTapMeDown, _this);
        _this.addEventListener(egret.TouchEvent.TOUCH_END, _this.onTapMeUp, _this);
        return _this;
        // this.addEventListener( egret.TouchEvent.TOUCH_MOVE, this.onTapMeCancel, this );
    }
    CCosmosImage.prototype.SetAlpha = function (fAlpha) {
        this.alpha = fAlpha;
    };
    CCosmosImage.prototype.SetEnabled = function (bEnabled) {
        this.enabled = bEnabled; // eui的控件设置touchEnabl属性e没什么卵用，要设置enabled属性
        if (bEnabled) {
            this.filters = null;
            this.alpha = 1;
        }
        else {
            this.filters = [CColorFucker.GetDisabledGray()];
            this.alpha = 0.8;
        }
    };
    CCosmosImage.prototype.SetExml = function (source) {
        this.skinName = source;
    };
    CCosmosImage.prototype.onTapMeDown = function (evt) {
        if (this.enabled) {
            this.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(190, 190, 190)];
        }
    };
    CCosmosImage.prototype.onTapMeCancel = function (evt) {
        if (this.enabled) {
            this.filters = null; //[ CColorFucker.GetColorMatrixFilterByRGBA(1,1,1,1) ];
        }
    };
    CCosmosImage.prototype.onTapMeUp = function (evt) {
        if (this.enabled) {
            this.filters = null; //[ CColorFucker.GetColorMatrixFilterByRGBA(1,1,1,1) ];
        }
    };
    CCosmosImage.prototype.SetTextVisible = function (nIndex, bVisible) {
        var txt = this.getChildByName("txt" + nIndex);
        txt.visible = bVisible;
    };
    CCosmosImage.prototype.SetTextColor = function (nIndex, color) {
        var txt = this.getChildByName("txt" + nIndex);
        txt.textColor = color;
    };
    CCosmosImage.prototype.SetTextSize = function (nIndex, nSize) {
        var txt = this.getChildByName("txt" + nIndex);
        txt.size = nSize;
    };
    CCosmosImage.prototype.SetText = function (nIndex, szContent) {
        var txt = this.getChildByName("txt" + nIndex);
        txt.text = szContent;
    };
    CCosmosImage.prototype.SetImageTexture = function (nIndex, tex) {
        var img = this.getChildByName("img" + nIndex);
        img.texture = tex;
    };
    CCosmosImage.prototype.SetImageAnchor = function (nIndex, nType) {
        var img = this.getChildByName("img" + nIndex);
        img.anchorOffsetX = img.width * 0.5;
        img.anchorOffsetX = img.height * 0.5;
    };
    CCosmosImage.prototype.SetImageSize = function (nIndex, nWidth, nHeight) {
        var img = this.getChildByName("img" + nIndex);
        img.width = nWidth;
        img.height = nHeight;
    };
    CCosmosImage.prototype.SetImageVisible = function (nIndex, bVisible) {
        var img = this.getChildByName("img" + nIndex);
        img.visible = bVisible;
    };
    CCosmosImage.prototype.SetImageAlpha = function (nIndex, fAlpha) {
        var img = this.getChildByName("img" + nIndex);
        img.alpha = fAlpha;
    };
    CCosmosImage.prototype.SetImageColor_New = function (nIndex, r, g, b) {
        var img = this.getChildByName("img" + nIndex);
        CColorFucker.SetImageColor(img, r, g, b);
    };
    CCosmosImage.prototype.SetImageColor_255 = function (tex, nIndex, r, g, b, a, bFilter) {
        if (a === void 0) { a = 255; }
        if (bFilter === void 0) { bFilter = true; }
        r /= 255;
        g /= 255;
        b /= 255;
        a /= 255;
        this.SetImageColor(tex, nIndex, r, g, b, a, bFilter);
    };
    CCosmosImage.prototype.SetImageColor = function (tex, nIndex, r, g, b, a, bFilter) {
        if (a === void 0) { a = 1; }
        if (bFilter === void 0) { bFilter = true; }
        var img = this.getChildByName("img" + nIndex);
        if (bFilter) {
            img.filters = [CColorFucker.GetColorMatrixFilterByRGBA(r, g, b, a)];
        }
        else {
            img.filters = null;
        }
        img.alpha = a;
        if (tex != null) {
            img.source = tex;
        }
    };
    CCosmosImage.prototype.SetLabelVisible = function (nIndex, bVisible) {
        var txt = this.getChildByName("txt" + nIndex);
        txt.visible = bVisible;
    };
    CCosmosImage.prototype.SetLabelContent = function (nIndex, szContent) {
        var txt = this.getChildByName("txt" + nIndex);
        txt.text = szContent;
    };
    CCosmosImage.prototype.UseColorSolution = function (nSolutionId) {
        if (nSolutionId === void 0) { nSolutionId = 0; }
        switch (nSolutionId) {
            case 0:
                {
                    var tex = RES.getRes(CDef.RES_1x1);
                    this.SetImageColor(tex, 0, 1, 1, 1);
                    this.SetImageColor(tex, 1, 0.663, 0.663, 0.663);
                    this.SetImageColor(tex, 2, 0.923, 0.278, 0.227);
                    this.SetImageColor(tex, 3, 0.753, 0.2, 0.145);
                }
                break;
            case 1:
                {
                    var tex = RES.getRes(CDef.RES_1x1);
                    this.SetImageColor(tex, 0, 1, 1, 1);
                    this.SetImageColor(tex, 1, 0.663, 0.663, 0.663);
                    this.SetImageColor(tex, 2, 0.420, 0.761, 0.224);
                    this.SetImageColor(tex, 3, 0.306, 0.541, 0.149);
                }
                break;
            case 2:
                {
                    this.SetImageColor(tex, 0, 1, 1, 1);
                    this.SetImageColor(tex, 1, 0.369, 0.561, 0.204);
                    this.SetImageColor(tex, 2, 0.65, 0.847, 0.463);
                    this.SetImageColor(tex, 3, 0.831, 0.831, 0.831);
                    this.SetTextColor(0, 0x5E8F34);
                    this.SetTextColor(1, 0x5E8F34);
                    this.SetTextColor(2, 0x666666);
                }
                break;
            case 3:
                {
                    this.SetImageColor(tex, 0, 1, 1, 1);
                    this.SetImageColor(tex, 1, 0.271, 0.514, 0.737);
                    this.SetImageColor(tex, 2, 0.561, 0.788, 0.984);
                    this.SetImageColor(tex, 3, 0.831, 0.831, 0.831);
                    this.SetTextColor(0, 0x4583BC);
                    this.SetTextColor(1, 0x4583BC);
                    this.SetTextColor(2, 0x666666);
                }
                break;
            case 4:
                {
                    this.SetImageColor(tex, 0, 1, 1, 1);
                    this.SetImageColor(tex, 1, 0.513, 0.478, 0.145);
                    this.SetImageColor(tex, 2, 0.835, 0.784, 0.275);
                    this.SetImageColor(tex, 3, 0.831, 0.831, 0.831);
                    this.SetTextColor(0, 0x837A25);
                    this.SetTextColor(1, 0x837A25);
                    this.SetTextColor(2, 0x666666);
                }
                break;
        }
    };
    CCosmosImage.prototype.CreateProgressBar = function (nImgIndex, nProgressBarExmlIndex, aryFilledbarColor, aryFilledbarBgColor) {
        var imgTemp = this.getChildByName("img2");
        this.m_progressBar = new CUIBaseProgressBar("resource/assets/MyExml/CProgressBar" + nProgressBarExmlIndex + ".exml");
        this.addChild(this.m_progressBar);
        this.m_progressBar.x = imgTemp.x;
        this.m_progressBar.y = imgTemp.y;
        this.m_progressBar.SetFilledBarTotalWidth(imgTemp.width);
        this.m_progressBar.SetFilledBarTotalHeight(imgTemp.height);
        this.m_progressBar.SetFilledBarColor(aryFilledbarColor[0], aryFilledbarColor[1], aryFilledbarColor[2]);
        this.m_progressBar.SetFilledBarBgColor(aryFilledbarBgColor[0], aryFilledbarBgColor[1], aryFilledbarBgColor[2]);
        this.removeChild(imgTemp);
    };
    CCosmosImage.prototype.SetProgressBarPercent = function (fPercent) {
        if (this.m_progressBar == null) {
            return;
        }
        this.m_progressBar.SetPercentByPercent(fPercent);
    };
    return CCosmosImage;
}(eui.Component)); // end class
__reflect(CCosmosImage.prototype, "CCosmosImage");
//# sourceMappingURL=CCosmosImage.js.map