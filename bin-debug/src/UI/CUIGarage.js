var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
     “车库”类
*/
var CUIGarage = (function (_super) {
    __extends(CUIGarage, _super);
    function CUIGarage() {
        var _this = _super.call(this) || this;
        _this.m_groupForCarsList = new eui.Group();
        _this.m_scrollForCarsList = new eui.Scroller();
        _this.m_aryItems = Array();
        _this.m_bmpBG = new eui.Image(RES.getRes("1X1_png"));
        _this.addChild(_this.m_bmpBG);
        _this.m_bmpBG.width = 600;
        _this.m_bmpBG.height = 900;
        _this.m_txtActiveCarTitle = new eui.Label();
        _this.addChild(_this.m_txtActiveCarTitle);
        _this.m_txtActiveCarTitle.width = _this.m_bmpBG.width;
        _this.m_txtActiveCarTitle.textAlign = egret.HorizontalAlign.CENTER;
        _this.m_txtActiveCarTitle.text = "活动的汽车数：10";
        _this.m_txtActiveCarTitle.textColor = 0x000000;
        _this.m_txtActiveCarTitle.y = 10;
        _this.m_txtActiveCarTitle.bold = true;
        _this.m_txtEarnigTitle = new eui.Label();
        _this.addChild(_this.m_txtEarnigTitle);
        _this.m_txtEarnigTitle.textAlign = egret.HorizontalAlign.LEFT;
        _this.m_txtEarnigTitle.text = "产出金币";
        _this.m_txtEarnigTitle.textColor = 0x777777;
        _this.m_txtEarnigTitle.x = 200;
        _this.m_txtEarnigTitle.y = 50;
        _this.m_txtEarnigTitle.size = 24;
        _this.m_txtEarnigValue = new eui.Label();
        _this.addChild(_this.m_txtEarnigValue);
        _this.m_txtEarnigValue.textAlign = egret.HorizontalAlign.LEFT;
        _this.m_txtEarnigValue.text = "75/秒";
        _this.m_txtEarnigValue.textColor = 0x777777;
        _this.m_txtEarnigValue.x = 340;
        _this.m_txtEarnigValue.y = 50;
        _this.m_txtEarnigValue.size = 24;
        _this.m_imgCoin = new eui.Image(CJinBiManager.GetFrameTexture(0));
        _this.addChild(_this.m_imgCoin);
        _this.m_imgCoin.scaleX = 0.4;
        _this.m_imgCoin.scaleY = 0.4;
        _this.m_imgCoin.x = 305;
        _this.m_imgCoin.y = 40;
        var nCarNum = CConfigManager.s_nTotalCarNum;
        var contentContainer = new egret.DisplayObjectContainer();
        for (var i = 0; i < nCarNum; i++) {
            var item = new CUIGarageItem();
            //  this.m_groupForCarsList.addChild( item );
            contentContainer.addChild(item);
            item.y = i * (CDef.s_fGarageItemHeight + 5);
            item.SetIndex(i);
            // item.SetLock( true );
            //    item.SetStatus();
            _this.m_aryItems.push(item);
        } // end for i
        // poppin test
        var sv = new egret.ScrollView();
        _this.addChild(sv);
        sv.setContent(contentContainer);
        sv.x = 25;
        sv.y = 100;
        sv.width = 550;
        sv.height = 7 * CDef.s_fGarageItemHeight;
        sv.verticalScrollPolicy = "on";
        sv.horizontalScrollPolicy = "off";
        _this.m_btnClose = new CUIBaseCommonButton();
        _this.addChild(_this.m_btnClose);
        _this.m_btnClose.x = 100;
        _this.m_btnClose.y = 910;
        _this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        return _this;
    } // end constructor
    ;
    CUIGarage.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.garage, false);
    };
    CUIGarage.prototype.MainLoop = function () {
    };
    CUIGarage.prototype.UpdateStatus = function (nIndex, eStatus) {
        /*
        for ( var i:number = 0; i < this.m_aryItems.length; i++ )
        {
            var item:CUIGarageItem = this.m_aryItems[i];
            item.SetStatus( Main.s_CurTown.GetGarageItemStatus( i ) );
        } // end i
        */
        var item = this.m_aryItems[nIndex];
        item.SetStatus(Main.s_CurTown.GetGarageItemStatus(nIndex));
    };
    CUIGarage.prototype.SetMaskVisible = function (nIndex, bVisible) {
        if (nIndex >= this.m_aryItems.length) {
            return;
        }
        var item = this.m_aryItems[nIndex];
        item.SetMaskVisible(bVisible);
    };
    return CUIGarage;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIGarage.prototype, "CUIGarage");
//# sourceMappingURL=CUIGarage.js.map