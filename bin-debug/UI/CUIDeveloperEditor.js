var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
    研发人员用的编辑器
*/
var CUIDeveloperEditor = (function (_super) {
    __extends(CUIDeveloperEditor, _super);
    function CUIDeveloperEditor() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CDeveloperEditor.exml";
        _this.addChild(_this.m_uiContainer);
        _this.m_listOperate = _this.m_uiContainer.getChildByName("lstMainMenu");
        _this.m_listSelectTown = _this.m_uiContainer.getChildByName("lstTownNo");
        _this.m_listSelectTown.selectedIndex = 0;
        _this.m_listSelectTown.dataProvider = new eui.ArrayCollection([
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
        ]);
        _this.m_listSelectTown.height = 1000;
        _this.m_listSelectTown.addEventListener(eui.ItemTapEvent.ITEM_TAP, _this.onListTownChange, _this);
        _this.m_listOperate.dataProvider = new eui.ArrayCollection([
            "do nothing",
            //    "打开车商城",
            "编辑建筑",
            "删除建筑",
            "增加钻石",
            "增加金币",
            "放置市政厅",
            "放置车站",
            "放置银行",
            "放置风车",
            "放置飞机场",
            "放置码头",
            "马路-左上右下",
            "马路-右上左下",
            "马路转角-上",
            "马路转角-下",
            "马路转角-左",
            "马路转角-右",
            "马路十字",
            "马路丁字右上",
            "马路丁字右下",
            "马路丁字左上",
            "马路丁字左下",
            "2*2型宗地",
            "4*4型宗地",
            "添加地表",
            "删除地表",
            "摆放树",
            "箭头左上",
            "箭头左下",
            "箭头右上",
            "箭头右下",
        ]);
        _this.m_listOperate.addEventListener(eui.ItemTapEvent.ITEM_TAP, _this.onListOperateChange, _this);
        _this.m_listOperate.height = 2000;
        _this.m_listOperate.selectedIndex = 1;
        _this.m_btnZoomIn = _this.m_uiContainer.getChildByName("btnZoomIn");
        _this.m_btnZoomIn.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_ZoomIn, _this);
        _this.m_btnZoomOut = _this.m_uiContainer.getChildByName("btnZoomOut");
        _this.m_btnZoomOut.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_ZoomOut, _this);
        // “保存”按钮
        _this.m_btnSave = _this.m_uiContainer.getChildByName("btnSave");
        _this.m_btnSave.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Save, _this);
        return _this;
    } // end constructor
    CUIDeveloperEditor.prototype.onListOperateChange = function (e) {
        //获取点击消息
        Main.s_CurTown.SetOperate(this.m_listOperate.selectedIndex);
    };
    CUIDeveloperEditor.prototype.onListTownChange = function (e) {
        var nTownId = this.m_listSelectTown.selectedIndex + 1;
        Main.s_CurTown.ChangeTown(nTownId);
    };
    CUIDeveloperEditor.prototype.onButtonClick_ZoomIn = function (e) {
        var fCurSize = Main.s_CurTown.scaleX;
        fCurSize += 0.03;
        Main.s_CurTown.scaleX = fCurSize;
        Main.s_CurTown.scaleY = fCurSize;
    };
    CUIDeveloperEditor.prototype.onButtonClick_ZoomOut = function (e) {
        var fCurSize = Main.s_CurTown.scaleX;
        fCurSize -= 0.03;
        Main.s_CurTown.scaleX = fCurSize;
        Main.s_CurTown.scaleY = fCurSize;
    };
    CUIDeveloperEditor.prototype.onButtonClick_Save = function (e) {
        var szData = Main.s_CurTown.GenerateData();
        egret.localStorage.setItem("Town" + Main.s_CurTown.GetId(), szData);
    };
    return CUIDeveloperEditor;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIDeveloperEditor.prototype, "CUIDeveloperEditor");
//# sourceMappingURL=CUIDeveloperEditor.js.map