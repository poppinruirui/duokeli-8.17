var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIUpgeadeLot = (function (_super) {
    __extends(CUIUpgeadeLot, _super);
    function CUIUpgeadeLot() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CUpgradePanel.exml";
        _this.addChild(_this.m_uiContainer);
        var imgTemp = _this.m_uiContainer.getChildByName("btnClose");
        _this.m_btnClose = new CCosmosImage();
        _this.m_btnClose.SetExml("resource/assets/MyExml/CCosmosButton1.exml");
        _this.m_uiContainer.addChild(_this.m_btnClose);
        _this.m_btnClose.x = imgTemp.x;
        _this.m_btnClose.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnClose.UseColorSolution(0);
        _this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        _this.m_labelCurLotLevel = _this.m_uiContainer.getChildByName("labelBuildingLevel");
        _this.m_labelBuildingName = _this.m_uiContainer.getChildByName("labelBuildingName");
        _this.m_labelCurLotCPS = _this.m_uiContainer.getChildByName("labelBuildingCPS");
        var imgTemp = _this.m_uiContainer.getChildByName("btnRevert");
        _this.m_btnRevert = new CCosmosImage();
        _this.m_uiContainer.addChild(_this.m_btnRevert);
        _this.m_btnRevert.SetExml("resource/assets/MyExml/CCosmosButton2.exml");
        _this.m_btnRevert.x = imgTemp.x;
        _this.m_btnRevert.y = imgTemp.y;
        _this.m_btnRevert.scaleX = imgTemp.scaleX;
        _this.m_btnRevert.scaleY = imgTemp.scaleY;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnRevert.SetImageColor_255(null, 1, 179, 58, 43, 255);
        _this.m_btnRevert.SetImageColor_255(null, 2, 235, 77, 58, 255);
        var tex = RES.getRes("bulldoze_png");
        _this.m_btnRevert.SetImageColor_255(tex, 4, 255, 255, 255, 255, false);
        _this.m_btnRevert.SetLabelContent(0, "");
        _this.m_btnRevert.SetLabelContent(1, "");
        _this.m_btnRevert.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Revert, _this);
        imgTemp = _this.m_uiContainer.getChildByName("btnInstantUpgrade");
        _this.m_btnInstantUpgrade = new CCosmosImage();
        _this.m_uiContainer.addChild(_this.m_btnInstantUpgrade);
        _this.m_btnInstantUpgrade.SetExml("resource/assets/MyExml/CCosmosButton2.exml");
        _this.m_btnInstantUpgrade.x = imgTemp.x;
        _this.m_btnInstantUpgrade.y = imgTemp.y;
        _this.m_btnInstantUpgrade.scaleX = imgTemp.scaleX;
        _this.m_btnInstantUpgrade.scaleY = imgTemp.scaleY;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnInstantUpgrade.SetImageColor_255(null, 1, 242, 166, 58, 255);
        _this.m_btnInstantUpgrade.SetImageColor_255(null, 2, 247, 206, 70, 255);
        var tex = RES.getRes("hammer_png");
        _this.m_btnInstantUpgrade.SetImageColor_255(tex, 4, 255, 255, 255, 255, false);
        _this.m_btnInstantUpgrade.SetLabelContent(0, "");
        _this.m_btnInstantUpgrade.SetLabelContent(1, "");
        _this.m_btnInstantUpgrade.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_InstantUpgrade, _this);
        //// “手动摆放”按钮
        imgTemp = _this.m_uiContainer.getChildByName("btnMannual");
        _this.m_btnMannual = new CCosmosImage();
        _this.m_uiContainer.addChild(_this.m_btnMannual);
        _this.m_btnMannual.SetExml("resource/assets/MyExml/CCosmosButton2.exml");
        _this.m_btnMannual.x = imgTemp.x;
        _this.m_btnMannual.y = imgTemp.y;
        _this.m_btnMannual.scaleX = imgTemp.scaleX;
        _this.m_btnMannual.scaleY = imgTemp.scaleY;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnMannual.SetImageColor_255(null, 1, 242, 166, 58, 255);
        _this.m_btnMannual.SetImageColor_255(null, 2, 247, 206, 70, 255);
        var tex = RES.getRes("hammer_png");
        _this.m_btnMannual.SetImageColor_255(tex, 4, 255, 255, 255, 255, false);
        _this.m_btnMannual.SetLabelContent(0, "");
        _this.m_btnMannual.SetLabelContent(1, "");
        _this.m_btnMannual.SetLabelContent(2, "");
        _this.m_btnMannual.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_BuildingsPanel, _this);
        //// end “手动摆放”按钮
        imgTemp = _this.m_uiContainer.getChildByName("img1");
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(188, 176, 56)];
        imgTemp = _this.m_uiContainer.getChildByName("img2");
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(221, 221, 221)];
        _this.m_imgBuildingAvatar = _this.m_uiContainer.getChildByName("imgAvatar");
        _this.m_btnRevert.SetText(2, "推平成空地");
        _this.m_btnInstantUpgrade.SetText(3, "1");
        _this.m_btnInstantUpgrade.SetText(2, "");
        _this.m_btnInstantUpgrade.SetImageTexture(5, RES.getRes("zuanshi_png"));
        _this.m_cbHistorical = _this.m_uiContainer.getChildByName("cbHistorical");
        _this.m_cbHistorical.addEventListener(eui.UIEvent.CHANGE, _this.onCheckBoxValueChange, _this);
        return _this;
    } // end constructor
    CUIUpgeadeLot.prototype.onCheckBoxValueChange = function (evt) {
        var obj = Main.s_CurTown.GetCurEditProcessingLot();
        obj.SetHistorical(this.IsHistorical());
    };
    CUIUpgeadeLot.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.lot_upgrade, false);
        Main.s_CurTown.SetCurEditProcessingLot(null);
    };
    CUIUpgeadeLot.prototype.SetCurLotLevel = function (nCurLotLevel) {
        this.m_labelCurLotLevel.text = nCurLotLevel + "级";
    };
    CUIUpgeadeLot.prototype.SetCurLotCPS = function (nCurLotCPS) {
        this.m_labelCurLotCPS.text = nCurLotCPS.toString();
    };
    CUIUpgeadeLot.prototype.SetBuildingAvatar = function (tex) {
        this.m_imgBuildingAvatar.texture = tex;
    };
    CUIUpgeadeLot.prototype.SetBuildingName = function (szBuildingName) {
        this.m_labelBuildingName.text = szBuildingName;
    };
    CUIUpgeadeLot.prototype.onButtonClick_BuildingsPanel = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.lot_upgrade, false);
        CUIManager.SetUiVisible(Global.eUiId.special_panel, true);
        CUIManager.s_uiSpecialPanel.UpdateInfo();
    };
    CUIUpgeadeLot.prototype.onButtonClick_Revert = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.revert_lot_confirm, true);
    };
    CUIUpgeadeLot.prototype.onButtonClick_InstantUpgrade = function (evt) {
        var lotEditing = Main.s_CurTown.GetCurEditProcessingLot();
        lotEditing.SetLotLevel(lotEditing.GetLotLevel() + 1);
        this.SetCurLotLevel(lotEditing.GetLotLevel());
        var nNeedDiamond = CConfigManager.GetNeedDiamondBySizeAndLevel(lotEditing.GetSizeType(), lotEditing.GetLotLevel());
        nNeedDiamond = Math.floor(nNeedDiamond);
        if (nNeedDiamond > CPlayer.GetDiamond()) {
            console.log("有Bug");
            return;
        }
        CPlayer.SetDiamond(CPlayer.GetDiamond() - nNeedDiamond);
        this.UpdateInstanceUpgradeButtonStatus();
    };
    CUIUpgeadeLot.prototype.UpdateInstanceUpgradeButtonStatus = function () {
        var lotEditing = Main.s_CurTown.GetupgradinLot();
        var lotUpgrading = Main.s_CurTown.GetCurEditProcessingLot();
        if (lotEditing == null) {
            return;
        }
        var nNeedDiamond = CConfigManager.GetNeedDiamondBySizeAndLevel(lotEditing.GetSizeType(), lotEditing.GetLotLevel());
        nNeedDiamond = Math.floor(nNeedDiamond);
        if (nNeedDiamond > CPlayer.GetDiamond()) {
            this.m_btnInstantUpgrade.SetEnabled(false);
        }
        else {
            this.m_btnInstantUpgrade.SetEnabled(true);
        }
        this.m_btnInstantUpgrade.SetText(3, nNeedDiamond.toString());
    };
    CUIUpgeadeLot.prototype.IsHistorical = function () {
        return this.m_cbHistorical.selected;
    };
    CUIUpgeadeLot.prototype.UpdatePanelStatusWhenOpen = function () {
        var obj = Main.s_CurTown.GetCurEditProcessingLot();
        this.m_cbHistorical.selected = obj.GetHistorical();
    };
    return CUIUpgeadeLot;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIUpgeadeLot.prototype, "CUIUpgeadeLot");
//# sourceMappingURL=CUIUpgeadeLot.js.map