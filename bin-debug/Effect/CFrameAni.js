var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CFrameAni = (function (_super) {
    __extends(CFrameAni, _super);
    function CFrameAni() {
        var _this = _super.call(this) || this;
        _this.m_bmpFrame = new egret.Bitmap();
        _this.m_szResKey = "";
        _this.m_nFrameTotalNum = 0;
        _this.m_nFrameInterval = 0;
        _this.m_bLoop = false;
        _this.m_nCurIndex = 0;
        _this.m_bPlaying = false;
        _this.m_nTimeElapse = 0;
        _this.addChild(_this.m_bmpFrame);
        return _this;
    } // end constructor
    CFrameAni.prototype.SetParams = function (res_key, total_frames, frame_interval, loop) {
        if (loop === void 0) { loop = false; }
        this.m_szResKey = res_key;
        this.m_nFrameTotalNum = total_frames;
        this.m_nFrameInterval = frame_interval;
        this.m_bLoop = loop;
    };
    CFrameAni.prototype.ChangeFrame = function (nIndex) {
        var szRes = this.m_szResKey + nIndex + "_png";
        this.m_bmpFrame.texture = RES.getRes(szRes);
        this.m_bmpFrame.anchorOffsetX = this.m_bmpFrame.width * 0.5;
        this.m_bmpFrame.anchorOffsetY = this.m_bmpFrame.height * 0.5;
        this.m_bmpFrame.scaleX = 1.2;
        this.m_bmpFrame.scaleY = 1.2;
    };
    CFrameAni.prototype.Play = function () {
        this.m_nTimeElapse = 0;
        this.m_bPlaying = true;
        this.ChangeFrame(0);
        this.m_nCurIndex = 0;
    };
    CFrameAni.prototype.FixedUpdate = function () {
        if (!this.m_bPlaying) {
            return true;
        }
        this.m_nTimeElapse += CDef.s_fFixedDeltaTime;
        if (this.m_nTimeElapse < this.m_nFrameInterval) {
            return true;
        }
        this.m_nTimeElapse = 0;
        this.m_nCurIndex++;
        if (this.m_nCurIndex >= this.m_nFrameTotalNum) {
            this.m_nCurIndex = 0;
            return true;
        }
        this.ChangeFrame(this.m_nCurIndex);
        return false;
    };
    return CFrameAni;
}(egret.DisplayObjectContainer)); // end class
__reflect(CFrameAni.prototype, "CFrameAni");
//# sourceMappingURL=CFrameAni.js.map